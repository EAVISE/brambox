"""
Brambox generic parsers module |br|
These parsers can be used to parse and generate both annotation or detection files.
"""

from ._pandas import *
