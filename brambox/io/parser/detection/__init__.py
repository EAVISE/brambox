"""
Brambox detection parsers module |br|
These parsers can be used to parse and generate detection files.
"""

from ._coco import *
from ._dollar import *
from ._erti import *
from ._mot import *
from ._pascalvoc import *
from ._yaml import *
