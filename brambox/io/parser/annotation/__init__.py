"""
Brambox annotation parsers module |br|
These parsers can be used to parse and generate annotation files.
"""

from ._coco import *
from ._cvat_image import *
from ._cvat_video import *
from ._cvc import *
from ._darknet import *
from ._dollar import *
from ._dota import *
from ._hrsc import *
from ._kitti import *
from ._mot import *
from ._pascalvoc import *
from ._vatic import *
from ._yaml import *
