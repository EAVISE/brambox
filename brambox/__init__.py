#
# BRAMBOX: Basic Recipes for Annotation Munching toolBOX
# Copyright EAVISE
#
from . import _version, eval, io, stat, util

__version__ = _version.get_versions()['version']
