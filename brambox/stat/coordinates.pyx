#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Statistics based around coordinates of the bounding boxes
#
import numpy as np
from brambox._imports import shapely
from ..util import np_col

cimport numpy as np
from libc.stdlib cimport malloc, free
from libc.math cimport ceil
from . cimport coordinates
from brambox.util cimport morton_argsort_single, SFM_INDEX

__all__ = ['intersection', 'iou', 'ioa', 'pdollar']
cdef int MAX_POLY = 1000
np.import_array()


def intersection(a, b, segment=None):
    """ Computes the intersection between all the bounding boxes of ``a`` and ``b``.

    If ``segment`` is not set to **False** and both ``a`` and ``b`` have a "segmentation" column,
    we compute the intersection using the segmentation data.
    Otherwise, we use the bounding box data.

    Args:
        a (pandas.DataFrame): Brambox bounding box dataframe
        b (pandas.DataFrame): Brambox bounding box dataframe
        segment (bool, optional): Whether to use the segmentation data; Default **use if available**

    Returns:
        (brambox.util.SparseFloatMatrix): [len(a) x len(b)] matrix containing the intersection values between all boxes
    """
    cdef Intersection coords = Intersection(a, b, segment)
    cdef SparseFloatMatrix result = SparseFloatMatrix(coords.alen, coords.blen)

    coords.compute(result)
    return result


def ioa(a, b, denominator='a', bias=1e-10, segment=None):
    """ Computes the intersection over area between all the bounding boxes of ``a`` and ``b``. |br|
    :math:`IoA = \\frac { {intersection}(a, b) } { {area}(denominator) }`

    If ``segment`` is not set to **False** and both ``a`` and ``b`` have a "segmentation" column,
    we compute the IoA using the segmentation data.
    Otherwise, we use the bounding box data.

    Args:
        a (pandas.DataFrame): Brabox bounding box dataframe
        b (pandas.DataFrame): Brambox bounding box dataframe
        denominator (string or int, optional): String indicating from which box to compute the area (possible values: either 'a', 'b' or equivalently 0, 1); Default **'a'**
        bias (Number, optional): Small bias value that is added to the denominator to prevent 'division by zero error'; Default **1e-10**
        segment (bool, optional): Whether to use the segmentation data; Default **use if available**

    Returns:
        (brambox.util.SparseFloatMatrix): [len(a) x len(b)] matrix containing the IoA values between all boxes

    Note:
        If you know there are no bounding boxes with an area of **0**, you can disable the bias term by setting it to **0**.
        This will result in slightly more accurate results, but might cause errors if you have a union denominator of **0**. |br|
        However, the difference in result caused by the bias term is usually negligible.
    """
    cdef IoA coords = IoA(a, b, segment, bias, denominator)
    cdef SparseFloatMatrix result = SparseFloatMatrix(coords.alen, coords.blen)

    coords.compute(result)
    return result


def iou(a, b, float bias=1e-10, segment=None):
    """ Computes the intersection over union between all the bounding boxes of ``a`` and ``b``. |br|
    :math:`IoU = \\frac { {intersection}(a, b) } { {union}(a, b) }`

    If ``segment`` is not set to **False** and both ``a`` and ``b`` have a "segmentation" column,
    we compute the IoU using the segmentation data.
    Otherwise, we use the bounding box data.

    Args:
        a (pandas.DataFrame): Brambox bounding box dataframe
        b (pandas.DataFrame): Brambox bounding box dataframe
        bias (Number, optional): Small bias value that is added to the union to prevent 'division by zero error'; Default **1e-10**
        segment (bool, optional): Whether to use the segmentation data; Default **use if available**

    Returns:
        (brambox.util.SparseFloatMatrix): [len(a) x len(b)] matrix containing the IoU values between all boxes

    Note:
        If you know there are no bounding boxes with an area of **0**, you can disable the bias term by setting it to **0**.
        This will result in slightly more accurate results, but might cause errors if you have a union denominator of **0**. |br|
        However, the difference in result caused by the bias term is usually negligible.
    """
    cdef IoU coords = IoU(a, b, segment, bias)
    cdef SparseFloatMatrix result = SparseFloatMatrix(coords.alen, coords.blen)

    coords.compute(result)
    return result


def pdollar(det, anno, bint use_ignore_method=True, float bias=1e-10, segment=None):
    """ This function computes a matching criteria between bounding boxes like the `Piotr Dollar Toolbox <dollar_>`_. |br|
    It computes the IoU between the bounding boxes of ``det`` and ``anno`` for the annotations that are not ignored.
    For the ignored annotations this function computes the IoA with the area of the detection as denominator.

    If ``segment`` is not set to **False** and both ``a`` and ``b`` have a "segmentation" column,
    we compute pdollar IoU using the segmentation data.
    Otherwise, we use the bounding box data.

    Args:
        det (pandas.DataFrame): Brambox detection dataframe
        anno (pandas.DataFrame): Brambox annotation dataframe
        use_ignore_method (Boolean, optional): Whether to use the 'ignore_method' column of the annotations as regions if available; Default **True**
        bias (Number, optional): Small bias value that is added to the union to prevent 'division by zero error'; Default **1e-10**
        segment (bool, optional): Whether to use the segmentation data; Default **use if available**

    Returns:
        (brambox.util.SparseFloatMatrix): [len(det) x len(anno)] matrix containing the pdollar IoU values between all boxes

    Warning:
        Unlike other coordinate statistical functions, this function requires
        the first dataframe to be detections and the second one to be annotations.

    Note:
        If your annotation dataframe contains an `ignore_method` integer column, which contains the values from the :class:`~brambox.stat.IgnoreMethod` enum,
        this function will use `anno['ignore'] & (anno['ignore_method'] == 2)` as a boolean column for regions,
        as these are the annotations which you wish to be treated as **IgnoreMethod.MULTIPLE**.
        Otherwise it will simply use the `ignore` column.

        You can disable this behaviour and always use the `ignore` column, by setting `use_ignore_method` to **False**.

    Note:
        If you know there are no bounding boxes with an area of **0**, you can disable the bias term by setting it to **0**.
        This will result in slightly more accurate results, but might cause errors if you have a union denominator of **0**. |br|
        However, the difference in result caused by the bias term is usually negligible.
    """
    cdef SparseFloatMatrix result
    cdef PDollar coords

    coords = PDollar(det, anno, segment, bias, use_ignore_method)
    result = SparseFloatMatrix(coords.alen, coords.blen)

    coords.compute(result)
    return result


cdef class Coordinates:
    def __cinit__(self, a not None, b not None, poly=None, *args, **kwargs):
        cdef ssize_t bi
        self.poly = poly or (poly is None and 'segmentation' in a and 'segmentation' in b)

        if self.poly:
            if 'segmentation' not in a or 'segmentation' not in b:
                raise TypeError('No segmentation column found!')
            self.ap = np_col(a, 'segmentation')
            self.bp = np_col(b, 'segmentation')
            self.alen = self.ap.shape[0]
            self.blen = self.bp.shape[0]
        else:
            self.ax = np_col(a, 'x_top_left')
            self.ay = np_col(a, 'y_top_left')
            self.aw = np_col(a, 'width')
            self.ah = np_col(a, 'height')
            self.bx = np_col(b, 'x_top_left')
            self.by = np_col(b, 'y_top_left')
            self.bw = np_col(b, 'width')
            self.bh = np_col(b, 'height')
            self.alen = self.ax.shape[0]
            self.blen = self.bx.shape[0]

            self.bx2 = <np.float64_t *> malloc(self.blen * sizeof(np.float64_t))
            self.by2 = <np.float64_t *> malloc(self.blen * sizeof(np.float64_t))
            self.ba = <np.float64_t *> malloc(self.blen * sizeof(np.float64_t))
            if not (self.bx2 and self.by2 and self.ba):
                raise MemoryError()

            for bi in range(self.blen):
                self.bx2[bi] = self.bx[bi] + self.bw[bi]
                self.by2[bi] = self.by[bi] + self.bh[bi]
                self.ba[bi] = self.bw[bi] * self.bh[bi]

    def __dealloc__(self):
        free(self.bx2)
        free(self.by2)
        free(self.ba)

    cdef void compute(self, SparseFloatMatrix result):
        cdef ssize_t ai, bi, i
        cdef ssize_t chunksize, iter
        cdef np.float64_t ax2, ay2, aa
        cdef np.float64_t value
        cdef ssize_t[:] idx_a, idx_b
        cdef object[:] poly_a, poly_b
        cdef object tree

        if self.poly:
            if self.alen * self.blen < MAX_POLY:
                idx_a = np.PyArray_Arange(<double> 0, <double> self.alen, 1.0, np.NPY_TYPES.NPY_INTP)
                idx_b = np.PyArray_Arange(<double> 0, <double> self.blen, 1.0, np.NPY_TYPES.NPY_INTP)
                self.compute_poly(result, self.ap, idx_a, self.bp, idx_b)
            else:
                chunksize = max(1, MAX_POLY // self.blen)
                iter = max(1, <ssize_t> ceil((<float> self.alen) / chunksize))

                tree = shapely.STRtree(self.bp)
                idx_a = morton_argsort_single(
                    shapely.get_coordinates(shapely.centroid(self.ap), include_z=False).astype(np.uint32),
                )

                for ai in range(iter):
                    ai *= chunksize
                    bi = min(self.alen, ai + chunksize)

                    poly_a = self.ap.base[idx_a[ai:bi]]
                    idx_b = np.unique(tree.query(poly_a)[1])
                    poly_b = self.bp.base[idx_b]

                    self.compute_poly(result, poly_a, idx_a[ai:bi], poly_b, idx_b)
        else:
            for ai in range(self.alen):
                ax2 = self.ax[ai] + self.aw[ai]
                ay2 = self.ay[ai] + self.ah[ai]
                aa = self.aw[ai] * self.ah[ai]
                for bi in range(self.blen):
                    value = self.compute_box(ai, bi, ax2, ay2, aa)
                    if value:
                        result.set(SFM_INDEX(ai, bi), value)

    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil:
        return 0

    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b):
        pass


cdef class Intersection(Coordinates):
    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil:
        cdef np.float64_t min_val, max_val, dx, dy

        min_val = ax2 if ax2 <= self.bx2[bi] else self.bx2[bi]
        max_val = self.ax[ai] if self.ax[ai] >= self.bx[bi] else self.bx[bi]
        dx = min_val - max_val
        if dx < 0:
            return 0

        min_val = ay2 if ay2 <= self.by2[bi] else self.by2[bi]
        max_val = self.ay[ai] if self.ay[ai] >= self.by[bi] else self.by[bi]
        dy = min_val - max_val
        if dy < 0:
            return 0

        return dx * dy

    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b):
        cdef ssize_t i, j
        cdef np.float64_t[:, :] inter

        inter = shapely.area(shapely.intersection(poly_a[:, None], poly_b[None, :]))
        for i in range(inter.shape[0]):
            for j in range(inter.shape[1]):
                if inter[i, j]:
                    result.set(
                        SFM_INDEX(idx_a[i], idx_b[j]),
                        inter[i, j],
                    )


cdef class IoA(Coordinates):
    def __cinit__(self, a not None, b not None, poly=None, np.float64_t bias=1e-10, str denominator='a'):
        self.bias = bias
        self.denominator_a = denominator.lower() == 'a' if isinstance(denominator, str) else denominator <= 0
        if self.poly and not self.denominator_a:
            self.area_bp = (shapely.area(self.bp) + self.bias)

    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil:
        cdef np.float64_t min_val, max_val, dx, dy

        min_val = ax2 if ax2 <= self.bx2[bi] else self.bx2[bi]
        max_val = self.ax[ai] if self.ax[ai] >= self.bx[bi] else self.bx[bi]
        dx = min_val - max_val
        if dx < 0:
            return 0

        min_val = ay2 if ay2 <= self.by2[bi] else self.by2[bi]
        max_val = self.ay[ai] if self.ay[ai] >= self.by[bi] else self.by[bi]
        dy = min_val - max_val
        if dy < 0:
            return 0

        if self.denominator_a:
            return (dx * dy) / (aa + self.bias)
        else:
            return (dx * dy) / (self.ba[bi] + self.bias)

    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b):
        cdef ssize_t i, j
        cdef object[:, :] ap, bp
        cdef np.float64_t[:, :] inter, area
        cdef np.float64_t value

        ap = poly_a[:, None]
        bp = poly_b[None, :]

        inter = shapely.area(shapely.intersection(ap, bp))
        if self.denominator_a:
            area_ap = (shapely.area(ap) + self.bias)

        for i in range(inter.shape[0]):
            for j in range(inter.shape[1]):
                value = inter[i, j]
                if value:
                    i = idx_a[i]
                    j = idx_b[j]

                    if self.denominator_a:
                        result.set(
                            SFM_INDEX(i, j),
                            value / area_ap[i, 0],
                        )
                    else:
                        result.set(
                            SFM_INDEX(i, j),
                            value / self.area_bp[j],
                        )


cdef class IoU(Coordinates):
    def __cinit__(self, a not None, b not None, poly=None, np.float64_t bias=1e-10):
        self.bias = bias

    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil:
        cdef np.float64_t min_val, max_val, dx, dy

        min_val = ax2 if ax2 <= self.bx2[bi] else self.bx2[bi]
        max_val = self.ax[ai] if self.ax[ai] >= self.bx[bi] else self.bx[bi]
        dx = min_val - max_val
        if dx < 0:
            return 0

        min_val = ay2 if ay2 <= self.by2[bi] else self.by2[bi]
        max_val = self.ay[ai] if self.ay[ai] >= self.by[bi] else self.by[bi]
        dy = min_val - max_val
        if dy < 0:
            return 0

        dx *= dy
        return dx / (aa + self.ba[bi] - dx + self.bias)

    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b):
        cdef ssize_t i, j
        cdef object[:, :] ap, bp
        cdef np.float64_t[:, :] inter, union

        ap = poly_a[:, None]
        bp = poly_b[None, :]

        inter = shapely.area(shapely.intersection(ap, bp))
        union = shapely.area(shapely.union(ap, bp))

        for i in range(inter.shape[0]):
            for j in range(inter.shape[1]):
                if inter[i, j]:
                    result.set(
                        SFM_INDEX(idx_a[i], idx_b[j]),
                        inter[i, j] / (union[i, j] + self.bias),
                    )


cdef class PDollar(Coordinates):
    def __cinit__(self, a not None, b not None, poly=None, np.float64_t bias=1e-10, bint use_ignore_method=True):
        self.bias = bias

        if use_ignore_method and 'ignore_method' in b:
            self.region = (np_col(b, 'ignore_method') == 2) & np_col(b, 'ignore')
        else:
            self.region = np_col(b, 'ignore')

    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil:
        cdef np.float64_t min_val, max_val, dx, dy

        min_val = ax2 if ax2 <= self.bx2[bi] else self.bx2[bi]
        max_val = self.ax[ai] if self.ax[ai] >= self.bx[bi] else self.bx[bi]
        dx = min_val - max_val
        if dx < 0:
            return 0

        min_val = ay2 if ay2 <= self.by2[bi] else self.by2[bi]
        max_val = self.ay[ai] if self.ay[ai] >= self.by[bi] else self.by[bi]
        dy = min_val - max_val
        if dy < 0:
            return 0

        dx *= dy
        if self.region[bi]:
            return dx / (aa + self.bias)                        # IoA
        else:
            return dx / (aa + self.ba[bi] - dx + self.bias)     # IoU

    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b):
        cdef ssize_t i, j, size
        cdef object[:, :] ap, bp
        cdef np.float64_t[:, :] inter, area, union

        ap = poly_a[:, None]
        bp = poly_b[None, :]

        inter = shapely.area(shapely.intersection(ap, bp))
        union = shapely.area(shapely.union(ap, bp))
        area = shapely.area(ap)

        size = inter.shape[0]
        for j in range(inter.shape[1]):
            if self.region[idx_b[j]]:
                for i in range(size):
                    if inter[i, j]:
                        result.set(
                            SFM_INDEX(idx_a[i], idx_b[j]),
                            inter[i, j] / (area[i, 0] + self.bias),
                        )
            else:
                for i in range(size):
                    if inter[i, j]:
                        result.set(
                            SFM_INDEX(idx_a[i], idx_b[j]),
                            inter[i, j] / (union[i, j] + self.bias),
                        )
