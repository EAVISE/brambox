#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Statistics based around coordinates of the bounding boxes
#
cimport numpy as np
from libcpp.pair cimport pair
from brambox.util cimport SparseFloatMatrix

cdef class Coordinates:
    cdef bint poly
    cdef ssize_t alen, blen
    cdef np.float64_t[:] ax, bx
    cdef np.float64_t[:] ay, by
    cdef np.float64_t[:] aw, bw
    cdef np.float64_t[:] ah, bh
    cdef object[:] ap, bp
    cdef np.float64_t* bx2
    cdef np.float64_t* by2
    cdef np.float64_t* ba

    cdef void compute(self, SparseFloatMatrix result)
    cdef np.float64_t compute_box(self, ssize_t ai, ssize_t bi, np.float64_t ax2, np.float64_t ay2, np.float64_t aa) nogil
    cdef void compute_poly(self, SparseFloatMatrix result, object[:] poly_a, ssize_t[:] idx_a, object[:] poly_b, ssize_t[:] idx_b)


cdef class Intersection(Coordinates):
    pass


cdef class Union(Coordinates):
    pass


cdef class IoA(Coordinates):
    cdef np.float64_t bias
    cdef bint denominator_a
    cdef np.float64_t[:] area_bp


cdef class IoU(Coordinates):
    cdef np.float64_t bias


cdef class PDollar(Coordinates):
    cdef np.float64_t bias
    cdef np.uint8_t[:] region
