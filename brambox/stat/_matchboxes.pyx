#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Common functions for this package
#
import sys
from enum import Enum
from collections.abc import Iterable
from itertools import chain
import logging
import numpy as np
import pandas as pd
from . import coordinates
from ..util import np_col, DualGroupByNumpy

cimport cython
cimport numpy as np
from brambox.util cimport SparseFloatMatrix
from .coordinates cimport Coordinates, Intersection, IoA, IoU, PDollar

__all__ = ['IgnoreMethod', 'match_anno', 'match_det', 'match_box']
log = logging.getLogger(__name__)
np.import_array()


cdef extern from "<cmath>" namespace "std":
    bint isnan(np.float64_t x) nogil


class IgnoreMethod(Enum):
    """
    Enum containing the different methods to handle ignored annotations.

    Attributes:
        - NONE: The ignore flag of annotations will not be taken into consideration when matching boxes
        - SINGLE: Ignored annotations are considered as a possible match after normal annotations and can only be matched once with a detection
        - MULTIPLE: Ignored annotations are consedered as a possible match after normal annotations and can be matched with multiple detections
    """
    NONE = 0        #: The ignore flag of annotations will not be taken into consideration when matching boxes
    SINGLE = 1      #: Ignored annotations are considered as a possible match after normal annotations and can only be matched once with a detection
    MULTIPLE = 2    #: Ignored annotations are consedered as a possible match after normal annotations and can be matched with multiple detections
    INDIVIDUAL = 3  #: The annotations dataframe should contain an ignore_method column, the different method per annotation


def match_anno(det, anno, threshold, criteria=coordinates.pdollar, class_label=True, ignore=IgnoreMethod.MULTIPLE, **kwargs):
    """
    Compare 2 sets of bounding boxes and return the ``index`` and ``criteria`` of the matched detection for every annotation.

    Args:
        det (pandas.DataFrame): Dataframe with the detection boxes
        anno (pandas.DataFrame): Dataframe with the annotation boxes
        threshold (number): threshold to count a detection as true positive
        criteria (callable, optional): function to compute a criteria value between detection and annotation (eg. IoU); Default :func:`brambox.stat.coordinates.pdollar`
        class_label (boolean, optional): Whether class_labels must be equal to be able to match annotations and detections; Default **True**
        ignore (brambox.stat.IgnoreMethod, optional): How to consider the ignored annotations when matching detections (see note). Default :py:attr:`~brambox.stat.IgnoreMethod.MULTIPLE`
        **kwargs (dict, optional): Extra keyword arguments that are passed on to the *criteria* function

    Returns:
        pandas.DataFrame:
            Annotation dataframe with 2 extra columns **('detection', 'criteria')** that gives the index of the matched detection (NaN if no match) and the matching criteria value (eg. IoU)

    Note:
        If you pass multiple thresholds, the returned dataframe will contain 2 columns **(f'detection-{threshold}', f'criteria-{threshold}')** per threshold.

    Note:
        The criteria function is called with the arguments `det_df, anno_df, **kwargs`,
        where the dataframes contain the detections and annotations of a certain image respectively. |br|
        All the functions in :ref:`brambox.stat.coordinates` are valid criterias, and are optimized to run faster than custom functions.

    Note:
        For this function, there is no difference between :py:attr:`~brambox.stat.IgnoreMethod.SINGLE` and :py:attr:`~brambox.stat.IgnoreMethod.MULTIPLE` ignore mode,
        as we return the index of the matching detection with the highest confidence value. |br|
        There might be a difference to your matching function, so make sure to still use the correct version.
    """
    # Checks
    if isinstance(det['image'].dtype, pd.CategoricalDtype) and isinstance(anno['image'].dtype, pd.CategoricalDtype) and set(det.image.cat.categories) != set(anno.image.cat.categories):
        log.error('Annotation and detection dataframes do not have the same image categories')
    if not det.index.is_unique:
        log.error('Detection indices are not unique')
    if threshold == 0 or (isinstance(threshold, Iterable) and 0 in threshold):
        log.error('One of the threshold values is zero, which is usually a mistake. Make sure to check your criteria function!')

    # Preprocess data
    if not isinstance(threshold, Iterable):
        threshold = np.array((threshold,), dtype=float)
    else:
        threshold = np.array(threshold, dtype=float)
    lt = len(threshold)

    anno = anno.copy()
    if len(anno.index) == 0:
        if lt == 1:
            anno['detection'] = []
            anno['criteria'] = []
        else:
            for t in threshold:
                anno[f'detection-{t}'] = []
                anno[f'criteria-{t}'] = []
        return anno

    det = det.sort_values('confidence', ascending=False)
    det['index'] = det.index.astype('float')

    # Convert class label to integer category
    if class_label:
        anno_cl = np_col(anno, 'class_label')
        det_cl = np_col(det, 'class_label')
        _, cl_keys = np.unique(np.concatenate([anno_cl, det_cl]), return_inverse=True)
        cl_keys = np.split(cl_keys, anno_cl.shape)
        anno['class_key'] = cl_keys[0]
        det['class_key'] = cl_keys[1]

    # Create ignore column
    ignore = IgnoreMethod(ignore)
    if ignore != IgnoreMethod.INDIVIDUAL:
        anno['ignore_method'] = int(ignore.value)
    elif 'ignore_method' not in anno.columns:
        raise AttributeError('IgnoreMethod is set to INDIVIDUAL, but the annotation dataframe does not contain an ignore_method column.')
    elif not pd.api.types.is_integer_dtype(anno.ignore_method):
        anno['ignore_method'] = anno['ignore_method'].map({im: int(im.value) for im in IgnoreMethod})

    fn = ComputeMatches(criteria, threshold, class_label, kwargs)
    res = DualGroupByNumpy(anno, det, 'image').apply(fn, [2*lt], float, False, dict(match_det=False))

    if lt == 1:
        cols = ['detection', 'criteria']
    else:
        cols = [c for t in threshold for c in (f'detection-{t}', f'criteria-{t}')]

    res = pd.DataFrame(res, columns=cols, index=anno.index)
    res = pd.concat([anno, res], axis=1).drop(['ignore_method'], axis=1)

    if class_label:
        res = res.drop(['class_key'], axis=1)

    return res


def match_det(det, anno, threshold, criteria=coordinates.pdollar, class_label=True, ignore=IgnoreMethod.MULTIPLE, **kwargs):
    """ Compare 2 sets of bounding boxes and return the ``True Positives`` and ``False Positives`` of the detection set.

    Args:
        det (pandas.DataFrame): Dataframe with the detection boxes
        anno (pandas.DataFrame): Dataframe with the annotation boxes
        threshold (number or list of numbers): threshold to count a detection as true positive
        criteria (callable, optional): function to compute a criteria value between detection and annotation (eg. IoU); Default :func:`brambox.stat.coordinates.pdollar`
        class_label (boolean, optional): Whether class_labels must be equal to be able to match annotations and detections; Default **True**
        ignore (brambox.stat.IgnoreMethod, optional): How to consider the ignored annotations when matching detections (see note). Default :py:attr:`~brambox.stat.IgnoreMethod.MULTIPLE`
        **kwargs (dict, optional): Extra keyword arguments that are passed on to the *criteria* function

    Returns:
        pandas.DataFrame:
            Detection dataframe with 3 extra columns **('tp', 'fp', 'annotation')** that tells whether that particular detection is a True or False Positive and the matching annotation index if it is a TP.

    Note:
        If you pass multiple thresholds, the returned dataframe will contain 3 columns **(f'tp-{threshold}', f'fp-{threshold}', f'annotation-{threshold}')** per threshold.

    Note:
        The criteria function is called with the arguments `det_df, anno_df, **kwargs`,
        where the dataframes contain the detections and annotations of a certain image respectively. |br|
        All the functions in :ref:`brambox.stat.coordinates` are valid criterias, and are optimized to run faster than custom functions.

    Warning:
        If you have annotations with the ignore property, this will result in detections where both **('tp', 'fp')** columns will be false. |br|
        If a certain detection does not match with a regular annotation, but does match with an ignored annotation,
        we set both columns to false so that they will not be considered when computing further statistics. |br|
        Check out the `Bounding box matching tutorial <../notes/03-A-matching_boxes.html>`_ for more information about matching behaviour.
    """
    # Checks
    if isinstance(det['image'].dtype, pd.CategoricalDtype) and isinstance(anno['image'].dtype, pd.CategoricalDtype) and set(det.image.cat.categories) != set(anno.image.cat.categories):
        log.error('Annotation and detection dataframes do not have the same image categories')
    if not anno.index.is_unique:
        log.error('Annotation indices are not unique')
    if threshold == 0 or (isinstance(threshold, Iterable) and 0 in threshold):
        log.error('One of the threshold values is zero, which is usually a mistake. Make sure to check your criteria function!')

    # Preprocess data
    if not isinstance(threshold, Iterable):
        threshold = np.array((threshold,), dtype=float)
    else:
        threshold = np.array(threshold, dtype=float)
    lt = len(threshold)

    det = det.sort_values('confidence', ascending=False)
    if len(det.index) == 0:
        if lt == 1:
            det['tp'] = []
            det['fp'] = []
            det['annotation'] = []
        else:
            for t in threshold:
                det[f'tp-{t}'] = []
                det[f'fp-{t}'] = []
                det[f'annotation-{t}'] = []
        return det

    anno = anno.copy()
    anno['index'] = anno.index.astype('float')

    # Convert class label to integer category
    if class_label:
        anno_cl = np_col(anno, 'class_label')
        det_cl = np_col(det, 'class_label')
        _, cl_keys = np.unique(np.concatenate([anno_cl, det_cl]), return_inverse=True)
        cl_keys = np.split(cl_keys, anno_cl.shape)
        anno['class_key'] = cl_keys[0]
        det['class_key'] = cl_keys[1]

    # Create ignore column
    ignore = IgnoreMethod(ignore)
    if ignore != IgnoreMethod.INDIVIDUAL:
        anno['ignore_method'] = int(ignore.value)
    elif 'ignore_method' not in anno.columns:
        raise AttributeError('IgnoreMethod is set to INDIVIDUAL, but the annotation dataframe does not contain an ignore_method column.')
    elif not pd.api.types.is_integer_dtype(anno.ignore_method):
        anno['ignore_method'] = anno['ignore_method'].map({im: int(im.value) for im in IgnoreMethod})

    tpfp = ComputeMatches(criteria, threshold, class_label, kwargs)
    res = DualGroupByNumpy(det, anno, 'image').apply(tpfp, [3*lt], float, False, dict(match_det=True))

    if lt == 1:
        cols = ['tp', 'fp', 'annotation']
    else:
        cols = [c for t in threshold for c in (f'tp-{t}', f'fp-{t}', f'annotation-{t}')]

    res = pd.DataFrame(res, columns=cols, index=det.index)
    res = pd.concat([det, res], axis=1)
    for c in chain(cols[::3], cols[1::3]):
        res[c] = res[c].astype(bool)

    if class_label:
        res = res.drop(['class_key'], axis=1)

    return res


def match_box(det, anno, threshold, criteria=coordinates.pdollar, class_label=True, ignore=IgnoreMethod.MULTIPLE, **kwargs):
    """ Convenience function that combines the :func:`~brambox.stat.match_anno` and :func:`~brambox.stat.match_det` functions.
    See those functions for more explanation.

    Args:
        det (pandas.DataFrame): Dataframe with the detection boxes
        anno (pandas.DataFrame): Dataframe with the annotation boxes
        threshold (number): threshold to count a detection as true positive
        criteria (callable, optional): function to compute a criteria value between detection and annotation (eg. IoU); Default :func:`brambox.stat.coordinates.pdollar`
        class_label (boolean, optional): Whether class_labels must be equal to be able to match annotations and detections; Default **True**
        ignore (brambox.stat.IgnoreMethod, optional): How to consider the ignored annotations when matching detections (see note). Default :py:attr:`~brambox.stat.IgnoreMethod.MULTIPLE`
        **kwargs (dict, optional): Extra keyword arguments that are passed on to the *criteria* function

    Returns:
        tuple (pandas.DataFrame, pandas.DataFrame):
            detection and annotation dataframe with extra columns (See :func:`bb.stat.match_anno` and :func:`bb.stat.match_det` for details about the dataframes).

    Note:
        The criteria function is called with the arguments `det_df, anno_df, **kwargs`,
        where the dataframes contain the detections and annotations of a certain image respectively. |br|
        All the functions in :ref:`brambox.stat.coordinates` are valid criterias, and are optimized to run faster than custom functions.
    """
    # Checks
    if isinstance(det['image'].dtype, pd.CategoricalDtype) and isinstance(anno['image'].dtype, pd.CategoricalDtype) and set(det.image.cat.categories) != set(anno.image.cat.categories):
        log.error('Annotation and detection dataframes do not have the same image categories')
    if not det.index.is_unique:
        log.error('Detection indices are not unique')
    if not anno.index.is_unique:
        log.error('Annotation indices are not unique')
    if threshold == 0 or (isinstance(threshold, Iterable) and 0 in threshold):
        log.error('One of the threshold values is zero, which is usually a mistake. Make sure to check your criteria function!')

    # Preprocess data
    if not isinstance(threshold, Iterable):
        threshold = np.array((threshold,), dtype=float)
    else:
        threshold = np.array(threshold, dtype=float)
    lt = len(threshold)

    anno = anno.copy()
    anno['index'] = anno.index.astype('float')
    det = det.sort_values('confidence', ascending=False)
    det['index'] = det.index.astype('float')

    # Convert class label to integer category
    if class_label:
        anno_cl = np_col(anno, 'class_label')
        det_cl = np_col(det, 'class_label')
        _, cl_keys = np.unique(np.concatenate([anno_cl, det_cl]), return_inverse=True)
        cl_keys = np.split(cl_keys, anno_cl.shape)
        anno['class_key'] = cl_keys[0]
        det['class_key'] = cl_keys[1]

    # Create ignore column
    ignore = IgnoreMethod(ignore)
    if ignore != IgnoreMethod.INDIVIDUAL:
        anno['ignore_method'] = int(ignore.value)
    elif 'ignore_method' not in anno.columns:
        raise AttributeError('IgnoreMethod is set to INDIVIDUAL, but the annotation dataframe does not contain an ignore_method column.')
    elif not pd.api.types.is_integer_dtype(anno.ignore_method):
        anno['ignore_method'] = anno['ignore_method'].map({im: int(im.value) for im in IgnoreMethod})

    cm = ComputeMatches(criteria, threshold, class_label, kwargs)

    if len(det.index) > 0:
        tpfp = DualGroupByNumpy(det, anno, 'image').apply(cm, [3*lt], float, False, dict(match_det=True))

        if lt == 1:
            cols = ['tp', 'fp', 'annotation']
        else:
            cols = [c for t in threshold for c in (f'tp-{t}', f'fp-{t}', f'annotation-{t}')]

        tpfp = pd.DataFrame(tpfp, columns=cols, index=det.index)
        tpfp = pd.concat([det, tpfp], axis=1)
        for c in chain(cols[::3], cols[1::3]):
            tpfp[c] = tpfp[c].astype(bool)
    else:
        tpfp = det.copy()
        if lt == 1:
            tpfp['tp'] = []
            tpfp['fp'] = []
            tpfp['annotation'] = []
        else:
            for t in threshold:
                tpfp[f'tp-{t}'] = []
                tpfp[f'fp-{t}'] = []
                tpfp[f'annotation'] = []

    if len(anno.index) > 0:
        fn = DualGroupByNumpy(anno, det, 'image').apply(cm, [2*lt], float, False, dict(match_det=False))

        if lt == 1:
            cols = ['detection', 'criteria']
        else:
            cols = [c for t in threshold for c in (f'detection-{t}', f'criteria-{t}')]

        fn = pd.DataFrame(fn, columns=cols, index=anno.index)
        fn = pd.concat([anno, fn], axis=1)
    else:
        fn = anno.copy()
        if lt == 1:
            fn['detection'] = []
            fn['criteria'] = []
        else:
            for t in threshold:
                fn[f'detection-{t}'] = []
                fn[f'criteria-{t}'] = []

    tpfp = tpfp.drop(['index'], axis=1)
    fn = fn.drop(['ignore_method'], axis=1)
    if class_label:
        tpfp = tpfp.drop(['class_key'], axis=1)
        fn = fn.drop(['class_key'], axis=1)

    return tpfp, fn


@cython.final
cdef class ComputeMatches:
    cdef object criteria
    cdef np.float64_t[:] threshold
    cdef bint class_label
    cdef bint match_det
    cdef dict kwargs
    cdef int fast_apply
    cdef double nan

    def __cinit__(self, criteria, np.float64_t[:] threshold not None, bint class_label, dict kwargs):
        self.criteria = criteria
        self.threshold = threshold
        self.class_label = class_label
        self.kwargs = kwargs
        self.nan = np.nan

    def __call__(self, dict df1 not None, dict df2 not None, bint match_det):
        if match_det:
            if self.criteria == coordinates.intersection:
                return self.fast_tpfp(df1, df2, Intersection(df1, df2, **self.kwargs)).base
            elif self.criteria == coordinates.ioa:
                return self.fast_tpfp(df1, df2, IoA(df1, df2, **self.kwargs)).base
            elif self.criteria == coordinates.iou:
                return self.fast_tpfp(df1, df2, IoU(df1, df2, **self.kwargs)).base
            elif self.criteria == coordinates.pdollar:
                return self.fast_tpfp(df1, df2, PDollar(df1, df2, **self.kwargs)).base
            else:
                return self.slow_tpfp(df1, df2, np.asarray(self.criteria(pd.DataFrame(df1), pd.DataFrame(df2), **self.kwargs))).base
        else:
            if self.criteria == coordinates.intersection:
                return self.fast_fn(df1, df2, Intersection(df2, df1, **self.kwargs)).base
            elif self.criteria == coordinates.ioa:
                return self.fast_fn(df1, df2, IoA(df2, df1, **self.kwargs)).base
            elif self.criteria == coordinates.iou:
                return self.fast_fn(df1, df2, IoU(df2, df1, **self.kwargs)).base
            elif self.criteria == coordinates.pdollar:
                return self.fast_fn(df1, df2, PDollar(df2, df1, **self.kwargs)).base
            else:
                return self.slow_fn(df1, df2, np.asarray(self.criteria(pd.DataFrame(df2), pd.DataFrame(df1), **self.kwargs))).base

    cdef inline np.float64_t[:, :] fast_tpfp(self, dict det, dict anno, Coordinates criteria):
        cdef np.int64_t[:] dcl, acl, aim
        cdef np.uint8_t[:] ai
        cdef np.float64_t[:] aidx
        cdef np.float64_t[:, :] tpfp
        cdef size_t i, j, t, max_val_idx, numdet, numanno, numthresh
        cdef int ignoremethod
        cdef np.float64_t value, max_val
        cdef SparseFloatMatrix crit_mask, crit_mask_copy

        if self.class_label:
            dcl = det['class_key']
            acl = anno['class_key']
        ai = anno['ignore']
        aim = anno['ignore_method']
        aidx = anno['index']

        numdet = det['x_top_left'].shape[0]
        numanno = aidx.shape[0]
        numthresh = self.threshold.shape[0]

        tpfp = np.tile(np.array([0, 1, np.nan], dtype=float), (numdet, numthresh))
        if ai.shape[0] == 0 or tpfp.shape[0] == 0:
            return tpfp

        crit_mask = SparseFloatMatrix(numdet, numanno, self.nan)
        criteria.compute(crit_mask)

        for t in range(numthresh):
            if t < numthresh-1:
                crit_mask_copy = crit_mask.copy()
            else:
                crit_mask_copy = crit_mask

            for i in range(numdet):
                for j in range(numanno):
                    value = crit_mask_copy.get((i, j))
                    if isnan(value):
                        continue
                    if value < self.threshold[t]:
                        crit_mask_copy.remove((i, j))
                    elif self.class_label and dcl[i] != acl[j]:
                        crit_mask_copy.remove((i, j))
                    elif ai[j] and aim[j]:
                        crit_mask_copy.set((i, j), value - 2)

                max_val = -10
                max_val_idx = -1
                for j in range(numanno):
                    value = crit_mask_copy.get((i, j))
                    if not isnan(value) and value > max_val:
                        max_val_idx = j
                        max_val = value

                if max_val_idx != -1:
                    ignoremethod = aim[max_val_idx]
                    tpfp[i, 3*t+1] = 0
                    tpfp[i, 3*t+2] = aidx[max_val_idx]
                    if ignoremethod == 0 or not ai[max_val_idx]:
                        tpfp[i, 3*t] = 1
                        crit_mask_copy.remove_column(max_val_idx, i)
                    elif ignoremethod == 1:
                        crit_mask_copy.remove_column(max_val_idx, i)

        return tpfp

    cdef inline np.float64_t[:, :] fast_fn(self, dict anno, dict det, Coordinates criteria):
        cdef np.float64_t[:, :] fn
        cdef np.float64_t[:] didx
        cdef np.int64_t[:] dcl, acl, aim
        cdef np.uint8_t[:] ai
        cdef size_t i, j, t, max_val_idx, numdet, numanno, numthresh
        cdef np.float64_t value, max_val, max_val_real
        cdef SparseFloatMatrix crit_mask, crit_mask_copy

        if self.class_label:
            dcl = det['class_key']
            acl = anno['class_key']
        ai = anno['ignore']
        aim = anno['ignore_method']
        didx = det['index']

        numanno = ai.shape[0]
        numdet = didx.shape[0]
        numthresh = self.threshold.shape[0]

        fn = np.full((numanno, 2*numthresh), np.nan, dtype=float)
        fn[:, 1::2] = 0
        if didx.shape[0] == 0 or ai.shape[0] == 0:
            return fn

        crit_mask = SparseFloatMatrix(numdet, numanno, self.nan)
        criteria.compute(crit_mask)

        for t in range(numthresh):
            if t < numthresh-1:
                crit_mask_copy = crit_mask.copy()
            else:
                crit_mask_copy = crit_mask

            for i in range(numdet):
                for j in range(numanno):
                    value = crit_mask_copy.get((i, j))
                    if isnan(value):
                        continue
                    elif value < self.threshold[t]:
                        crit_mask_copy.remove((i, j))
                    elif self.class_label and dcl[i] != acl[j]:
                        crit_mask_copy.remove((i, j))
                    elif ai[j] and aim[j]:
                        crit_mask_copy.set((i, j), value - 2)

                max_val = -10
                max_val_real = 0
                max_val_idx = -1
                for j in range(numanno):
                    value = crit_mask_copy.get((i, j))
                    if not isnan(value) and value > max_val:
                        max_val_idx = j
                        max_val = value
                        max_val_real = value
                        if ai[j] and aim[j]:
                            max_val_real += 2

                if max_val_idx != -1:
                    fn[max_val_idx, 2*t] = didx[i]
                    fn[max_val_idx, 2*t+1] = max_val_real
                    if aim[max_val_idx] != 2 or not ai[max_val_idx]:
                        crit_mask_copy.remove_column(max_val_idx, i)

        return fn

    cdef inline np.float64_t[:, :] slow_tpfp(self, dict det, dict anno, np.float64_t[:, :] crit_mask):
        cdef np.float64_t[:, :] crit_mask_copy
        cdef np.int64_t[:] dcl, acl, aim
        cdef np.uint8_t[:] ai
        cdef np.float64_t[:] aidx
        cdef np.float64_t[:, :] tpfp
        cdef size_t i, j, t, cm_shape0, cm_shape1, max_val_idx, numdet, numthresh
        cdef int ignoremethod
        cdef np.float64_t max_val

        numdet = det['x_top_left'].shape[0]
        numthresh = self.threshold.shape[0]

        if self.class_label:
            dcl = det['class_key']
            acl = anno['class_key']
        aidx = anno['index']
        ai = anno['ignore']
        aim = anno['ignore_method']
        tpfp = np.tile(np.array([0, 1, np.nan], dtype=float), (numdet, numthresh))

        if aidx.shape[0] == 0 or tpfp.shape[0] == 0:
            return tpfp

        cm_shape0 = crit_mask.shape[0]
        cm_shape1 = crit_mask.shape[1]
        for t in range(numthresh):
            if t < numthresh-1:
                crit_mask_copy = crit_mask.copy()
            else:
                crit_mask_copy = crit_mask

            for i in range(cm_shape0):
                for j in range(cm_shape1):
                    if isnan(crit_mask_copy[i, j]):
                        continue
                    if crit_mask_copy[i, j] < self.threshold[t]:
                        crit_mask_copy[i, j] = self.nan
                    elif self.class_label and dcl[i] != acl[j]:
                        crit_mask_copy[i, j] = self.nan
                    elif ai[j] and aim[j]:
                        crit_mask_copy[i, j] -= 2

                max_val = -10
                max_val_idx = -1
                for j in range(cm_shape1):
                    if not isnan(crit_mask_copy[i, j]) and crit_mask_copy[i, j] > max_val:
                        max_val_idx = j
                        max_val = crit_mask_copy[i, j]

                if max_val_idx != -1:
                    ignoremethod = aim[max_val_idx]
                    tpfp[i, 3*t+1] = 0
                    tpfp[i, 3*t+2] = aidx[max_val_idx]
                    if ignoremethod == 0 or not ai[max_val_idx]:
                        tpfp[i, 3*t] = 1
                        for j in range(i, cm_shape0):
                            crit_mask_copy[j, max_val_idx] = self.nan
                    elif ignoremethod == 1:
                        for j in range(i, cm_shape0):
                            crit_mask_copy[j, max_val_idx] = self.nan

        return tpfp

    cdef inline np.float64_t[:, :] slow_fn(self, dict anno, dict det, np.float64_t[:, :] crit_mask):
        cdef np.float64_t[:, :] crit_mask_copy, fn
        cdef np.float64_t[:] didx
        cdef np.int64_t[:] dcl, acl, aim
        cdef np.uint8_t[:] ai
        cdef size_t i, j, t, cm_shape0, cm_shape1, max_val_idx, numanno, numthresh
        cdef np.float64_t max_val, max_val_real

        if self.class_label:
            dcl = det['class_key']
            acl = anno['class_key']
        ai = anno['ignore']
        aim = anno['ignore_method']
        numanno = ai.shape[0]
        numthresh = self.threshold.shape[0]
        didx = det['index']
        fn = np.full((numanno, 2*numthresh), np.nan, dtype=float)
        fn[:, 1::2] = 0

        if didx.shape[0] == 0 or ai.shape[0] == 0:
            return fn

        cm_shape0 = crit_mask.shape[0]
        cm_shape1 = crit_mask.shape[1]
        for t in range(numthresh):
            if t < numthresh-1:
                crit_mask_copy = crit_mask.copy()
            else:
                crit_mask_copy = crit_mask

            for i in range(cm_shape0):
                for j in range(cm_shape1):
                    if isnan(crit_mask_copy[i, j]):
                        continue
                    elif crit_mask_copy[i, j] < self.threshold[t]:
                        crit_mask_copy[i, j] = self.nan
                    elif self.class_label and dcl[i] != acl[j]:
                        crit_mask_copy[i, j] = self.nan
                    elif ai[j] and aim[j]:
                        crit_mask_copy[i, j] -= 2

                max_val = -10
                max_val_real = 0
                max_val_idx = -1
                for j in range(cm_shape1):
                    if not isnan(crit_mask_copy[i, j]) and crit_mask_copy[i, j] > max_val:
                        max_val_idx = j
                        max_val = crit_mask_copy[i, j]
                        max_val_real = crit_mask_copy[i, j]
                        if ai[j] and aim[j]:
                            max_val_real += 2

                if max_val_idx != -1:
                    fn[max_val_idx, 2*t] = didx[i]
                    fn[max_val_idx, 2*t+1] = max_val_real
                    if aim[max_val_idx] != 2 or not ai[max_val_idx]:
                        for j in range(i, cm_shape0):
                            crit_mask_copy[j, max_val_idx] = self.nan

        return fn
