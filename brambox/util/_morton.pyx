#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Morton (Z Order) related functionality
#
import numpy as np

cimport cython
from . cimport _morton

np.import_array()


cdef inline np.uint64_t interleave_bits(np.uint32_t val):
    """ Interleave the bits of the 32-bit input number into a 64-bit output. """
    cdef np.uint64_t result = val                               # ---- ---- ---- ---- ---- ---- ---- ---- abcd efgh ijkl mnop qrst uvwx yz12 3456
    
    result = (result ^ (result << 16)) & 0x0000ffff0000ffff     # ---- ---- ---- ---- abcd efgh ijkl mnop ---- ---- ---- ---- qrst uvwx yz12 3456
    result = (result ^ (result << 8)) & 0x00ff00ff00ff00ff      # ---- ---- abcd efgh ---- ---- ijkl mnop ---- ---- qrst uvwx ---- ---- yz12 3456
    result = (result ^ (result << 4)) & 0x0f0f0f0f0f0f0f0f      # ---- abcd ---- efgh ---- ijkl ---- mnop ---- qrst ---- uvwx ---- yz12 ---- 3456
    result = (result ^ (result << 2)) & 0x3333333333333333      # --ab --cd --ef --gh --ij --kl --mn --op --qr --st --uv --wx --yz --12 --34 --56
    result = (result ^ (result << 1)) & 0x5555555555555555      # -a-b -c-d -e-f -g-h -i-j -k-l -m-n -o-p -q-r -s-t -u-v -w-x -y-z -1-2 -3-4 -5-6

    return result


cdef inline morton_encode(np.uint32_t y, np.uint32_t x):
    return (interleave_bits(y) << 1) | interleave_bits(x)


cdef np.uint64_t morton_index(np.uint32_t y, np.uint32_t x):
    return morton_encode(y, x)


cdef ssize_t[:] morton_argsort_split(np.uint32_t[:] y, np.uint32_t[:] x):
    cdef size_t idx
    cdef np.ndarray[np.uint64_t, ndim=1] result = np.PyArray_EMPTY(1, <np.npy_intp*> x.shape, np.NPY_TYPES.NPY_UINT64, 0)

    for idx in range(x.shape[0]):
        result[idx] = morton_encode(y[idx], x[idx])

    return np.PyArray_ArgSort(result, 0, np.NPY_SORTKIND.NPY_QUICKSORT)


cdef ssize_t[:] morton_argsort_single(np.uint32_t[:, :] points):
    """ points is a 2D array with (x,y) tuples. """
    cdef size_t idx
    cdef np.ndarray[np.uint64_t, ndim=1] result = np.PyArray_EMPTY(1, <np.npy_intp*> points.shape, np.NPY_TYPES.NPY_UINT64, 0)

    for idx in range(points.shape[0]):
        result[idx] = morton_encode(points[idx, 1], points[idx, 0])

    return np.PyArray_ArgSort(result, 0, np.NPY_SORTKIND.NPY_QUICKSORT)
