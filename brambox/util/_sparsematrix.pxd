#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Sparse Matrix implementation
#
cimport numpy as np
from libcpp.pair cimport pair
from libcpp.unordered_map cimport unordered_map


cdef extern from "stdint.h":
    ctypedef unsigned long long uint64_t

ctypedef pair[ssize_t, ssize_t] SFM_INDEX

cdef class SparseFloatMatrix:
    cdef unordered_map[uint64_t, np.float64_t] dok_array
    cdef np.float64_t default
    cdef ssize_t width
    cdef ssize_t height

    cdef void init(self, ssize_t height, ssize_t width, np.float64_t default)
    cpdef SparseFloatMatrix copy(self)
    cpdef np.ndarray[np.float64_t, ndim=2] __array__(self)

    cdef np.float64_t get(self, SFM_INDEX index)
    cdef void set(self, SFM_INDEX index, np.float64_t value)
    cdef void remove(self, SFM_INDEX index)

    cpdef void remove_column(self, ssize_t column, ssize_t start_row=?)
    cpdef void remove_row(self, ssize_t row, ssize_t start_column=?)
    cpdef void set_area(self, pair[size_t, size_t] height, pair[size_t, size_t] width, np.float64_t[:, :] values)
    cpdef void set_indices(self, ssize_t[:] height, ssize_t[:] width, np.float64_t[:] values)
