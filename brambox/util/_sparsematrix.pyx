#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Sparse Matrix implementation
#
import numpy as np

cimport cython
from cython.operator cimport dereference as deref
from cpython.slice cimport PySlice_Unpack
from . cimport _sparsematrix

__all__ = ['SparseFloatMatrix']
np.import_array()


cdef extern from "<cmath>" namespace "std":
    bint isnan(np.float64_t x) nogil


cdef inline uint64_t dok_key(ssize_t height, ssize_t width, SFM_INDEX index):
    cdef uint64_t x, y

    y = <uint64_t> (index.first if index.first >= 0 else height + index.first)
    x = <uint64_t> (index.second if index.second >= 0 else width + index.second)
    return (y << 32) | x


cdef inline SFM_INDEX dok_index(np.uint64_t index):
    cdef ssize_t x, y

    y = (index >> 32) & (<ssize_t> 0xFFFFFFFF)
    x = index & (<ssize_t> 0xFFFFFFFF)
    return SFM_INDEX(y, x)


cdef inline bint equal(np.float64_t val1, np.float64_t val2):
    return (val1 == val2) or (isnan(val1) and isnan(val2))


@cython.final
cdef class SparseFloatMatrix:
    """
    Sparse Matrix for floating point values, implemented in Cython for faster usage. |br|
    This sparse matrix is implemented as a 'dictionary of keys'.

    While you are free to use this class, it is meant as an internal implementation detail for computing coordinate statistics.
    Documentation may thus be lacking.

    Args:
        height: Height of the matrix
        width: Width of the matrix
        default: Default value of the matrix

    Functions:
        copy:
            Copy matrix to new object.
        remove_row:
            Remove a (partial) row of the SFM, starting from a specific index.
        remove_column:
            Remove a (partial) column of the SFM, starting from a specific index.
        set_area:
            Set values for a specific rectangular area in the SFM (Warning: no bounds checking).
        set_indices:
            Set multiple values in the SFM (Warning: no bounds checking).
        shape:
            property that returns the shape of the SFM (similarly to numpy arrays).
        __array__:
            Implements the numpy array interface so we can use `np.asarray()` to get the full numpy array representation.
        __getitem__:
            Gets certain items from the matrix.
            Requires a `y, x` tuple, where the values can be indices or slices.
        __setitem__:
            Sets (or removes if the value is the default one) certain items from the matrix.
            Requires a `y, x` tuple, where the values can be indices or slices.
        __str__:
            Short string representation with some details about the matrix.
        __repr__:
            Long string representation with all set values.

    Warning:
        When setting or getting items, no bounds checking is performed!
    """
    def __cinit__(self, ssize_t height, ssize_t width, np.float64_t default=0):
        cdef ssize_t count

        self.init(height, width, default)

        count = height * width
        self.dok_array.reserve(500 * count / (500 + count))

    cdef void init(self, ssize_t height, ssize_t width, np.float64_t default):
        self.height = height
        self.width = width
        self.default = default

    cpdef SparseFloatMatrix copy(self):
        cdef SparseFloatMatrix sfm

        sfm = SparseFloatMatrix.__new__(SparseFloatMatrix, 0, 0)
        sfm.init(self.height, self.width, self.default)
        sfm.dok_array = unordered_map[uint64_t, np.float64_t](self.dok_array)

        return sfm

    cpdef np.ndarray[np.float64_t, ndim=2] __array__(self):
        cdef np.float64_t[:, :] result
        cdef pair[uint64_t, np.float64_t] it
        cdef SFM_INDEX index

        result = np.full((self.height, self.width), self.default, dtype=np.float64)
        for it in self.dok_array:
            index = dok_index(it.first)
            result[index.first, index.second] = it.second

        return result.base

    cdef np.float64_t get(self, SFM_INDEX index):
        cdef unordered_map[uint64_t, np.float64_t].iterator it

        it = self.dok_array.find(dok_key(self.height, self.width, index))
        if it == self.dok_array.end():
            return self.default
        return deref(it).second

    cdef void set(self, SFM_INDEX index, np.float64_t value):
        self.dok_array[dok_key(self.height, self.width, index)] = value

    cdef void remove(self, SFM_INDEX index):
        self.dok_array.erase(dok_key(self.height, self.width, index))

    cpdef void remove_column(self, ssize_t column, ssize_t start_row=0):
        cdef ssize_t row

        if column < 0:
            column += self.width
        if start_row < 0:
            start_row += self.height

        for row in range(start_row, self.height):
            self.dok_array.erase(dok_key(self.height, self.width, (row, column)))

    cpdef void remove_row(self, ssize_t row, ssize_t start_column=0):
        cdef ssize_t column

        if row < 0:
            row += self.height
        if start_column < 0:
            start_column += self.width

        for column in range(start_column, self.width):
            self.dok_array.erase(dok_key(self.height, self.width, (row, column)))

    cpdef void set_area(self, pair[size_t, size_t] height, pair[size_t, size_t] width, np.float64_t[:, :] values):
        cdef ssize_t y, x
        cdef np.float64_t val

        for y in range(height.first, height.second):
            for x in range(width.first, width.second):
                val = values[y - height.first, x - width.first]
                if equal(val, self.default):
                    self.remove(SFM_INDEX(y, x))
                else:
                    self.set(SFM_INDEX(y, x), val)

    cpdef void set_indices(self, ssize_t[:] height, ssize_t[:] width, np.float64_t[:] values):
        cdef ssize_t y, x
        cdef np.float64_t val

        num_items = min(width.shape[0], height.shape[0], values.shape[0])
        for idx in range(num_items):
            val = values[idx]
            y = height[idx]
            x = width[idx]

            if equal(val, self.default):
                self.remove(SFM_INDEX(y, x))
            else:
                self.set(SFM_INDEX(y, x), val)

    def __getitem__(self, tuple index):
        cdef ssize_t y_len, y_start, y_step, y
        cdef ssize_t x_len, x_start, x_step, x
        cdef np.float64_t[:, :] res2d
        cdef np.float64_t[:] res1d
        y_idx, x_idx = index

        if isinstance(y_idx, slice) and isinstance(x_idx, slice):
            PySlice_Unpack(y_idx, <Py_ssize_t*>&y_start, <Py_ssize_t*>&y_len, <Py_ssize_t*>&y_step)
            y_len = len(range(*y_idx.indices(self.height)))
            PySlice_Unpack(x_idx, <Py_ssize_t*>&x_start, <Py_ssize_t*>&x_len, <Py_ssize_t*>&x_step)
            x_len = len(range(*x_idx.indices(self.width)))
            res2d = np.empty((y_len, x_len), dtype=np.float64)

            for y in range(y_len):
                for x in range(x_len):
                    res2d[y, x] = self.get(SFM_INDEX(y_start + y * y_step, x_start + x * x_step))

            return res2d.base

        if isinstance(y_idx, slice):
            PySlice_Unpack(y_idx, <Py_ssize_t*>&y_start, <Py_ssize_t*>&y_len, <Py_ssize_t*>&y_step)
            y_len = len(range(*y_idx.indices(self.height)))
            x = x_idx
            res1d = np.empty(y_len, dtype=np.float64)

            for y in range(y_len):
                res1d[y] = self.get(SFM_INDEX(y_start + y * y_step, x))

            return res1d.base

        if isinstance(x_idx, slice):
            y = y_idx
            PySlice_Unpack(x_idx, <Py_ssize_t*>&x_start, <Py_ssize_t*>&x_len, <Py_ssize_t*>&x_step)
            x_len = len(range(*x_idx.indices(self.width)))
            res1d = np.empty(x_len, dtype=np.float64)

            for x in range(x_len):
                res1d[x] = self.get(SFM_INDEX(y, x_start + x * x_step))

            return res1d.base

        y = y_idx
        x = x_idx
        return self.get(SFM_INDEX(y, x))

    def __setitem__(self, tuple index, np.float64_t value):
        cdef ssize_t y_len, y_start, y_step, y
        cdef ssize_t x_len, x_start, x_step, x
        y_idx, x_idx = index

        if isinstance(y_idx, slice) and isinstance(x_idx, slice):
            PySlice_Unpack(y_idx, <Py_ssize_t*>&y_start, <Py_ssize_t*>&y_len, <Py_ssize_t*>&y_step)
            y_len = len(range(*y_idx.indices(self.height)))
            PySlice_Unpack(x_idx, <Py_ssize_t*>&x_start, <Py_ssize_t*>&x_len, <Py_ssize_t*>&x_step)
            x_len = len(range(*x_idx.indices(self.width)))

            if equal(value, self.default):
                for y in range(y_len):
                    for x in range(x_len):
                        self.remove(SFM_INDEX(y_start + y * y_step, x_start + x * x_step))
            else:
                for y in range(y_len):
                    for x in range(x_len):
                        self.set(SFM_INDEX(y_start + y * y_step, x_start + x * x_step), value)

            return

        if isinstance(y_idx, slice):
            PySlice_Unpack(y_idx, <Py_ssize_t*>&y_start, <Py_ssize_t*>&y_len, <Py_ssize_t*>&y_step)
            y_len = len(range(*y_idx.indices(self.height)))
            x = x_idx

            if equal(value, self.default):
                for y in range(y_len):
                    self.remove(SFM_INDEX(y_start + y * y_step, x))
            else:
                for y in range(y_len):
                    self.set(SFM_INDEX(y_start + y * y_step, x), value)

            return

        if isinstance(x_idx, slice):
            y = y_idx
            PySlice_Unpack(x_idx, <Py_ssize_t*>&x_start, <Py_ssize_t*>&x_len, <Py_ssize_t*>&x_step)
            x_len = len(range(*x_idx.indices(self.width)))

            if equal(value, self.default):
                for x in range(x_len):
                    self.remove(SFM_INDEX(y, x_start + x * x_step))
            else:
                for x in range(x_len):
                    self.set(SFM_INDEX(y, x_start + x * x_step), value)

            return

        y = y_idx
        x = x_idx
        if equal(value, self.default):
            self.remove(SFM_INDEX(y, x))
        else:
            self.set(SFM_INDEX(y, x), value)

    @property
    def shape(self):
        return (self.height, self.width)

    def __str__(self):
        return f'{self.__class__.__name__}({self.height}x{self.width}, default={self.default}, items={self.dok_array.size()}, size={self.dok_array.bucket_count()})'

    def __repr__(self):
        cdef pair[uint64_t, np.float64_t] it
        cdef SFM_INDEX index

        repr = f'{self.__class__.__name__}(\n  {self.height}x{self.width},\n  default={self.default},\n  size={self.dok_array.bucket_count()}'
        for it in self.dok_array:
            index = dok_index(it.first)
            repr += f'\n  {index.first, index.second}: {it.second}'

        return repr + '\n)'
