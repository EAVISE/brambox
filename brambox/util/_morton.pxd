#cython: language_level=3, boundscheck=False, initializedcheck=False, wraparound=False, cdivision=True
#distutils: language=c++
#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Morton (Z Order) related functionality
#
cimport numpy as np


cdef np.uint64_t morton_index(np.uint32_t y, np.uint32_t x)
cdef ssize_t[:] morton_argsort_split(np.uint32_t[:] y, np.uint32_t[:] x)
cdef ssize_t[:] morton_argsort_single(np.uint32_t[:, :] points)
