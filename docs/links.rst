.. LINKS
.. _brambox: https://www.gitlab.com/EAVISE/brambox/
.. _python: https://docs.python.org/3.6/
.. _pillow: https://pillow.readthedocs.io/en/3.1.x/
.. _pillow_simd: https://github.com/uploadcare/pillow-simd
.. _opencv: https://opencv.org/
.. _matplotlib: https://matplotlib.org/
.. _pypi: https://pypi.org/project/brambox
.. _lrp: https://arxiv.org/pdf/1807.01696.pdf
.. _cocoapi: https://github.com/cocodataset/cocoapi
.. _tidecv: https://github.com/dbolya/tide

.. FORMAT LINKS
.. _coco_anno: http://cocodataset.org/#format-data
.. _coco_det: http://cocodataset.org/#format-results
.. _cvat: https://docs.cvat.ai/docs/manual/advanced/xml_format/
.. _cvc: http://adas.cvc.uab.es/elektra/datasets/pedestrian-detection/
.. _dollar: https://github.com/pdollar/toolbox/blob/master/detector/bbGt.m
.. _kitti: https://www.cvlibs.net/datasets/kitti/eval_object.php?obj_benchmark=2d
.. _pascal voc: http://host.robots.ox.ac.uk/pascal/VOC/
.. _vatic: https://github.com/cvondrick/vatic
.. _erti: https://sites.google.com/site/uavision2019/challenge
.. _hrsc: https://www.kaggle.com/datasets/guofeng/hrsc2016

.. DIRECTIVES
.. |br| raw:: html

   <br />
