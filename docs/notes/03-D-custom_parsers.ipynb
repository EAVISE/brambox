{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Custom parsers\n",
    "This notebook will show how to write parsers for any format that is not supported out of the box by brambox."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import contextlib\n",
    "import os\n",
    "\n",
    "from IPython.display import display\n",
    "\n",
    "import brambox as bb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parsing mechanics\n",
    "At the most basic level, parsers are nothing more than class objects that implement 2 different methods `serialize()` and `deserialize()`, which perform the conversion between serialized file format and pandas dataframe.\n",
    "They then provide some additional properties, allowing the brambox [load()](../api/generated/brambox.io.load.rst) and [save()](../api/generated/brambox.io.save.rst) functions to know how to handle these parsers.\n",
    "Have a look at the [base parser class](../api/generated/brambox.io.parser.Parser.rst) for a full list of properties you can set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Deserialization\n",
    "The `deserialize()` function recieves the raw input data (eg. string) and needs to extract the different data fields from those strings and add them to the data dictionary.\n",
    "The base parser class offers 2 functions to help with this task:\n",
    "\n",
    "- [append_image()](../api/generated/brambox.io.parser.Parser.rst#brambox.io.parser.Parser.append_image): This function will store the image name you pass to it. This function is only important to add images that do not contain bounding boxes, so that it can be stored inside the categorical image column.\n",
    "- [append()](../api/generated/brambox.io.parser.Parser.rst#brambox.io.parser.Parser.append): This function will add a bounding box to your data. You need to give it the image the box belongs to and all necessary fields (eg. coordinates, class label, ignore property, ...).\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**Note:**  \n",
    "If the data you are parsing has additional properties you would like to store in the dataframe, you can do so by calling the [add_column()](../api/generated/brambox.io.parser.Parser.rst#brambox.io.parser.Parser.add_column) function. This is usually done in the constructor of your parser.\n",
    "Once you added an extra column, you just need to pass it as a keyword argument to the [append()](../api/generated/brambox.io.parser.Parser.rst#brambox.io.parser.Parser.append) function for every bounding box.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Serialization\n",
    "The `serialize()` function is responsible for generating the serialized raw data from a dataframe.\n",
    "However, it is slightly more complicated than the `deserialize()` function.\n",
    "\n",
    "In stead of passing the entire dataframe to this function, we first group the dataframe by a certain column, which is defined by the [serialize_group](../api/generated/brambox.io.parser.Parser.rst#brambox.io.parser.Parser.serialize_group) property of your parser. The default value for this property is to group the dataframe per image.\n",
    "The `serialize()` function must then generate the raw data that matches that grouped dataframe (eg. string).  \n",
    "The brambox [save()](../api/generated/brambox.io.save.rst) function will then use some of the remaining properties (eg. header, footer, ...) of the parser to save this data to the correct file(s)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Parser types\n",
    "To start writing your own parser, you will create a new class that inherits from one of these parsers:\n",
    "\n",
    "- [AnnotationParser](../api/generated/brambox.io.parser.AnnotationParser.rst) : The parser you are implementing performs IO on an annotation format\n",
    "- [DetectionPArser](../api/generated/brambox.io.parser.DetectionParser.rst)  : The parser you are implementing performs IO on a detection format\n",
    "- [Parser](../api/generated/brambox.io.parser.Parser.rst) : The parser you are implementing performs IO on a format that can be both annotations or detections (Examples of these kind of parsers can be found in the [Generic parsers](../api/io.rst#module-brambox.io.parser.generic) section)\n",
    "\n",
    "Once you know which parser to inherit from, you need to define what the [ParserType](../api/generated/brambox.io.parser.ParserType.rst) of your parser is.  \n",
    "This section of the tutorial will discuss each kind of type and show a minimal example on how to use them."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single file parsers\n",
    "Single file parsers store all data in a single file, and thus provide all necessary properties for all bounding boxes.  \n",
    "In order to demonstrate the single file parser, we will define a dummy file format for storing car annotations.\n",
    "The format is defined as:\n",
    "\n",
    "```\n",
    "<image_name>; <car type>; <car color>; <x>; <y>; <w>; <h>; <occluded flag (0/1)>\n",
    "```\n",
    "\n",
    "We will first create a file that has some dummy data according to our format specification"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('/tmp/dummy_anno.txt', 'w') as f:\n",
    "    f.write('img01; car;   red;   10;  15; 175; 70;  0\\n')\n",
    "    f.write('img01; truck; gray;  100; 50; 250; 100; 1\\n')\n",
    "    f.write('img02; bus;   white; 5;   6;  500; 125; 0\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have some data to load, we can write our parser!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "code_folding": []
   },
   "outputs": [],
   "source": [
    "class CarParser(bb.io.AnnotationParser):\n",
    "    parser_type = bb.io.ParserType.SINGLE_FILE\n",
    "    extension = '.txt'\n",
    "    # For this parser, there is no point in serializing per group, so we set it to none\n",
    "    serialize_group = None\n",
    "    # We want to combine the different strings with a newline when serializing\n",
    "    serialize_group_separator = '\\n'\n",
    "\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "        self.add_column('color')  # Add a color column, which will hold the car color property and has a default value -\n",
    "        self.add_column('occluded', 0, int)  # Add occluded column, with default value 0 and type of int\n",
    "\n",
    "    def deserialize(self, rawdata):\n",
    "        \"\"\" parse raw string data -> dataframe \"\"\"\n",
    "        for line in rawdata.splitlines():\n",
    "            # Get individual items\n",
    "            elements = [item.strip() for item in line.split(';')]\n",
    "\n",
    "            # Add image (not strictly necessary as each image has a bounding box for sure in this format)\n",
    "            self.append_image(elements[0])\n",
    "\n",
    "            # Add bounding box\n",
    "            self.append(\n",
    "                # image\n",
    "                elements[0],\n",
    "\n",
    "                # properties\n",
    "                class_label=elements[1],\n",
    "                x_top_left=float(elements[3]),\n",
    "                y_top_left=float(elements[4]),\n",
    "                width=float(elements[5]),\n",
    "                height=float(elements[6]),\n",
    "                occluded=(elements[7] == '1'),\n",
    "\n",
    "                # Extra properties\n",
    "                color=elements[2]\n",
    "            )\n",
    "\n",
    "    def pre_serialize(self, df):\n",
    "        \"\"\" This function gets the entire dataframe\n",
    "            and must return a (modified) dataframe that will be serialized\n",
    "        \"\"\"\n",
    "        # Add color column if necessary (Note that you can also simply set a default value '-' in the add_column() call, which will handle everything automatically)\n",
    "        if 'color' not in df:\n",
    "            df['color'] = '-'\n",
    "\n",
    "        return df\n",
    "\n",
    "    def serialize(self, row):\n",
    "        \"\"\" parse row -> string\n",
    "            We set `serialize_group` to None, so we get the data row by row\n",
    "        \"\"\"\n",
    "        return (  # This creates a single string (just split up to fit on single line in editor)\n",
    "            f'{row.image}; {row.class_label}; {row.color}; '\n",
    "            f'{row.x_top_left}; {row.y_top_left}; {row.width}; {row.height}; {row.occluded}'\n",
    "        )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test this parser by loading our dummy data, modifying a value and saving it as another file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>image</th>\n",
       "      <th>class_label</th>\n",
       "      <th>x_top_left</th>\n",
       "      <th>y_top_left</th>\n",
       "      <th>width</th>\n",
       "      <th>height</th>\n",
       "      <th>ignore</th>\n",
       "      <th>color</th>\n",
       "      <th>occluded</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>img01</td>\n",
       "      <td>car</td>\n",
       "      <td>10.0</td>\n",
       "      <td>15.0</td>\n",
       "      <td>175.0</td>\n",
       "      <td>70.0</td>\n",
       "      <td>False</td>\n",
       "      <td>red</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>img01</td>\n",
       "      <td>truck</td>\n",
       "      <td>100.0</td>\n",
       "      <td>50.0</td>\n",
       "      <td>250.0</td>\n",
       "      <td>100.0</td>\n",
       "      <td>False</td>\n",
       "      <td>gray</td>\n",
       "      <td>1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>img02</td>\n",
       "      <td>bus</td>\n",
       "      <td>5.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>500.0</td>\n",
       "      <td>125.0</td>\n",
       "      <td>False</td>\n",
       "      <td>white</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   image class_label  x_top_left  y_top_left  width  height  ignore  color  \\\n",
       "0  img01         car        10.0        15.0  175.0    70.0   False    red   \n",
       "1  img01       truck       100.0        50.0  250.0   100.0   False   gray   \n",
       "2  img02         bus         5.0         6.0  500.0   125.0   False  white   \n",
       "\n",
       "   occluded  \n",
       "0         0  \n",
       "1         1  \n",
       "2         0  "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "img01; car; red; 10.0; 15.0; 175.0; 70.0; 0\n",
      "img01; truck; gray; 100.0; 50.0; 250.0; 100.0; 1\n",
      "img02; bus; orange; 5.0; 6.0; 500.0; 125.0; 0"
     ]
    }
   ],
   "source": [
    "# load data\n",
    "df = bb.io.load(CarParser, '/tmp/dummy_anno.txt')\n",
    "display(df)\n",
    "\n",
    "# modify color of second row\n",
    "df.loc[2, 'color'] = 'orange'\n",
    "\n",
    "# Save data\n",
    "bb.io.save(df, CarParser, '/tmp/dummy_anno2.txt')\n",
    "\n",
    "# Show saved file\n",
    "!cat /tmp/dummy_anno2.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multi file parsers\n",
    "Multi file parsers store your data in multiple files and thus have one property for your bounding boxes in common for a file. This is usually the image the file belongs to, but could also be the class label or any other property.  \n",
    "Let's define a car detection file format that to work with our annotation format:\n",
    "\n",
    "```\n",
    "<image_name>.txt:\n",
    "  <car type>; <car color>; <x>; <y>; <w>; <h>; <detection confidence>; <color confidence>\n",
    "```\n",
    "\n",
    "We will now create a couple of dummy files with this format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "with contextlib.suppress(FileExistsError):\n",
    "    os.mkdir('/tmp/detections')\n",
    "\n",
    "with open('/tmp/detections/img01.txt', 'w') as f:\n",
    "    f.write('car;   red;   10;  15; 175; 70;  0.7; 0.75\\n')\n",
    "    f.write('truck; gray;  100; 50; 250; 100; 0.8; 0.33\\n')\n",
    "\n",
    "with open('/tmp/detections/img02.txt', 'w') as f:\n",
    "    f.write('bus;   white; 5; 6; 500; 125; 0.6; 0.96\\n')\n",
    "    f.write('truck; white; 2; 8; 490; 132; 0.2; 0.87\\n')\n",
    "\n",
    "with open('/tmp/detections/img03.txt', 'w') as f:\n",
    "    # Empty file\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just like with the annotations we will now create a parser for these files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "@bb.io.register_parser('car')\n",
    "class CarDetParser(bb.io.DetectionParser):\n",
    "    parser_type = bb.io.ParserType.MULTI_FILE\n",
    "    extension = '.txt'\n",
    "\n",
    "    # We want to serialize per image, as this is how the files are stored\n",
    "    serialize_group = 'image'\n",
    "\n",
    "    def __init__(self, precision=None):\n",
    "        super().__init__()\n",
    "        self.precision = precision  # Rounding precision for confidence, default to None->integer\n",
    "\n",
    "        self.add_column('color')\n",
    "        self.add_column('color_confidence')\n",
    "\n",
    "    def deserialize(self, rawdata, file_id):\n",
    "        \"\"\" parse raw string data -> dataframe\n",
    "            Since this is a multifile format, we get the file_id (name of file without extension) as an argument.\n",
    "        \"\"\"\n",
    "        # Add image (for files without boxes, like img03)\n",
    "        self.append_image(file_id)\n",
    "\n",
    "        for line in rawdata.splitlines():\n",
    "            # Get individual items\n",
    "            elements = [item.strip() for item in line.split(';')]\n",
    "\n",
    "            # Add bounding box\n",
    "            self.append(\n",
    "                # image\n",
    "                file_id,\n",
    "\n",
    "                # properties\n",
    "                class_label=elements[0],\n",
    "                x_top_left=float(elements[2]),\n",
    "                y_top_left=float(elements[3]),\n",
    "                width=float(elements[4]),\n",
    "                height=float(elements[5]),\n",
    "                confidence=float(elements[6]),\n",
    "\n",
    "                # Extra properties\n",
    "                color=elements[1],\n",
    "                color_confidence=float(elements[7])\n",
    "            )\n",
    "\n",
    "    def serialize(self, df):\n",
    "        \"\"\" parse dataframe -> string\n",
    "            We set `serialize_group` property to 'image', so here we get a (sub-)dataframe\n",
    "            with the elements from one certain image (this function gets called as `groupby().apply()`)\n",
    "        \"\"\"\n",
    "        result = ''\n",
    "\n",
    "        for row in df.itertuples():\n",
    "            result += (\n",
    "                f'{row.class_label}; {row.color}; '\n",
    "                f'{int(row.x_top_left)}; {int(row.y_top_left)}; {int(row.width)}; {int(row.height)}; '\n",
    "                f'{round(row.confidence, self.precision)}; {round(row.color_confidence, self.precision)}\\n'\n",
    "            )\n",
    "\n",
    "        return result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**Note:**  \n",
    "On the last line of our parser code, we registered the new parser in brambox.  \n",
    "Among other things, this means we can use it in the save/load functions by writing its string classifier.\n",
    "As this is a detection format, it will be stored in the [detection_formats](../api/io.rst#formats) dictionary as 'car' and in the [formats](../api/io.rst#formats) dictionary as 'det_car'.\n",
    "\n",
    "</div>\n",
    "\n",
    "We will test this parser by loading our dummy data, filtering out boxes based on detections, and saving it again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>image</th>\n",
       "      <th>class_label</th>\n",
       "      <th>x_top_left</th>\n",
       "      <th>y_top_left</th>\n",
       "      <th>width</th>\n",
       "      <th>height</th>\n",
       "      <th>confidence</th>\n",
       "      <th>color</th>\n",
       "      <th>color_confidence</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>img01</td>\n",
       "      <td>car</td>\n",
       "      <td>10.0</td>\n",
       "      <td>15.0</td>\n",
       "      <td>175.0</td>\n",
       "      <td>70.0</td>\n",
       "      <td>0.7</td>\n",
       "      <td>red</td>\n",
       "      <td>0.75</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>img01</td>\n",
       "      <td>truck</td>\n",
       "      <td>100.0</td>\n",
       "      <td>50.0</td>\n",
       "      <td>250.0</td>\n",
       "      <td>100.0</td>\n",
       "      <td>0.8</td>\n",
       "      <td>gray</td>\n",
       "      <td>0.33</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>img02</td>\n",
       "      <td>bus</td>\n",
       "      <td>5.0</td>\n",
       "      <td>6.0</td>\n",
       "      <td>500.0</td>\n",
       "      <td>125.0</td>\n",
       "      <td>0.6</td>\n",
       "      <td>white</td>\n",
       "      <td>0.96</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>img02</td>\n",
       "      <td>truck</td>\n",
       "      <td>2.0</td>\n",
       "      <td>8.0</td>\n",
       "      <td>490.0</td>\n",
       "      <td>132.0</td>\n",
       "      <td>0.2</td>\n",
       "      <td>white</td>\n",
       "      <td>0.87</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   image class_label  x_top_left  y_top_left  width  height  confidence  \\\n",
       "0  img01         car        10.0        15.0  175.0    70.0         0.7   \n",
       "1  img01       truck       100.0        50.0  250.0   100.0         0.8   \n",
       "2  img02         bus         5.0         6.0  500.0   125.0         0.6   \n",
       "3  img02       truck         2.0         8.0  490.0   132.0         0.2   \n",
       "\n",
       "   color  color_confidence  \n",
       "0    red              0.75  \n",
       "1   gray              0.33  \n",
       "2  white              0.96  \n",
       "3  white              0.87  "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Index(['img01', 'img02', 'img03'], dtype='object')\n",
      "\n",
      "img01.txt\n",
      "car; red; 10; 15; 175; 70; 0.7; 0.75\n",
      "truck; gray; 100; 50; 250; 100; 0.8; 0.33\n",
      "\n",
      "img02.txt\n",
      "bus; white; 5; 6; 500; 125; 0.6; 0.96\n",
      "\n",
      "img03.txt\n"
     ]
    }
   ],
   "source": [
    "# load data\n",
    "df = bb.io.load('det_car', '/tmp/detections')\n",
    "display(df)\n",
    "print(df.image.cat.categories)  # Note the img03 category!\n",
    "\n",
    "# Filter data\n",
    "df = df[(df.confidence > .3) & (df.color_confidence > .2)]\n",
    "\n",
    "# Save data\n",
    "det_dir = '/tmp/detections2/'\n",
    "\n",
    "with contextlib.suppress(FileExistsError):\n",
    "    os.mkdir(det_dir)\n",
    "\n",
    "bb.io.save(df, 'det_car', det_dir, precision=2)  # Round to 2 digits after comma\n",
    "\n",
    "# Show saved file\n",
    "for filename in sorted(os.listdir(det_dir)):\n",
    "    print('\\n' + filename)\n",
    "    !cat {det_dir + filename}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### External parsers\n",
    "The last type of parser, are external parsers.\n",
    "These parser get the actual path to the file(s) and must perform all the necessary IO.\n",
    "You can use this type if none of the above scenarios work for your format.\n",
    "\n",
    "For the deserialization process, you can either use the `append()` and `append_image()` functions, or you can create the dataframe yourself and store it as `self.data` in the parser.\n",
    "\n",
    "<div class=\"alert alert-info\">\n",
    "\n",
    "**Note:**  \n",
    "The *serialize_group* and *serialize_group_separator* have no influence on this kind of parser, as they always get the entire dataframe when serializing the data.\n",
    "\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will not show an example of this type, but rather write a boilerplate class you can use as a starting point to get started!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "@bb.io.register_parser('NAME')\n",
    "class ExternalParser(bb.io.Parser):  # Choose Parser, AnnotationParser or DetectionParser\n",
    "    parser_type = bb.io.ParserType.EXTERNAL\n",
    "\n",
    "    def __init__(self):\n",
    "        super().__init__()\n",
    "\n",
    "    def deserialize(self, path):\n",
    "        \"\"\" Convert data located at `path` to dataframe.\n",
    "            Either use self.append() to build up data, or save dataframe as `self.data` directly\n",
    "        \"\"\"\n",
    "        pass\n",
    "\n",
    "    def serialize(self, df, path):\n",
    "        \"\"\" Save dataframe at location `path` \"\"\"\n",
    "        pass"
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.19"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
