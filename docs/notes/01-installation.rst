Installation
============
The easiest way to install brambox is to get the latest version from pypi_.

.. code-block:: bash

   # Default version
   pip install brambox

   # Segmentation support
   pip install brambox[segmentation]


.. note::
   Brambox has prebuild wheels that were generated with manylinux2014. |br|
   If your pip is too old, it will try to manually build brambox,
   because it does not know how to install these new wheels.

   In order to have a correct build, please make sure to have a pip version >= 19.3. |br|
   You can upgrade your pip with: ``pip install -U pip``.


Installing from source
----------------------
If you want to install a specific branch or version, you can install this package from our brambox_ gitlab source code.

This package uses cython to speed up certain parts of the computations.
This means that there is some C code that needs to be compiled for this package, which makes installing from source a little bit more involved. |br|
However, since we push the generated C files as well as the cython code, users can install our package from source, without needing cython.

.. code-block:: bash

   # Installing development branch in dev-mode (not recommended)
   git clone https://gitlab.com/eavise/brambox ./brambox 
   cd brambox
   git checkout develop
   pip install -r develop.txt

.. warning::
   Using the generated C files does not work on all environments. |br|
   They were generated on an intel machine with Ubuntu 16 and using python 3.9, so your mileage may vary...


If you want to generate the C files from cython, we added a Makefile to make this easier.

.. code-block:: bash
   
   # Requires cython and numpy already installed!
   git clone https://gitlab.com/eavise/brambox
   cd brambox
   make clean
   make build cython=1
   pip install -r requirements.txt


Requirements
------------
The necessary packages to be able to use brambox will be downloaded when installing brambox itself, but there are a few optional dependecies you can choose to install.

segmentation
   If you want segmentation support, you need the pgpd_ library, which you can install by selecting the "segmentation" extra requirement dependencies.

cython and numpy
   If you want to be able to generate the C code from the cython files, you will need to install cython_ and numpy_, before installing brambox.

pyyaml
   If you want to be able to use the YamlParsers, you need to have pyyaml_ installed.

visualization
   If you want to be able to use the visualization functions in brambox, you need to install either pillow_ (or the faster pillow_simd_)
   or opencv_.

pandas files
   Depending on which :py:class:`~brambox.io.parser.generic.PandasParser` file format you want to use, you need to install some different packages.
   (eg. for hdf files, you will need to install the pytables_ package)


.. _cython: https://cython.org/
.. _numpy: https://www.numpy.org/
.. _pyyaml: https://pyyaml.org/wiki/PyYAMLDocumentation
.. _pytables: https://www.pytables.org/
.. _pgpd: https://0phoff.github.io/pygeos-pandas
.. include:: ../links.rst
