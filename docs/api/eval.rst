Evaluation
==========
.. automodule:: brambox.eval

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: allmember-template.rst

   brambox.eval.COCO
   brambox.eval.TIDE
   brambox.eval.TIDAL


.. include:: ../links.rst
