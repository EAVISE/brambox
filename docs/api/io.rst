IO
==
.. automodule:: brambox.io

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: function-template.rst

   brambox.io.load
   brambox.io.save
   brambox.io.register_parser

----

Annotation Parsers
------------------
.. automodule:: brambox.io.parser.annotation

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   brambox.io.parser.annotation.CocoParser
   brambox.io.parser.annotation.CvatImageAnnoParser
   brambox.io.parser.annotation.CvatVideoAnnoParser
   brambox.io.parser.annotation.CvcParser
   brambox.io.parser.annotation.DarknetParser
   brambox.io.parser.annotation.DollarParser
   brambox.io.parser.annotation.KittiParser
   brambox.io.parser.annotation.PascalVocParser
   brambox.io.parser.annotation.VaticParser
   brambox.io.parser.annotation.YamlParser

----

Detection Parsers
-----------------
.. automodule:: brambox.io.parser.detection

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   brambox.io.parser.detection.CocoParser
   brambox.io.parser.detection.DollarParser
   brambox.io.parser.detection.ErtiParser
   brambox.io.parser.detection.PascalVocParser
   brambox.io.parser.detection.YamlParser

----

Generic Parsers
---------------
.. automodule:: brambox.io.parser.generic

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: nomember-template.rst

   brambox.io.parser.generic.PandasParser

----

Others
------
.. currentmodule:: brambox.io.parser

These classes are used to build your own parsers.
Check out our `tutorial <../notes/03-B-custom_parsers.html>`_ to learn how to do this.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: member-template.rst

   brambox.io.parser.Parser
   brambox.io.parser.AnnotationParser
   brambox.io.parser.DetectionParser
   brambox.io.parser.ParserType

----

Formats
-------
The following dictionary contains the default supported parsers and is updated when the :py:func:`~brambox.io.register_parser` function gets called.

.. _brambox.io.formats:
.. dict:: brambox.io formats 

.. include:: ../links.rst
