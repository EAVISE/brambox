Statistics
==========
.. automodule:: brambox.stat


Detection
---------
These functions compute various statistics that are useful for evaluating detection results.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: function-template.rst

   brambox.stat.pr
   brambox.stat.ap
   brambox.stat.fscore
   brambox.stat.lrp
   brambox.stat.mr_fppi
   brambox.stat.lamr


Tracking
--------
The following functions compute statstics necessary for evaluating tracking results.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: function-template.rst

   brambox.stat.mota
   brambox.stat.idf1


Curve
-----
These functions are generic functions that work on the different curve-like dataframes from this library *(eg. PR, F-score, MR_FPPI)*.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: function-template.rst

   brambox.stat.auc
   brambox.stat.auc_interpolated
   brambox.stat.peak
   brambox.stat.point
   brambox.stat.simplify
   brambox.stat.threshold

.. _brambox.stat.coordinates:


Coordinates
-----------
The following functions are lower level functions that can be used to generate overlap matrices between a set of detections and annotations.
They all return a :class:`~brambox.util.SparseFloatMatrix`.

.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: function-template.rst
   
   brambox.stat.coordinates.intersection
   brambox.stat.coordinates.ioa
   brambox.stat.coordinates.iou
   brambox.stat.coordinates.pdollar

Utilitary
---------
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst
   
   brambox.stat.match_anno
   brambox.stat.match_det
   brambox.stat.match_box
   brambox.stat.IgnoreMethod


.. include:: ../links.rst
