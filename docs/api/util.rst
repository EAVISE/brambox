Utilitary
=========
.. automodule:: brambox.util

Dataframe manipulation
----------------------
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst

   brambox.util.new
   brambox.util.from_dict
   brambox.util.concat
   brambox.util.np_col
   brambox.util.select_images
   brambox.util.remove_images
   brambox.util.split_images
   brambox.util.DualGroupBy
   brambox.util.DualGroupByNumpy


Segmentation Mask
-----------------
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst

   brambox.util.BoundingBoxTransformer
   brambox.util.rasterize_segmentation


Visualization
-------------
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst

   draw_boxes
   BoxDrawer
   brambox.util.DrawMethod


Other
-----
.. autosummary::
   :toctree: generated
   :nosignatures:
   :template: mixed-template.rst

   brambox.util.SparseFloatMatrix


.. include:: ../links.rst
