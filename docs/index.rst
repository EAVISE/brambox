.. Brambox documentation master file, created by
   sphinx-quickstart on Tue Dec 26 14:20:36 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

:gitlab_url: https://gitlab.com/EAVISE/brambox

Brambox documentation
======================
Brambox, or Basic Requisites for Algorithms on iMages toolBOX, is a python_ library that provides the necessary tools to convert image annotations, compute statistics and more.
Its main use is for object detection algorithms and datasets.

.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Notes

   notes/01-installation.rst
   notes/02-getting_started.ipynb
   notes/03-advanced_topics.rst

.. toctree::
   :maxdepth: 2
   :caption: API

   brambox.io <api/io>
   brambox.stat <api/stat>
   brambox.util <api/util>
   brambox.eval <api/eval>


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`


.. include:: links.rst
