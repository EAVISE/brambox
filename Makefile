SHELL := /bin/bash
.ONESHELL:
.PHONY: build install wheel clean unittest lint format coverage cprofile docs
.SILENT: build install wheel clean unittest lint format coverage cprofile docs
.NOTPARALLEL: build install wheel clean unittest lint format coverage cprofile docs

####################################################################################################

all: build install
test: unittest lint

####################################################################################################

build: cython := 0
build: debug := 0
build:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	CYTHON=${cython} CDEBUG=${debug} python setup.py build_ext --force --inplace

####################################################################################################

install: version := requirements.txt
install:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	pip install -r ${version}

####################################################################################################

wheel:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	python -m build --sdist
	python -m twine upload dist/*.tar.gz

####################################################################################################

clean:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	python setup.py clean --all
	find brambox -type f -name '*.c' -exec rm {} \;
	find brambox -type f -name '*.cpp' -exec rm {} \;
	find brambox -type f -name '*.so' -exec rm {} \;

####################################################################################################

unittest: file := ./test/
unittest: flags :=
unittest: expr :=
unittest: EXPR := $(if $(strip ${expr}), -k "${expr}",)
unittest: marker := not long
unittest: MARKER := $(if $(strip ${marker}), -m "${marker}",)
unittest: number :=
unittest: NUMBER := $(if $(strip ${number}), -n ${number},)
unittest:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	BB_LOGLVL=warning python -m pytest ${flags} ${NUMBER} ${EXPR} ${MARKER} ${file}

####################################################################################################

lint:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	ruff check
	ruff format --check

####################################################################################################

format:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	ruff format

####################################################################################################

coverage:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	coverage report -m

####################################################################################################

cprofile: folder := cython_anno
cprofile: includes := brambox/stat
cprofile:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	mkdir -p ${folder}
	rm -f ${folder}/*.c ${folder}/*.cpp && rm -f ${folder}/*.html
	cython -I ${includes} -o ${folder} -a $(shell find brambox/ -name '*.pyx')
	rm -f ${folder}/*.c ${folder}/*.cpp

####################################################################################################

docs: notebook:= 0
docs:
	[ -s .venv/bin/activate ] && source .venv/bin/activate
	cd ./docs && JNB=${notebook} make clean html
