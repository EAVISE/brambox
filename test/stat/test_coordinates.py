#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test coordinate functions
#
import math

import numpy as np
import pytest

import brambox as bb


def test_intersection(df_anno_simple, df_det_simple):
    ans = np.asarray(bb.stat.coordinates.intersection(df_det_simple, df_anno_simple))
    assert ans.shape == (3, 3)
    assert list(ans[0]) == [400, 351, 304]
    assert ans[1][1] == 1000
    assert ans[2][2] == 1800

    df_det_simple.at[0, 'x_top_left'] = 15
    df_det_simple.at[1, 'y_top_left'] = 60
    ans = np.asarray(bb.stat.coordinates.intersection(df_det_simple, df_anno_simple))
    assert ans[0][0] == 0
    assert ans[1][1] == 0


def test_iou(df_anno_simple, df_det_simple):
    ans = np.asarray(bb.stat.coordinates.iou(df_det_simple, df_anno_simple, bias=0))
    assert ans.shape == (3, 3)
    assert pytest.approx(list(ans[0]), 0.001) == [1, 0.3346, 0.16034]
    assert ans[1][1] == 1
    assert ans[2][2] == 1

    df_det_simple.at[0, 'x_top_left'] = 15
    df_det_simple.at[1, 'y_top_left'] = 60
    ans = np.asarray(bb.stat.coordinates.iou(df_det_simple, df_anno_simple, bias=0))
    assert ans[0][0] == 0
    assert ans[1][1] == 0


def test_ioa(df_anno_simple, df_det_simple):
    ans = np.asarray(bb.stat.coordinates.ioa(df_det_simple, df_anno_simple, bias=0))
    assert ans.shape == (3, 3)
    assert pytest.approx(list(ans[0]), 0.001) == [1, 0.8775, 0.76000]
    assert ans[1][1] == 1
    assert ans[2][2] == 1

    df_anno_simple.at[0, 'width'] = 11
    ans = np.asarray(bb.stat.coordinates.ioa(df_det_simple, df_anno_simple, 'b', bias=0))
    assert pytest.approx(ans[0][0], 0.001) == 0.90909

    df_det_simple.at[0, 'x_top_left'] = 15
    df_det_simple.at[1, 'y_top_left'] = 60
    ans = np.asarray(bb.stat.coordinates.ioa(df_det_simple, df_anno_simple, bias=0))
    assert ans[0][0] == 0
    assert ans[1][1] == 0


def test_pdollar(df_anno_simple, df_det_simple):
    df_anno_simple.at[1, 'ignore'] = True
    ans = np.asarray(bb.stat.coordinates.pdollar(df_det_simple, df_anno_simple, bias=0))
    assert ans.shape == (3, 3)
    assert pytest.approx(list(ans[0]), 0.001) == [1, 0.8775, 0.16034]
    assert ans[1][1] == 1
    assert ans[2][2] == 1

    df_det_simple.at[0, 'x_top_left'] = 15
    df_det_simple.at[1, 'y_top_left'] = 60
    ans = np.asarray(bb.stat.coordinates.pdollar(df_det_simple, df_anno_simple, bias=0))
    assert ans[0][0] == 0
    assert ans[1][1] == 0


def test_pdollar_ignoremethod(df_anno_simple, df_det_simple):
    df_anno_simple.at[1, 'ignore'] = True
    df_anno_simple.at[0, 'ignore'] = True
    df_anno_simple['ignore_method'] = 2
    df_anno_simple.at[0, 'ignore_method'] = 1

    ans = np.asarray(bb.stat.coordinates.pdollar(df_det_simple, df_anno_simple, bias=0))
    assert ans.shape == (3, 3)
    assert pytest.approx(list(ans[:, 0]), 0.001) == [1, 0.3346, 0.16034]
    assert ans[1][1] == 1
    assert ans[2][2] == 1

    ans = np.asarray(bb.stat.coordinates.pdollar(df_det_simple, df_anno_simple, bias=0, use_ignore_method=False))
    assert pytest.approx(list(ans[:, 0]), 0.001) == [1, 0.3510, 0.16889]

    df_det_simple.at[0, 'x_top_left'] = 15
    df_det_simple.at[1, 'y_top_left'] = 60
    ans = np.asarray(bb.stat.coordinates.pdollar(df_det_simple, df_anno_simple, bias=0))
    assert ans[0][0] == 0
    assert ans[1][1] == 0


@pytest.mark.parametrize(
    'uut',
    [
        (bb.stat.coordinates.intersection, {}),
        (bb.stat.coordinates.iou, {}),
        (bb.stat.coordinates.iou, {'bias': 0}),
        (bb.stat.coordinates.ioa, {}),
        (bb.stat.coordinates.ioa, {'bias': 0}),
        (bb.stat.coordinates.ioa, {'denominator': 'b', 'bias': 0}),
        (bb.stat.coordinates.pdollar, {}),
        (bb.stat.coordinates.pdollar, {'bias': 0}),
        (bb.stat.coordinates.pdollar, {'use_ignore_method': False, 'bias': 0}),
    ],
)
def test_segmentation(df_anno_simple, df_det_simple, uut):
    func, kwargs = uut

    # Prep data
    df_anno_simple.at[1, 'ignore'] = True
    df_anno_simple.at[0, 'ignore'] = True
    df_anno_simple['ignore_method'] = 2
    df_anno_simple.at[0, 'ignore_method'] = 1

    # Compute HBB polygons
    dfa_seg = bb.util.BoundingBoxTransformer(df_anno_simple).get_hbb_poly()
    dfd_seg = bb.util.BoundingBoxTransformer(df_det_simple).get_hbb_poly()

    hbb_result = np.asarray(func(df_det_simple, df_anno_simple, **kwargs))
    seg_res = np.asarray(func(dfd_seg, dfa_seg, segment=True, **kwargs))

    assert (hbb_result == seg_res).all()


@pytest.mark.long
def test_chunked_segmentation(df_anno_simple, df_det_simple):
    # Prep data (Targetting a MAX_POLY of 250_000)
    dfd = bb.util.concat([df_det_simple] * math.ceil(505 / len(df_det_simple)))
    dfa = bb.util.concat([df_anno_simple] * math.ceil(500 / len(df_anno_simple)))
    assert len(dfd) * len(dfa) > 250_000

    # Compute HBB polygons
    dfd_seg = bb.util.BoundingBoxTransformer(dfd).get_hbb_poly()
    dfa_seg = bb.util.BoundingBoxTransformer(dfa).get_hbb_poly()

    hbb_result = np.asarray(bb.stat.coordinates.pdollar(dfd, dfa))
    seg_res = np.asarray(bb.stat.coordinates.pdollar(dfd_seg, dfa_seg, segment=True))

    assert (hbb_result == seg_res).all()
