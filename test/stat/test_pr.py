#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test pr function
#
import pandas as pd

import brambox as bb


def test_basic(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    pr = bb.stat.pr(df_det_simple, df_anno_simple, 0.5)
    assert list(pr.precision) == [1, 1, 1]
    assert list(pr.recall) == [1 / 3, 2 / 3, 1]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]


def test_no_ignore(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 2
    df_det_simple.at[0, 'width'] = 9
    df_anno_simple.at[0, 'ignore'] = True

    pr = bb.stat.pr(df_det_simple, df_anno_simple, 0.95, ignore=False)
    assert list(pr.precision) == [1, 1 / 2, 2 / 3]
    assert list(pr.recall) == [1 / 3, 1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]

    # Default should be True because we have ignored annos
    pr = bb.stat.pr(df_det_simple, df_anno_simple, 0.95)
    assert list(pr.precision) == [1, 1]
    assert list(pr.recall) == [1 / 2, 1]
    assert list(pr.confidence) == [1.0, 0.0]


def test_seperate_match(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 2
    df_det_simple.at[0, 'width'] = 9
    df_det_matched = bb.stat.match_det(df_det_simple, df_anno_simple, 0.95, criteria=bb.stat.coordinates.iou)

    assert not df_det_matched.loc[0].tp
    assert df_det_matched.loc[0].fp

    pr = bb.stat.pr(df_det_matched, df_anno_simple)
    assert list(pr.precision) == [1, 1 / 2, 2 / 3]
    assert list(pr.recall) == [1 / 3, 1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]


def test_pr_unsorted(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    pr = bb.stat.pr(df_det_simple, df_anno_simple)

    assert list(pr.precision) == [1, 1 / 2, 2 / 3]
    assert list(pr.recall) == [1 / 3, 1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]


def test_pr_same_conf(df_anno_simple, df_det_simple):
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    pr = bb.stat.pr(df_det_simple, df_anno_simple)

    assert list(pr.precision) == [1, 2 / 3]
    assert list(pr.recall) == [1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.0]


def test_no_boxes(df_anno_simple, df_det_simple):
    df_no_det = df_det_simple.iloc[0:0]
    pr = bb.stat.pr(df_no_det, df_anno_simple, 0.5)
    assert list(pr.precision) == [0.0]
    assert list(pr.recall) == [0.0]
    assert list(pr.confidence) == [0.0]

    df_no_anno = df_anno_simple.iloc[0:0]
    pr = bb.stat.pr(df_det_simple, df_no_anno, 0.5)
    assert list(pr.precision) == [0.0, 0.0]
    assert list(pr.recall) == [0.0, 0.0]
    assert list(pr.confidence) == [1.0, 0.0]

    pr = bb.stat.pr(df_no_det, df_no_anno, 0.5)
    assert list(pr.precision) == []
    assert list(pr.recall) == []
    assert list(pr.confidence) == []


def test_smooth(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 10

    # No Smoothing
    pr = bb.stat.pr(df_det_simple, df_anno_simple, 0.8)
    assert list(pr.precision) == [1, 0.5, 2 / 3]
    assert list(pr.recall) == [1 / 3, 1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]

    # Smoothing
    pr = bb.stat.pr(df_det_simple, df_anno_simple, 0.8, smooth=True)
    assert list(pr.precision) == [1, 2 / 3, 2 / 3]
    assert list(pr.recall) == [1 / 3, 1 / 3, 2 / 3]
    assert list(pr.confidence) == [1.0, 0.5, 0.0]
