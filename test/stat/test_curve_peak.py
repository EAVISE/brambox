#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test peak function
#
import pandas as pd
import pytest

import brambox as bb


def test_max():
    curve = pd.DataFrame({'y': [0.7, 1, 0.5], 'x': [0.2, 0.6, 0.9], 'confidence': [0.7, 1.0, 0.3]})
    pt = bb.stat.peak(curve)

    assert pt['confidence'] == 1.0
    assert pt['x'] == 0.6
    assert pt['y'] == 1


def test_min():
    curve = pd.DataFrame({'y': [0.7, 1, 0.5], 'x': [0.2, 0.6, 0.9], 'confidence': [0.7, 1.0, 0.3]})
    pt = bb.stat.peak(curve, maximum=False)

    assert pt['confidence'] == 0.3
    assert pt['x'] == 0.9
    assert pt['y'] == 0.5


def test_custom_axis():
    curve = pd.DataFrame({'y': [0.7, 1, 0.5], 'x': [0.9, 0.2, 0.6], 'confidence': [0.7, 1.0, 0.3]})
    pt = bb.stat.peak(curve, y='x')

    assert pt['confidence'] == 0.7
    assert pt['x'] == 0.9
    assert pt['y'] == 0.7
