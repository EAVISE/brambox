#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test lamr function
#
import numpy as np
import pandas as pd

import brambox as bb


def test_basic():
    df = pd.DataFrame({'miss_rate': [1, 1 / 2, 2 / 3], 'false_positives_per_image': [0.01, 0.1, 1]})
    lamr = bb.stat.lamr(df, 3)
    assert lamr == np.exp(np.log(np.array([1, 1 / 2, 2 / 3])).sum() / 3)


def test_no_data():
    lamr = bb.stat.lamr(pd.DataFrame({'miss_rate': [], 'false_positives_per_image': []}))
    assert np.isnan(lamr)
