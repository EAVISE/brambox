#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test match_anno, match_box function
#
import numpy as np
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture
def match_box_a():
    def _match_box_a(*args, **kwargs):
        d, a = bb.stat.match_box(*args, **kwargs)
        return a

    return _match_box_a


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_basic(df_anno_simple, df_det_simple, match):
    result = match(df_det_simple, df_anno_simple, 0.9)
    result = result.sort_index()

    assert len(result) == 3
    assert list(result.detection) == [0, 1, 2]
    assert list(result.criteria) == pytest.approx([1, 1, 1])


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_no_anno(df_anno_simple, df_det_simple, match):
    df_det = df_det_simple.copy()
    df_det = bb.util.concat([df_det, df_det.loc[0:0]], ignore_index=True, sort=False)
    df_det.at[3, 'image'] = 'img03'
    result = match(df_det, df_anno_simple, 0.9)
    assert len(result) == 3
    assert list(result.detection) == [0, 1, 2]
    assert list(result.criteria) == pytest.approx([1, 1, 1])

    df_anno_simple = df_anno_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    assert len(result) == 0

    df_det_simple = df_det_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    assert len(result) == 0


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_no_det(df_anno_simple, df_det_simple, match):
    df_anno = df_anno_simple.copy()
    df_anno = bb.util.concat([df_anno, df_anno.loc[0:0]], ignore_index=True, sort=False)
    df_anno.at[3, 'image'] = 'img03'
    result = match(df_det_simple, df_anno, 0.9)
    assert len(result) == 4
    assert np.isnan(result.detection.loc[3])
    assert result.criteria.loc[3] == 0

    df_det_simple = df_det_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    assert len(result) == 3
    assert all(np.isnan(val) for val in result.detection.values)
    assert list(result.criteria) == [0, 0, 0]


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_class(df_anno_simple, df_det_simple, match):
    df_det_simple.at[1, 'class_label'] = 'random'

    result = match(df_det_simple, df_anno_simple, 0.9)
    assert np.isnan(result.detection.loc[1])
    assert result.criteria.loc[1] == 0

    result = match(df_det_simple, df_anno_simple, 0.9, class_label=False)
    assert result.detection.loc[1] == 1
    assert result.criteria.loc[1] == pytest.approx(1)


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_ignore(df_anno_simple, df_det_simple, match):
    df_det_simple.at[0, 'confidence'] = 0.60
    df_anno_simple = bb.util.concat([df_anno_simple, df_anno_simple.loc[0:0]], ignore_index=True, sort=False)
    df_anno_simple.at[0, 'ignore'] = True

    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.NONE)
    assert result.detection.loc[0] == 0
    assert result.criteria.loc[0] == pytest.approx(1)

    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.SINGLE)
    assert np.isnan(result.detection.loc[0])
    assert result.criteria.loc[0] == 0

    df_det_simple = bb.util.concat([df_det_simple, df_det_simple.loc[0:0]], ignore_index=True, sort=False)
    df_det_simple.at[0, 'confidence'] = 0.4

    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.MULTIPLE)
    assert result.detection.loc[0] == 0
    assert result.criteria.loc[0] == pytest.approx(1)


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_custom_crit(df_anno_simple, df_det_simple, match):
    def crit(det, anno):
        xd = det.x_top_left.values
        xa = anno.x_top_left.values
        return 1 - np.abs(xd[None, :].T - xa)

    df_det_simple.at[0, 'x_top_left'] = 0.5
    df_det_simple.at[1, 'x_top_left'] = 2.2
    result = match(df_det_simple, df_anno_simple, 0.7, criteria=crit)
    result = result.sort_index()

    assert np.isnan(result.detection.loc[0])
    assert list(result.detection[1:]) == [1, 2]
    assert result.criteria.loc[0] == 0
    assert list(result.criteria[1:]) == pytest.approx([0.8, 1])


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_multi_iou(df_anno_simple, df_det_simple, match):
    df_anno_simple.at[0, 'x_top_left'] = 2
    df_det_simple['confidence'] = [1.0, 0.7, 0.4]
    result = match(df_det_simple, df_anno_simple, [0.6, 0.9])
    result = result.sort_index()

    assert len(result) == 3
    assert list(result['detection-0.6']) == [0, 1, 2]
    assert list(result['criteria-0.6']) == pytest.approx([9 / 11, 1, 1])
    assert list(result['detection-0.9']) == pytest.approx([np.nan, 1, 2], nan_ok=True)
    assert list(result['criteria-0.9']) == pytest.approx([0, 1, 1])


@pytest.mark.parametrize('match', [bb.stat.match_anno, pytest.lazy_fixture('match_box_a')])
def test_ignoremethods(df_anno_simple, df_det_simple, match):
    df_det_simple.at[0, 'confidence'] = 0.1
    df_anno_simple.at[1, 'width'] *= 10
    result = match(df_det_simple, df_anno_simple, 0.9)
    result = result.sort_index()
    assert len(result) == 3
    assert list(result.detection) == pytest.approx([0, np.nan, 2], nan_ok=True)
    assert list(result.criteria) == pytest.approx([1, 0, 1])

    # By setting ignore_method to 2,
    # we count on the fact that pdollar will use IoA to match and thus ignore the increased anno width
    df_anno_simple['ignore'] = True
    df_anno_simple['ignore_method'] = np.array([0, 2, 0], dtype=int)
    result = match(df_det_simple, df_anno_simple, 0.9, ignore=3)
    result = result.sort_index()
    assert len(result) == 3
    assert list(result.detection) == [0, 1, 2]
    assert list(result.criteria) == pytest.approx([1, 1, 1])
