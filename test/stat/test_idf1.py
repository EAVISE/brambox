#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test idf1 function
#
import pandas as pd

import brambox as bb


def test_basic(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_det_multi_img['id'] = pd.Series([0, 1, 2, 3, 2, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img) == 8 / 11

    df_det_multi_img.loc[8, 'x_top_left'] = 250
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img) == 7 / 11


def test_multivideo(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 0, 1, 1, 1, 1, 2, 2, 2, 2], dtype=pd.Int64Dtype())
    df_det_multi_img['id'] = pd.Series([0, 1, 0, 1, 1, 2, 2, 1, 1, 1, 1], dtype=pd.Int64Dtype())

    # Single Video
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img) == 8 / 11

    # Single Video Bis
    df_det_multi_img['video'] = list('aaaaaaaaaaa')
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img) == 8 / 11

    # Multi Video
    df_det_multi_img['video'] = list('aaaaaaabbbb')
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img) == 9 / 11


def test_class_label(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_anno_multi_img.loc[9, 'class_label'] = 'dog'
    df_det_multi_img['id'] = pd.Series([0, 1, 2, 3, 2, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())

    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img, class_label=False) == 8 / 11
    assert bb.stat.idf1(df_det_multi_img, df_anno_multi_img, class_label=True) == 7 / 11
