#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test auc function
#
import numpy as np
import pandas as pd
import pytest

import brambox as bb


def test_basic():
    df = pd.DataFrame({'precision': [1, 1 / 2, 2 / 3, 2 / 4, 3 / 5, 3 / 6], 'recall': [1 / 3, 1 / 3, 2 / 3, 2 / 3, 3 / 3, 3 / 3]})
    auc = bb.stat.auc(df)
    assert auc == pytest.approx(32 / 45)


def test_unsorted():
    df = pd.DataFrame({'precision': [2 / 3, 1, 1 / 2, 2 / 4, 3 / 5, 3 / 6], 'recall': [2 / 3, 1 / 3, 1 / 3, 2 / 3, 3 / 3, 3 / 3]})
    auc = bb.stat.auc(df)
    assert auc == pytest.approx(32 / 45)


def test_no_data():
    auc = bb.stat.auc(pd.DataFrame({'precision': [], 'recall': []}))
    assert np.isnan(auc)


def test_one_point():
    auc = bb.stat.auc(pd.DataFrame({'precision': [0.7], 'recall': [0.5]}))
    assert auc == 0.35


def test_custom_axis():
    df = pd.DataFrame({'precision': [1 / 3, 1 / 3, 2 / 3, 2 / 3, 3 / 3, 3 / 3], 'recall': [1, 1 / 2, 2 / 3, 2 / 4, 3 / 5, 3 / 6]})
    auc = bb.stat.auc(df, x='precision', y='recall')
    assert auc == pytest.approx(32 / 45)
