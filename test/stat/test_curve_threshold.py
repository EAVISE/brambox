#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test threshold function
#
import logging

import pandas as pd
import pytest

import brambox as bb


def test_basic():
    curve = pd.DataFrame({'y': [1, 0.7, 0.5], 'x': [0.2, 0.6, 0.9], 'confidence': [1.0, 0.7, 0.3]})

    pt = bb.stat.threshold(curve, 'x', 0.5)
    assert pt['confidence'] == 0.7
    assert pt['x'] == 0.6
    assert pt['y'] == 0.7

    pt = bb.stat.threshold(curve, 'y', 0.5, first=False)
    assert pt['confidence'] == 0.3
    assert pt['x'] == 0.9
    assert pt['y'] == 0.5


def test_same_values():
    curve = pd.DataFrame({'y': [0.3, 0.5, 0.5], 'x': [0.2, 0.6, 0.9], 'confidence': [1.0, 0.7, 0.3]})

    pt = bb.stat.threshold(curve, 'y', 0.5)
    assert pt['confidence'] == 0.7
    assert pt['x'] == 0.6
    assert pt['y'] == 0.5

    pt = bb.stat.threshold(curve, 'y', 0.5, first=False)
    assert pt['confidence'] == 0.3
    assert pt['x'] == 0.9
    assert pt['y'] == 0.5


def test_nomatch():
    curve = pd.DataFrame({'y': [1, 0.7, 0.5], 'x': [0.9, 0.2, 0.6], 'confidence': [1.0, 0.7, 0.3]})
    pt = bb.stat.threshold(curve, 'x', 0.95)
    assert pt is None


def test_x_unsorted(df_det_simple):
    curve = pd.DataFrame({'y': [1, 0.7, 0.5], 'x': [0.6, 0.9, 0.2], 'confidence': [0.7, 0.3, 1.0]})

    pt = bb.stat.threshold(curve, 'x', 0.5)
    assert pt['confidence'] == 0.7
    assert pt['x'] == 0.6
    assert pt['y'] == 1.0


def test_x_conf_not_matched(caplog):
    caplog.set_level(logging.DEBUG)
    curve = pd.DataFrame({'y': [1, 0.7, 0.5], 'x': [0.6, 0.9, 0.2], 'confidence': [1.0, 0.7, 0.3]})

    pt = bb.stat.threshold(curve, 'x', 0.5)
    assert pt['confidence'] == 1.0
    assert pt['x'] == 0.6
    assert pt['y'] == 1.0

    assert len(caplog.records) == 1
    assert caplog.records[0].levelname == 'ERROR'
    assert 'not sorted in increasing order' in caplog.text
