#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test ap function
#
import numpy as np
import pandas as pd

import brambox as bb


def test_basic():
    df = pd.DataFrame({'precision': [1, 1 / 2, 2 / 3, 2 / 4, 3 / 5, 3 / 6], 'recall': [1 / 3, 1 / 3, 2 / 3, 2 / 3, 3 / 3, 3 / 3]})
    ap = bb.stat.ap(df)
    assert ap == 1 * 1 / 3 + 1 / 2 * 0 + 2 / 3 * 1 / 3 + 2 / 4 * 0 + 3 / 5 * 1 / 3 + 3 / 6 * 0


def test_no_data():
    ap = bb.stat.ap(pd.DataFrame({'precision': [], 'recall': []}))
    assert np.isnan(ap)


def test_one_point():
    ap = bb.stat.ap(pd.DataFrame({'precision': [0.7], 'recall': [0.5]}))
    assert ap == 0.35
