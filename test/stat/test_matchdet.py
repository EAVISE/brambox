#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test match_det function
#
import numpy as np
import pytest

import brambox as bb


@pytest.fixture
def match_box_d():
    def _match_box_d(*args, **kwargs):
        d, a = bb.stat.match_box(*args, **kwargs)
        return d

    return _match_box_d


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_basic(df_anno_simple, df_det_simple, match):
    result = match(df_det_simple, df_anno_simple, 0.9)
    result = result.sort_index()

    assert len(result) == 3
    assert list(result.tp) == [1, 1, 1]
    assert list(result.fp) == [0, 0, 0]
    assert list(result.annotation) == [0, 1, 2]


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_no_anno(df_anno_simple, df_det_simple, match):
    df_det = df_det_simple.copy()
    df_det = bb.util.concat([df_det, df_det.loc[0:0]], ignore_index=True, sort=False)
    df_det.at[3, 'image'] = 'img03'
    result = match(df_det, df_anno_simple, 0.9)
    result = result.sort_index()
    assert not result.tp.loc[3]
    assert result.fp.loc[3]

    df_anno_simple = df_anno_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    result = result.sort_index()

    assert list(result.tp) == [False] * len(df_det_simple)
    assert list(result.fp) == [True] * len(df_det_simple)
    np.testing.assert_equal(result.annotation.values, [np.nan] * len(df_det_simple))


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_no_det(df_anno_simple, df_det_simple, match):
    df_anno = df_anno_simple.copy()
    df_anno = bb.util.concat([df_anno, df_anno.loc[0:0]], ignore_index=True, sort=False)
    df_anno.at[3, 'image'] = 'img03'
    result = match(df_det_simple, df_anno, 0.9)
    result = result.sort_index()
    assert len(result) == 3
    assert list(result.tp) == [True, True, True]
    assert list(result.fp) == [False, False, False]
    assert list(result.annotation) == [0, 1, 2]

    df_det_simple = df_det_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    assert len(result) == 0

    df_anno_simple = df_anno_simple.iloc[0:0]
    result = match(df_det_simple, df_anno_simple, 0.9)
    assert len(result) == 0


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_class(df_anno_simple, df_det_simple, match):
    df_det_simple.at[0, 'class_label'] = 'random'
    result = match(df_det_simple, df_anno_simple, 0.9)
    result = result.sort_index()
    assert not result.tp.loc[0]
    assert result.fp.loc[0]
    assert np.isnan(result.annotation.loc[0])

    result = match(df_det_simple, df_anno_simple, 0.9, class_label=False)
    result = result.sort_index()
    assert result.tp.loc[0]
    assert not result.fp.loc[0]
    assert result.annotation.loc[0] == 0


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_ignore(df_anno_simple, df_det_simple, match):
    df_anno_simple.at[0, 'ignore'] = True
    df_det_simple = bb.util.concat([df_det_simple, df_det_simple.loc[0:0]], ignore_index=True, sort=False)
    df_det_simple.loc[3, 'confidence'] = 0.60
    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.NONE)
    result = result.sort_index()
    assert result.loc[3, 'tp']
    assert not result.loc[3, 'fp']
    assert result.loc[3, 'annotation'] == 0
    assert not result.loc[0, 'tp']
    assert result.loc[0, 'fp']
    assert np.isnan(result.loc[0, 'annotation'])

    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.SINGLE)
    result = result.sort_index()
    assert not result.loc[3, 'tp']
    assert not result.loc[3, 'fp']
    assert result.loc[3, 'annotation'] == 0
    assert not result.loc[0, 'tp']
    assert result.loc[0, 'fp']
    assert np.isnan(result.loc[0, 'annotation'])

    result = match(df_det_simple, df_anno_simple, 0.9, ignore=bb.stat.IgnoreMethod.MULTIPLE)
    result = result.sort_index()
    assert not result.loc[3, 'tp']
    assert not result.loc[3, 'fp']
    assert result.loc[3, 'annotation'] == 0
    assert not result.loc[0, 'tp']
    assert not result.loc[0, 'fp']
    assert result.loc[0, 'annotation'] == 0


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_custom_crit(df_anno_simple, df_det_simple, match):
    def crit(det, anno):
        xd = det.x_top_left.values
        xa = anno.x_top_left.values
        return (xa == xd[None, :].T).astype(float)

    df_det_simple.at[0, 'x_top_left'] = 0
    result = match(df_det_simple, df_anno_simple, 1, criteria=crit)
    result = result.sort_index()
    assert list(result.tp) == [False, True, True]
    assert list(result.fp) == [True, False, False]
    np.testing.assert_equal(result.annotation.values, [np.nan, 1, 2])


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_multi_iou(df_anno_simple, df_det_simple, match):
    df_anno_simple.at[0, 'x_top_left'] = 3
    df_det_simple.at[0, 'confidence'] = 0.1
    result = match(df_det_simple, df_anno_simple, [0.1, 0.9])
    result = result.sort_index()
    assert len(result) == 3
    assert list(result['tp-0.1']) == [True, True, True]
    assert list(result['fp-0.1']) == [False, False, False]
    assert list(result['annotation-0.1']) == [0, 1, 2]
    assert list(result['tp-0.9']) == [False, True, True]
    assert list(result['fp-0.9']) == [True, False, False]
    np.testing.assert_equal(result['annotation-0.9'].values, [np.nan, 1, 2])


@pytest.mark.parametrize('match', [bb.stat.match_det, pytest.lazy_fixture('match_box_d')])
def test_ignoremethods(df_anno_simple, df_det_simple, match):
    df_anno_simple['ignore'] = True
    df_anno_simple['ignore_method'] = np.array([0, 1, 1], dtype=int)
    df_det_simple.at[0, 'confidence'] = 0.1  # conf : 0.1 1.0 0.0 0.0
    df_det_simple.loc[3] = df_det_simple.loc[2]
    result = match(df_det_simple, df_anno_simple, 0.9, ignore=3)
    result = result.sort_index()
    assert len(result) == 4
    assert list(result.tp) == [True, False, False, False]
    assert list(result.fp) == [False, False, False, True]
    np.testing.assert_equal(result.annotation.values, [0, 1, 2, np.nan])

    df_anno_simple['ignore_method'] = np.array([0, 1, 2], dtype=int)
    result = match(df_det_simple, df_anno_simple, 0.9, ignore=3)
    result = result.sort_index()
    assert len(result) == 4
    assert list(result.tp) == [True, False, False, False]
    assert list(result.fp) == [False, False, False, False]
    np.testing.assert_equal(result.annotation.values, [0, 1, 2, 2])
