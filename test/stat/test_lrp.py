#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test lrp function
#
import logging

import pandas as pd
import pytest

import brambox as bb


def test_basic(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    lrp = bb.stat.lrp(df_det_simple, df_anno_simple, 0.5)

    assert list(lrp.iou) == pytest.approx([0, 0, 0])
    assert list(lrp.fp) == pytest.approx([0, 0, 0])
    assert list(lrp.fn) == pytest.approx([2 / 3, 1 / 3, 0])
    assert list(lrp.lrp) == pytest.approx([2 / 3, 1 / 3, 0])
    assert list(lrp.confidence) == pytest.approx([1.0, 0.5, 0.0])


def test_iou(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'width'] = 9.0
    lrp = bb.stat.lrp(df_det_simple, df_anno_simple, 0.5)

    assert list(lrp.iou) == pytest.approx([0, 1 / 20, 1 / 30])
    assert list(lrp.fp) == pytest.approx([0, 0, 0])
    assert list(lrp.fn) == pytest.approx([2 / 3, 1 / 3, 0])
    assert list(lrp.lrp) == pytest.approx([2 / 3, 2 / 5, 1 / 15])
    assert list(lrp.confidence) == pytest.approx([1.0, 0.5, 0.0])


def test_no_ignore(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 2
    df_det_simple.at[0, 'width'] = 9
    df_anno_simple.at[0, 'ignore'] = True

    lrp = bb.stat.lrp(df_det_simple, df_anno_simple, 0.95, ignore=False)
    assert list(lrp.iou) == pytest.approx([0, 0, 0])
    assert list(lrp.fp) == pytest.approx([0, 1 / 2, 1 / 3])
    assert list(lrp.fn) == pytest.approx([2 / 3, 2 / 3, 1 / 3])
    assert list(lrp.lrp) == pytest.approx([2 / 3, 3 / 4, 1 / 2])
    assert list(lrp.confidence) == pytest.approx([1.0, 0.5, 0.0])

    # Default should be True because we have ignored annos
    lrp = bb.stat.lrp(df_det_simple, df_anno_simple, 0.95)
    assert list(lrp.iou) == pytest.approx([0, 0])
    assert list(lrp.fp) == pytest.approx([0, 0])
    assert list(lrp.fn) == pytest.approx([1 / 2, 0])
    assert list(lrp.lrp) == pytest.approx([1 / 2, 0], abs=1e-11)
    assert list(lrp.confidence) == pytest.approx([1.0, 0.0])


def test_lrp_already_matched(df_anno_simple, df_det_simple, caplog):
    caplog.set_level(logging.DEBUG)

    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    df_anno_simple['detection'] = [0, 1, 2]
    df_anno_simple['criteria'] = [1, 1, 1]
    lrp = bb.stat.lrp(df_det_simple, df_anno_simple)

    assert list(lrp.iou) == pytest.approx([0, 0, 0])
    assert list(lrp.fp) == pytest.approx([0, 1 / 2, 1 / 3])
    assert list(lrp.fn) == pytest.approx([2 / 3, 2 / 3, 1 / 3])
    assert list(lrp.lrp) == pytest.approx([2 / 3, 3 / 4, 1 / 2])
    assert list(lrp.confidence) == pytest.approx([1.0, 0.5, 0.0])

    assert len(caplog.records) == 1
    assert caplog.records[0].levelname == 'INFO'
    assert 'Custom matching' in caplog.text


def test_lrp_same_conf(df_anno_simple, df_det_simple):
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    df_anno_simple['detection'] = [0, 1, 2]
    df_anno_simple['criteria'] = [1, 1, 1]
    lrp = bb.stat.lrp(df_det_simple, df_anno_simple)

    assert list(lrp.iou) == pytest.approx([0, 0])
    assert list(lrp.fp) == pytest.approx([0, 1 / 3])
    assert list(lrp.fn) == pytest.approx([2 / 3, 1 / 3])
    assert list(lrp.lrp) == pytest.approx([2 / 3, 1 / 2])
    assert list(lrp.confidence) == pytest.approx([1.0, 0.0])
