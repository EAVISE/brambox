#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test fscore function
#
import numpy as np
import pandas as pd
import pytest

import brambox as bb


def test_basic():
    df = pd.DataFrame(
        {
            'precision': [1, 1 / 2, 2 / 3, 2 / 4, 3 / 5, 3 / 6],
            'recall': [1 / 3, 1 / 3, 2 / 3, 2 / 3, 3 / 3, 3 / 3],
            'confidence': [1.0, 0.9, 0.8, 0.7, 0.6, 0.5],
        }
    )
    f = bb.stat.fscore(df)
    assert list(f.f1) == pytest.approx([0.5, 0.4, 0.6666, 0.5714, 0.75, 0.6666], abs=1e-4)


def test_beta():
    df = pd.DataFrame(
        {
            'precision': [1, 1 / 2, 2 / 3, 2 / 4, 3 / 5, 3 / 6],
            'recall': [1 / 3, 1 / 3, 2 / 3, 2 / 3, 3 / 3, 3 / 3],
            'confidence': [1.0, 0.9, 0.8, 0.7, 0.6, 0.5],
        }
    )

    f = bb.stat.fscore(df, 2)
    assert list(f.f2.round(4)) == [0.3846, 0.3571, 0.6667, 0.625, 0.8824, 0.8333]

    f = bb.stat.fscore(df, 0.5)
    assert list(f.f0_5.round(4)) == [0.7143, 0.4545, 0.6667, 0.5263, 0.6522, 0.5556]


def test_no_data():
    f = bb.stat.fscore(pd.DataFrame({'precision': [], 'recall': [], 'confidence': []}))
    assert f.equals(pd.DataFrame({'f1': [], 'recall': [], 'confidence': []}))


def test_zeros():
    f = bb.stat.fscore(pd.DataFrame({'precision': [0.0, 1.0, 0.0], 'recall': [0.0, 0.0, 1.0], 'confidence': [0.9, 0.8, 0.7]}))
    assert f.equals(pd.DataFrame({'f1': [0, 0, 0], 'recall': [0, 0, 1], 'confidence': [0.9, 0.8, 0.7]}).astype(float))
