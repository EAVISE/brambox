#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test mota function
#
import pandas as pd

import brambox as bb


def test_basic(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_anno_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_det_multi_img['id'] = pd.Series([0, 1, 2, 3, 2, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_det_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_anno_multi_img.loc[0, 'x_top_left'] = 155
    df_det_multi_img.loc[0, 'x_top_left'] = 155
    assert bb.stat.mota(df_det_multi_img, df_anno_multi_img) == 1 - ((0 + 0 + 4) / 11)

    df_det_multi_img.loc[8, 'x_top_left'] = 250
    assert bb.stat.mota(df_det_multi_img, df_anno_multi_img) == 1 - ((1 + 1 + 4) / 11)


def test_multiclass(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img.loc[[0, 1], 'class_label'] = ['A', 'B']
    df_anno_multi_img['id'] = pd.Series([0, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2], dtype=pd.Int64Dtype())
    df_anno_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_det_multi_img.loc[[0, 1], 'class_label'] = ['B', 'B']
    df_det_multi_img['id'] = pd.Series([0, 1, 2, 3, 4, 3, 2, 2, 2, 2, 2], dtype=pd.Int64Dtype())
    df_det_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_anno_multi_img.loc[0, 'x_top_left'] = 155
    df_det_multi_img.loc[0, 'x_top_left'] = 155
    assert bb.stat.mota(df_det_multi_img, df_anno_multi_img) == 1 - ((1 + 1 + 4) / 11)


def test_singlevideo(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_anno_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_anno_multi_img['video'] = list('aaaaaaaaaaa')
    df_det_multi_img['id'] = pd.Series([0, 1, 2, 3, 2, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_det_multi_img['frame'] = [0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    df_det_multi_img['video'] = list('aaaaaaaaaaa')
    assert bb.stat.mota(df_det_multi_img, df_anno_multi_img) == 1 - ((0 + 0 + 4) / 11)


def test_multivideo(df_anno_multi_img, df_det_multi_img):
    df_anno_multi_img['id'] = pd.Series([0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], dtype=pd.Int64Dtype())
    df_anno_multi_img['frame'] = [0, 1, 2, 3, 4, 5, 6, 0, 1, 2, 3]
    df_anno_multi_img['video'] = list('aaaaaaabbbb')
    df_det_multi_img['id'] = pd.Series([0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3], dtype=pd.Int64Dtype())
    df_det_multi_img['frame'] = [0, 1, 2, 3, 4, 5, 6, 0, 1, 2, 3]
    df_det_multi_img['video'] = list('aaaaaaabbbb')
    assert bb.stat.mota(df_det_multi_img, df_anno_multi_img) == 1 - ((0 + 0 + 1) / 11)
