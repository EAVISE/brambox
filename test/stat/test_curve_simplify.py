#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test simplify function
#
import numpy as np
import pandas as pd
import pytest

import brambox as bb


def test_basic():
    df = pd.DataFrame({'y': [0, 1, 1, 1, 2, 3, 4, 5, 6, 7, 8], 'x': [0, 1, 2, 3, 4, 4, 4, 4, 5, 6, 7]})
    newdf = bb.stat.simplify(df)
    assert list(newdf['x']) == pytest.approx([0, 1, 3, 4, 4, 5, 6, 7])
    assert list(newdf['y']) == pytest.approx([0, 1, 1, 2, 5, 6, 7, 8])


def test_duplicate_pairs():
    df = pd.DataFrame({'y': [0, 2, 4, 4, 6], 'x': [0, 1, 2, 2, 3], 'confidence': [0.9, 0.8, 0.7, 0.6, 0.5]})
    newdf = bb.stat.simplify(df)
    assert list(newdf['x']) == pytest.approx([0, 1, 2, 3])
    assert list(newdf['y']) == pytest.approx([0, 2, 4, 6])
    assert list(newdf['confidence']) == pytest.approx([0.9, 0.8, 0.6, 0.5])


def test_unsorted_conf():
    df = pd.DataFrame(
        {
            'y': [8, 7, 6, 5, 4, 3, 2, 0, 1, 1, 1],
            'x': [7, 6, 5, 4, 4, 4, 4, 0, 1, 2, 3],
            'confidence': [0, 1, 2, 3, 4, 5, 6, 10, 9, 8, 7],
        }
    )
    newdf = bb.stat.simplify(df)
    assert list(newdf['x']) == pytest.approx([0, 1, 3, 4, 4, 5, 6, 7])
    assert list(newdf['y']) == pytest.approx([0, 1, 1, 2, 5, 6, 7, 8])


def test_no_data():
    newdf = bb.stat.simplify(pd.DataFrame({'y': [], 'x': []}))
    assert len(newdf) == 0
