#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test auc function
#
import numpy as np
import pandas as pd
import pytest

import brambox as bb


def test_right():
    df = pd.DataFrame(
        {
            'precision': [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3],
            'recall': [0.001, 0.09, 0.11, 0.23, 0.51, 0.69, 0.81],
        }
    )

    # x_inter = 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
    # y_inter = 1, 0.8, 0.7, 0.6, 0.6, 0.6, 0.5, 0.3, 0.3, 0,   0
    auc = bb.stat.auc_interpolated(df, samples=11)
    assert auc == pytest.approx(0.49090909)  # avg(y_inter)


def test_left():
    df = pd.DataFrame(
        {
            'precision': [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3],
            'recall': [0.001, 0.09, 0.11, 0.23, 0.51, 0.69, 0.81],
        }
    )

    # x_inter = 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0
    # y_inter = 0, 0.9, 0.8, 0.7, 0.7, 0.7, 0.6, 0.5, 0.5, 0.3, 0.3
    auc = bb.stat.auc_interpolated(df, samples=11, side='left')
    assert auc == pytest.approx(0.54545454)  # avg(y_inter)


def test_unsorted():
    df = pd.DataFrame(
        {
            'precision': [1, 0.8, 0.9, 0.5, 0.7, 0.6, 0.3],
            'recall': [0.001, 0.11, 0.09, 0.69, 0.23, 0.51, 0.81],
        }
    )

    auc = bb.stat.auc_interpolated(df, samples=11)
    assert auc == pytest.approx(0.49090909)


def test_no_data():
    auc = bb.stat.auc_interpolated(pd.DataFrame({'precision': [], 'recall': []}))
    assert np.isnan(auc)


def test_one_point():
    auc = bb.stat.auc_interpolated(pd.DataFrame({'precision': [0.7], 'recall': [0.6]}))
    assert auc == pytest.approx(0.7 * 0.6, abs=0.005)

    auc = bb.stat.auc_interpolated(pd.DataFrame({'precision': [0.7], 'recall': [0.6]}), side='left')
    assert auc == pytest.approx(0.7 * 0.4, abs=0.005)


def test_custom_axis():
    df = pd.DataFrame(
        {
            'recall': [0.001, 0.09, 0.11, 0.23, 0.51, 0.69, 0.81],
            'precision': [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3],
        }
    )

    auc = bb.stat.auc_interpolated(df, samples=11, x='recall', y='precision')
    assert auc == pytest.approx(0.49090909)


def test_custom_range():
    df = pd.DataFrame(
        {
            'precision': [100, 90, 80, 70, 60, 50, 30],
            'recall': [0.1, 9, 11, 23, 51, 69, 81],
        }
    )

    # 100 times bigger than `test_right`
    auc = bb.stat.auc_interpolated(df, samples=11, range=(0, 100))
    assert auc == pytest.approx(49.090909)
