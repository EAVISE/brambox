#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test mrfppi function
#
import pandas as pd
import pytest

import brambox as bb


def test_basic(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    mrfppi = bb.stat.mr_fppi(df_det_simple, df_anno_simple, 0.5)

    print(mrfppi)

    assert list(mrfppi.miss_rate) == pytest.approx([2 / 3, 1 / 3, 0])
    assert list(mrfppi.false_positives_per_image) == [0, 0, 0]
    assert list(mrfppi.confidence) == [1.0, 0.5, 0.0]


def test_no_ignore(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 2
    df_det_simple.at[0, 'width'] = 9
    df_anno_simple.at[0, 'ignore'] = True

    mrfppi = bb.stat.mr_fppi(df_det_simple, df_anno_simple, 0.95, ignore=False)
    assert list(mrfppi.miss_rate) == pytest.approx([2 / 3, 2 / 3, 1 / 3])
    assert list(mrfppi.false_positives_per_image) == pytest.approx([0, 1 / 3, 1 / 3])
    assert list(mrfppi.confidence) == [1.0, 0.5, 0.0]

    # Default should be True because we have ignored annos
    mrfppi = bb.stat.mr_fppi(df_det_simple, df_anno_simple, 0.95)
    assert list(mrfppi.miss_rate) == [1 / 2, 0]
    assert list(mrfppi.false_positives_per_image) == [0, 0]
    assert list(mrfppi.confidence) == [1.0, 0.0]


def test_seperate_match(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple.at[0, 'x_top_left'] = 2
    df_det_simple.at[0, 'width'] = 9
    df_det_matched = bb.stat.match_det(df_det_simple, df_anno_simple, 0.95, criteria=bb.stat.coordinates.iou)

    assert not df_det_matched.loc[0].tp
    assert df_det_matched.loc[0].fp

    mrfppi = bb.stat.mr_fppi(df_det_matched, df_anno_simple)
    assert list(mrfppi.miss_rate) == pytest.approx([2 / 3, 2 / 3, 1 / 3])
    assert list(mrfppi.false_positives_per_image) == pytest.approx([0, 1 / 3, 1 / 3])
    assert list(mrfppi.confidence) == [1.0, 0.5, 0.0]


def test_pr_unsorted(df_anno_simple, df_det_simple):
    df_det_simple.at[0, 'confidence'] = 0.5
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    mrfppi = bb.stat.mr_fppi(df_det_simple, df_anno_simple)

    assert list(mrfppi.miss_rate) == pytest.approx([2 / 3, 2 / 3, 1 / 3])
    assert list(mrfppi.false_positives_per_image) == pytest.approx([0, 1 / 3, 1 / 3])
    assert list(mrfppi.confidence) == [1.0, 0.5, 0.0]


def test_pr_same_conf(df_anno_simple, df_det_simple):
    df_det_simple['tp'] = [False, True, True]
    df_det_simple['fp'] = [True, False, False]
    mrfppi = bb.stat.mr_fppi(df_det_simple, df_anno_simple)

    assert list(mrfppi.miss_rate) == pytest.approx([2 / 3, 1 / 3])
    assert list(mrfppi.false_positives_per_image) == pytest.approx([0, 1 / 3])
    assert list(mrfppi.confidence) == [1.0, 0.0]
