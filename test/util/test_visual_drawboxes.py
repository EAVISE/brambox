#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test draw_boxes function
#

import pandas as pd
import pytest

import brambox as bb

try:
    import cv2
    import numpy as np
except ModuleNotFoundError:
    cv2 = None

try:
    from PIL import Image, ImageDraw
except ModuleNotFoundError:
    Image = None


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawboxes_pil_basic(df_anno_simple):
    img = Image.new('RGB', (75, 75))
    res = bb.util.draw_boxes(img, df_anno_simple, color='red')
    assert list(img.getdata()) != list(res.getdata())

    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 4), (11, 4), (11, 44), (1, 44), (1, 4)], 'red', 3)
    imgd.line([(2, 5), (22, 5), (22, 55), (2, 55), (2, 5)], 'red', 3)
    imgd.line([(3, 6), (33, 6), (33, 66), (3, 66), (3, 6)], 'red', 3)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawboxes_pil_poly(df_anno_poly):
    img = Image.new('RGB', (75, 75))
    res = bb.util.draw_boxes(img, df_anno_poly, color='red')
    assert list(img.getdata()) != list(res.getdata())

    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 6), (6, 4), (11, 35), (3, 44), (1, 6)], 'red', 3)
    imgd.ellipse([(1.0, 4.0), (4.0, 7.0)], fill='red')
    imgd.line([(3, 6.5), (33, 6.5)], 'red', 3)
    imgd.line([(4, 7), (44, 77)], 'red', 3)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawboxes_pil_l(df_anno_simple):
    img = Image.new('L', (75, 75))
    res = bb.util.draw_boxes(img, df_anno_simple, color='red')
    assert list(img.getdata()) != list(res.getdata())

    img = img.convert('RGB')
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 4), (11, 4), (11, 44), (1, 44), (1, 4)], 'red', 3)
    imgd.line([(2, 5), (22, 5), (22, 55), (2, 55), (2, 5)], 'red', 3)
    imgd.line([(3, 6), (33, 6), (33, 66), (3, 66), (3, 6)], 'red', 3)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawboxes_pil_empty():
    img = Image.new('RGB', (75, 75))
    res = bb.util.draw_boxes(img, pd.DataFrame())
    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawboxes_cv_basic(df_anno_simple):
    img = np.zeros((75, 75, 3), np.uint8)
    res = bb.util.draw_boxes(img, df_anno_simple, color=(255, 0, 0))
    assert not np.array_equal(img, res)

    cv2.rectangle(img, (1, 4), (11, 44), (0, 0, 255), 3)
    cv2.rectangle(img, (2, 5), (22, 55), (0, 0, 255), 3)
    cv2.rectangle(img, (3, 6), (33, 66), (0, 0, 255), 3)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawboxes_cv_poly(df_anno_poly):
    img = np.zeros((75, 75, 3), np.uint8)
    res = bb.util.draw_boxes(img, df_anno_poly, color=(255, 0, 0))
    assert not np.array_equal(img, res)

    cv2.polylines(
        img,
        [np.array([(1, 6), (6, 4), (11, 35), (3, 44), (1, 6)], dtype=np.int32).reshape(-1, 2)],
        False,
        (0, 0, 255),
        3,
    )
    cv2.circle(
        img,
        (2, 5),
        3,
        (0, 0, 255),
        thickness=-1,
    )
    cv2.polylines(
        img,
        [np.array([(3, 6), (33, 6)], dtype=np.int32).reshape(-1, 2)],
        False,
        (0, 0, 255),
        3,
    )
    cv2.polylines(
        img,
        [np.array([(4, 7), (44, 77)], dtype=np.int32).reshape(-1, 2)],
        False,
        (0, 0, 255),
        3,
    )

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawboxes_cv_empty(df_anno_simple):
    img = np.zeros((75, 75, 3), np.uint8)
    res = bb.util.draw_boxes(img, pd.DataFrame())
    assert np.array_equal(img, res)


def test_drawboxes_invalid(df_anno_simple):
    with pytest.raises(TypeError) as errinfo:
        bb.util.draw_boxes({}, df_anno_simple)
    assert 'Unkown image type' in str(errinfo.value)
