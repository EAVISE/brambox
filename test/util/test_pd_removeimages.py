#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.remove_images
#

import pytest

import brambox as bb


def test_removeimages(df_anno_simple):
    df = bb.util.remove_images(df_anno_simple, ['img02'])

    assert len(df) == len(df_anno_simple[df_anno_simple.image != 'img02'])
    assert list(df.columns) == list(df_anno_simple.columns)
    assert sorted(list(df.image.cat.categories) + ['img02']) == list(df_anno_simple.image.cat.categories)


def test_removeimages_nodata(df_anno_simple):
    df = bb.util.remove_images(df_anno_simple, ['img03'])

    assert len(df) == len(df_anno_simple)
    assert list(df.columns) == list(df_anno_simple.columns)
    assert sorted(list(df.image.cat.categories) + ['img03']) == list(df_anno_simple.image.cat.categories)


def test_removeimages_multiple(df_anno_simple):
    df = bb.util.remove_images(df_anno_simple, ['img01', 'img03'])

    assert len(df) == len(df_anno_simple[df_anno_simple.image != 'img01'])
    assert list(df.columns) == list(df_anno_simple.columns)
    assert sorted(list(df.image.cat.categories) + ['img01', 'img03']) == list(df_anno_simple.image.cat.categories)
