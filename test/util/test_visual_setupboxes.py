#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test setup_boxes function
#

import numpy as np
import pandas as pd
import pytest
import shapely

from brambox.util._visual import setup_boxes


def test_setupboxes_basic(df_anno_simple):
    df = setup_boxes(df_anno_simple)

    assert list(df['color']) == [(255, 127, 14), (255, 127, 14), (31, 119, 180)]
    assert list(df['size']) == [3, 3, 3]
    assert list(df['label']) == [False, False, False]
    assert list(df['alpha']) == [1, 1, 1]


def test_setupboxes_colsexist(df_anno_simple):
    df_anno_simple['color'] = 'red'
    df_anno_simple['size'] = 666
    df_anno_simple['label'] = 'gotit'
    df_anno_simple['alpha'] = 0.666
    df = setup_boxes(df_anno_simple)

    assert list(df['color']) == ['red', 'red', 'red']
    assert list(df['size']) == [666, 666, 666]
    assert list(df['label']) == ['gotit', 'gotit', 'gotit']
    assert list(df['alpha']) == [0.666, 0.666, 0.666]


def test_setupboxes_color(df_anno_simple):
    df = setup_boxes(df_anno_simple, color=[(1, 1, 1), (1, 1, 1), (1, 1, 1)])
    assert list(df['color']) == [(1, 1, 1), (1, 1, 1), (1, 1, 1)]

    df = setup_boxes(df_anno_simple, color='red')
    assert list(df['color']) == ['red', 'red', 'red']

    df = setup_boxes(df_anno_simple, color=(255, 0, 0))
    assert list(df['color']) == [(255, 0, 0), (255, 0, 0), (255, 0, 0)]


def test_setupboxes_color_id(df_anno_simple):
    df_anno_simple['color'] = [0, 0, 1]
    df = setup_boxes(df_anno_simple)
    assert list(df['color']) == [(31, 119, 180), (31, 119, 180), (255, 127, 14)]


def test_setupboxes_size(df_anno_simple):
    df = setup_boxes(df_anno_simple, size=[1, 2, 3])
    assert list(df['size']) == [1, 2, 3]

    df = setup_boxes(df_anno_simple, size=5.0)
    assert list(df['size']) == [5, 5, 5]

    df = setup_boxes(df_anno_simple, size=-1)
    assert list(df['size']) == [-1, -1, -1]


def test_setupboxes_label(df_det_simple):
    df = setup_boxes(df_det_simple, label=['aaa', 'bbb', 'ccc'])
    assert list(df['label']) == ['aaa', 'bbb', 'ccc']

    df = setup_boxes(df_det_simple, label='test')
    assert list(df['label']) == ['test', 'test', 'test']

    df_det_simple.loc[2, 'confidence'] = 0.555533
    df = setup_boxes(df_det_simple, label=True)
    assert list(df['label']) == ['person [0.0%]', 'person [100.0%]', 'dog [55.55%]']


def test_setupboxes_oriented(df_anno_oriented):
    df = setup_boxes(df_anno_oriented)
    pts = df.loc[0, 'points']
    print(pts)
    assert pts[0] == (1.0, 4.0)
    assert pts[2] == (
        int(1 + 10 * np.cos(np.pi / 3) + 40 * np.sin(np.pi / 3)),
        int(4 - 10 * np.sin(np.pi / 3) + 40 * np.cos(np.pi / 3)),
    )


def test_setupboxes_poly(df_anno_poly):
    df = setup_boxes(df_anno_poly)
    poly = df.loc[0, 'segmentation']
    assert isinstance(poly, shapely.Polygon)


def test_setupboxes_coord_inf(df_anno_simple, caplog):
    df_anno_simple.loc[0, 'x_top_left'] = np.inf
    df_anno_simple.loc[1, 'height'] = -np.inf

    df = setup_boxes(df_anno_simple)
    assert 'Some bounding boxes contain Inf coordinates' in caplog.text
    assert df.shape[0] == df_anno_simple.shape[0] - 2


def test_setupboxes_coord_nan(df_anno_simple, caplog):
    df_anno_simple.loc[0, 'x_top_left'] = np.nan

    df = setup_boxes(df_anno_simple)
    assert 'Some bounding boxes contain NaN coordinates' in caplog.text
    assert df.shape[0] == df_anno_simple.shape[0] - 1
