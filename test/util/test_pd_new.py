#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.new
#
import pytest

import brambox as bb


def test_new_anno():
    df = bb.util.new('Annotation')
    assert list(df.columns) == [
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'ignore',
    ]
    assert df.dtypes.image == 'category'
    assert df.dtypes.class_label == 'object'
    assert df.dtypes.x_top_left == 'float'
    assert df.dtypes.y_top_left == 'float'
    assert df.dtypes.width == 'float'
    assert df.dtypes.height == 'float'
    assert df.dtypes.ignore == 'bool'

    df2 = bb.util.new('anno')
    assert df.equals(df2)


def test_new_det():
    df = bb.util.new('Detection')
    assert list(df.columns) == [
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'confidence',
    ]
    assert df.dtypes.image == 'category'
    assert df.dtypes.class_label == 'object'
    assert df.dtypes.x_top_left == 'float'
    assert df.dtypes.y_top_left == 'float'
    assert df.dtypes.width == 'float'
    assert df.dtypes.height == 'float'
    assert df.dtypes.confidence == 'float'

    df2 = bb.util.new('det')
    assert df.equals(df2)


def test_new_invalid():
    with pytest.raises(ValueError) as errinfo:
        bb.util.new('unkown')
    assert 'Unkown dataframe type' in str(errinfo.value)
