#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test drawing functions
#

import pytest
import shapely

from brambox.util._visual import draw_cv, draw_pil

try:
    import cv2
    import numpy as np
except ModuleNotFoundError:
    cv2 = None

try:
    from PIL import Image, ImageDraw, ImageFont

    try:
        font = ImageFont.truetype('DejaVuSansMono', 10)
    except IOError:
        font = ImageFont.load_default()
except ModuleNotFoundError:
    Image = None


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_basic(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, 1, (1, 1, 1))

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 1), (5, 1), (5, 5), (1, 5), (1, 1)], (1, 1, 1), 1)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_size(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(((1, 1), (10, 1), (10, 10), (1, 10), (1, 1)), False, 3, (1, 1, 1))

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 1), (10, 1), (10, 10), (1, 10), (1, 1)], (1, 1, 1), 3)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_poly(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(
        ((1, 1), (10, 1), (10, 10), (1, 10), (1, 1)),
        False,
        1,
        (1, 1, 1),
        segmentation=shapely.Polygon([(1, 1), (10, 3), (5, 5), (1, 5)]),
    )

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 1), (10, 3), (5, 5), (1, 5), (1, 1)], (1, 1, 1), 1)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_color(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, 1, 'red')

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 1), (5, 1), (5, 5), (1, 5), (1, 1)], (255, 0, 0), 1)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_label(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(((20, 20), (25, 20), (25, 25), (20, 25), (20, 20)), 'label', 3, (1, 1, 1))

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.line([(20, 20), (25, 20), (25, 25), (20, 25), (20, 20)], (1, 1, 1), 3)
    imgd.text((20, 5), 'label', (1, 1, 1), font)

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_fill(Box):
    img = Image.new('RGB', (25, 25))
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, -1, (1, 1, 1))

    res = img.copy()
    draw_pil(ImageDraw.Draw(res), box)
    imgd = ImageDraw.Draw(img)
    imgd.polygon([(1, 1), (5, 1), (5, 5), (1, 5), (1, 1)], (1, 1, 1))

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_drawpil_alpha(Box):
    # NOTE : We cannot test alpha on lines, because corners have weird overlapping behaviour
    img = Image.new('RGB', (25, 25))
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, -1, (255, 255, 255), alpha=0.5)

    res = img.copy()
    draw_pil(ImageDraw.Draw(res, 'RGBA'), box)
    imgd = ImageDraw.Draw(img, 'RGB')
    imgd.polygon([(1, 1), (5, 1), (5, 5), (1, 5), (1, 1)], (127, 127, 127))

    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_basic(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, 1, (1, 1, 1))

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (1, 1), (5, 5), (1, 1, 1), 1)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_size(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((1, 1), (10, 1), (10, 10), (1, 10), (1, 1)), False, 3, (1, 1, 1))

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (1, 1), (10, 10), (1, 1, 1), 3)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_poly(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(
        ((1, 1), (10, 1), (10, 10), (1, 10), (1, 1)),
        False,
        1,
        (1, 1, 1),
        segmentation=shapely.Polygon([(1, 1), (10, 3), (5, 5), (1, 5)]),
    )

    res = img.copy()
    draw_cv(res, box)

    cv2.polylines(
        img,
        [np.array([(1, 1), (10, 3), (5, 5), (1, 5), (1, 1)], dtype=np.int32).reshape(-1, 1, 2)],
        False,
        (1, 1, 1),
        1,
    )

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_color(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, 1, (255, 0, 0))

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (1, 1), (5, 5), (0, 0, 255), 1)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_label(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((20, 20), (25, 20), (25, 25), (20, 25), (20, 20)), 'label', 3, (1, 1, 1))

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (20, 20), (25, 25), (1, 1, 1), 3)
    cv2.putText(img, 'label', (20, 15), cv2.FONT_HERSHEY_PLAIN, 0.75, (1, 1, 1), 1, cv2.LINE_AA)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_fill(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, -1, (1, 1, 1))

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (1, 1), (5, 5), (1, 1, 1), -1)

    assert np.array_equal(img, res)


@pytest.mark.skipif(cv2 is None, reason='OpenCV not installed')
def test_drawcv_alpha(Box):
    img = np.zeros((25, 25, 3), np.uint8)
    box = Box(((1, 1), (5, 1), (5, 5), (1, 5), (1, 1)), False, 1, (255, 255, 255), alpha=0.5)

    res = img.copy()
    draw_cv(res, box)
    cv2.rectangle(img, (1, 1), (5, 5), (127, 127, 127), 1)

    assert np.array_equal(img, res)
