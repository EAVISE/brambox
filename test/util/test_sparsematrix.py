#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util.SparseFloatMatrix
#
import numpy as np

import brambox as bb


def test_basic():
    sfm = bb.util.SparseFloatMatrix(15, 10, 0)
    sfm[0, 0] = 1
    sfm[1, :] = 2
    sfm[:, 2] = 3
    sfm[10:, 5:] = 4

    assert sfm.shape == (15, 10)
    assert sfm[1, 0] == 2
    assert sfm[4, 4] == 0
    np.testing.assert_array_equal(sfm[:, 2], np.full(15, 3))
    np.testing.assert_array_equal(sfm[13:, 7:], np.full((2, 3), 4))


def test_array():
    sfm = bb.util.SparseFloatMatrix(15, 10, 0)
    sfm[0, 0] = 1
    sfm[1, :] = 2
    sfm[:, 2] = 3
    sfm[10:, 5:] = 4
    sfm[:5, :5] = 0

    array = np.zeros((15, 10))
    array[0, 0] = 1
    array[1, :] = 2
    array[:, 2] = 3
    array[10:, 5:] = 4
    array[:5, :5] = 0

    np.testing.assert_array_equal(np.asarray(sfm), array)


def test_copy():
    sfm1 = bb.util.SparseFloatMatrix(15, 10, 0)
    sfm1[0, 0] = 1
    sfm1[1, :] = 2

    sfm2 = sfm1.copy()
    np.testing.assert_array_equal(np.asarray(sfm1), np.asarray(sfm2))

    sfm2[:, 2] = 3
    # https://stackoverflow.com/questions/38506044/numpy-testing-assert-array-not-equal
    np.testing.assert_raises(AssertionError, np.testing.assert_array_equal, sfm1, sfm2)


def test_remove_col_row():
    sfm = bb.util.SparseFloatMatrix(15, 10, 0)

    sfm[1, :] = 2
    np.testing.assert_array_equal(sfm[1, :], np.full(10, 2))
    sfm.remove_row(-14, -2)
    np.testing.assert_array_equal(sfm[1, :], np.array([2, 2, 2, 2, 2, 2, 2, 2, 0, 0]))
    sfm.remove_row(1, 5)
    np.testing.assert_array_equal(sfm[1, :], np.array([2, 2, 2, 2, 2, 0, 0, 0, 0, 0]))

    sfm[:, 2] = 3
    np.testing.assert_array_equal(sfm[:, 2], np.full(15, 3))
    sfm.remove_column(-8, -3)
    np.testing.assert_array_equal(sfm[:, 2], np.array([3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0]))
    sfm.remove_column(2, 10)
    np.testing.assert_array_equal(sfm[:, 2], np.array([3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0]))


def test_set_area():
    sfm = bb.util.SparseFloatMatrix(15, 10, 0)
    array = np.zeros((15, 10))
    target = np.arange(12, dtype=float).reshape(3, 4)

    sfm[5:8, 1:5] = 666
    sfm.set_area((5, 8), (1, 5), target)
    array[5:8, 1:5] = target

    np.testing.assert_array_equal(np.asarray(sfm), array)


def test_set_indices():
    sfm = bb.util.SparseFloatMatrix(15, 10, 0)
    array = np.zeros((15, 10))

    sfm[0, 0] = 1
    sfm.set_indices(
        np.array([0, 1, 2, 3], dtype=int),
        np.array([0, 4, 5, 6], dtype=int),
        np.array([0, 7, 8, 9], dtype=float),
    )
    array[1, 4] = 7
    array[2, 5] = 8
    array[3, 6] = 9

    np.testing.assert_array_equal(np.asarray(sfm), array)
