#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.concat
#

import pytest

import brambox as bb


def test_concat(df_anno_simple):
    df = bb.util.concat([df_anno_simple, df_anno_simple], ignore_index=True)

    assert len(df) == 2 * len(df_anno_simple)
    assert list(df.columns) == list(df_anno_simple.columns)
    assert list(df.image.cat.categories) == list(df_anno_simple.image.cat.categories)
