#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test dualgroupby function
#

import pytest

import brambox as bb


def test_apply(df_anno_simple, df_det_simple):
    result = bb.util.DualGroupBy(df_anno_simple, df_det_simple, 'image').apply(lambda g1, g2: [len(g1), len(g2)])
    assert list(result.values) == [[2, 2], [1, 1], [0, 0]]


def test_iter(df_anno_simple, df_det_simple):
    result = []
    for _, g1, g2 in bb.util.DualGroupBy(df_anno_simple, df_det_simple, 'image'):
        result.append([len(g1), len(g2)])

    assert result == [[2, 2], [1, 1], [0, 0]]


def test_np_apply(df_anno_simple, df_det_simple):
    result = bb.util.DualGroupByNumpy(df_anno_simple, df_det_simple, 'image').apply(lambda d1, d2: d1['width'] * d1['height'])
    assert list(result) == [400, 1000, 1800]


def test_np_iter(df_anno_simple, df_det_simple):
    result = []
    for _g1, g2 in bb.util.DualGroupByNumpy(df_anno_simple, df_det_simple, 'image'):
        result.extend(g2['width'] * g2['height'])

    assert list(result) == [400, 1000, 1800]
