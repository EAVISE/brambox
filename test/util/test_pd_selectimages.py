#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.select_images
#

import pytest

import brambox as bb


def test_selectimages(df_anno_simple):
    df = bb.util.select_images(df_anno_simple, ['img02'])

    assert len(df) == len(df_anno_simple[df_anno_simple.image == 'img02'])
    assert list(df.columns) == list(df_anno_simple.columns)
    assert list(df.image.cat.categories) == ['img02']


def test_selectimages_nodata(df_anno_simple):
    df = bb.util.select_images(df_anno_simple, ['img03'])

    assert len(df) == 0
    assert list(df.columns) == list(df_anno_simple.columns)
    assert list(df.image.cat.categories) == ['img03']


def test_selectimages_multiple(df_anno_simple):
    df = bb.util.select_images(df_anno_simple, ['img01', 'img03'])

    assert len(df) == len(df_anno_simple[df_anno_simple.image == 'img01'])
    assert list(df.columns) == list(df_anno_simple.columns)
    assert list(df.image.cat.categories) == ['img01', 'img03']
