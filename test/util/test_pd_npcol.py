#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test pandas util functions
#

import numpy as np
import pytest

import brambox as bb


def test_npcol(df_anno_simple):
    col = bb.util.np_col(df_anno_simple, 'x_top_left')
    assert np.all(col == np.array([1.0, 2.0, 3.0]))

    col = bb.util.np_col(df_anno_simple, 'image')
    assert np.all(col == np.array(['img01', 'img01', 'img02']))


def test_npcol_dict():
    col = bb.util.np_col({'a': np.array([1, 2, 3]), 'b': np.array([4, 5, 6])}, 'b')
    assert np.all(col == np.array([4, 5, 6]))
