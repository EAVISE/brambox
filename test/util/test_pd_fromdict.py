#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.from_dict
#

import numpy as np
import pytest

import brambox as bb

raw_dict = {
    'image': ['img01', 'img01', 'img02'],
    'class_label': ['person', 'person', 'dog'],
    'x_top_left': [1, 2, 3],
    'y_top_left': [4, 5, 6],
    'width': [10, 20, 30],
    'height': [40, 50, 60],
    'ignore': [False, False, False],
}


def test_fromdict(df_anno_simple):
    df = bb.util.from_dict(raw_dict, ['img01', 'img02', 'img03'])
    assert df_anno_simple.equals(df)


def test_fromdict_infer_images(df_anno_simple):
    df_anno_simple.image = df_anno_simple.image.cat.remove_categories(['img03'])
    df = bb.util.from_dict(raw_dict)
    assert df_anno_simple.equals(df)


def test_fromdict_missing_cols():
    raw_dict = {}
    with pytest.raises(ValueError) as errinfo:
        bb.util.from_dict(raw_dict)
    assert 'needs the following keys' in str(errinfo.value)
