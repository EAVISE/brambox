#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._boundingbox.BoundingBoxTransformer
#
import numpy as np
import pandas as pd
import pytest
import shapely

import brambox as bb


def test_hbb_hbb(df_anno_simple, assert_dataframes):
    hbb = bb.util.BoundingBoxTransformer(df_anno_simple).get_hbb()

    assert_dataframes(hbb, df_anno_simple)


def test_hbb_obb(df_anno_simple, assert_dataframes):
    obb = bb.util.BoundingBoxTransformer(df_anno_simple).get_obb()

    assert list(obb['angle']) == [0, 0, 0]
    assert_dataframes(
        obb[['x_top_left', 'y_top_left', 'width', 'height']],
        df_anno_simple[['x_top_left', 'y_top_left', 'width', 'height']],
    )


def test_hbb_poly(df_anno_simple, assert_dataframes, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_simple).get_poly()

    assert_shapely(
        poly['segmentation'],
        np.array(
            [
                shapely.box(1, 4, 11, 44),
                shapely.box(2, 5, 22, 55),
                shapely.box(3, 6, 33, 66),
            ]
        ),
    )
    assert_dataframes(
        poly[['x_top_left', 'y_top_left', 'width', 'height']],
        df_anno_simple[['x_top_left', 'y_top_left', 'width', 'height']],
    )


def test_hbb_poly_hbb(df_anno_simple, assert_dataframes, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_simple).get_hbb_poly()

    assert_shapely(
        poly['segmentation'],
        [
            shapely.box(1, 4, 11, 44),
            shapely.box(2, 5, 22, 55),
            shapely.box(3, 6, 33, 66),
        ],
    )
    assert_dataframes(
        poly[['x_top_left', 'y_top_left', 'width', 'height']],
        df_anno_simple[['x_top_left', 'y_top_left', 'width', 'height']],
    )


def test_hbb_poly_obb(df_anno_simple, assert_dataframes, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_simple).get_obb_poly()

    assert_shapely(
        poly['segmentation'],
        np.array(
            [
                shapely.box(1, 4, 11, 44),
                shapely.box(2, 5, 22, 55),
                shapely.box(3, 6, 33, 66),
            ]
        ),
    )
    assert_dataframes(
        poly[['x_top_left', 'y_top_left', 'width', 'height']],
        df_anno_simple[['x_top_left', 'y_top_left', 'width', 'height']],
    )


def test_obb_hbb(df_anno_oriented):
    hbb = bb.util.BoundingBoxTransformer(df_anno_oriented).get_hbb()

    # Values computed / checked with vector drawing program
    assert hbb['x_top_left'].tolist() == pytest.approx([1, 2, 3], abs=0.01)
    assert hbb['y_top_left'].tolist() == pytest.approx([-4.660, -9.142, -9], abs=0.01)
    assert hbb['width'].tolist() == pytest.approx([39.641, 49.497, 55.981], abs=0.01)
    assert hbb['height'].tolist() == pytest.approx([28.660, 49.497, 66.962], abs=0.01)


def test_obb_obb(df_anno_oriented, assert_dataframes):
    obb = bb.util.BoundingBoxTransformer(df_anno_oriented).get_obb()

    assert_dataframes(obb, df_anno_oriented)


def test_obb_poly(df_anno_oriented, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_oriented).get_poly()

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([1, 2, 3], abs=0.01)
    assert poly['y_top_left'].tolist() == pytest.approx([-4.660, -9.142, -9], abs=0.01)
    assert poly['width'].tolist() == pytest.approx([39.641, 49.497, 55.981], abs=0.01)
    assert poly['height'].tolist() == pytest.approx([28.660, 49.497, 66.962], abs=0.01)
    assert_shapely(
        poly['segmentation'],
        np.array(
            [
                shapely.polygons([[1, 4], [35.641, 24], [40.641, 15.340], [6, -4.660]]),
                shapely.polygons([[2, 5], [37.355, 40.355], [51.497, 26.213], [16.142, -9.142]]),
                shapely.polygons([[3, 6], [33, 57.962], [58.981, 42.962], [28.981, -9]]),
            ]
        ),
        tolerance=0.001,
    )


def test_obb_poly_hbb(df_anno_oriented, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_oriented).get_hbb_poly()

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([1, 2, 3], abs=0.01)
    assert poly['y_top_left'].tolist() == pytest.approx([-4.660, -9.142, -9], abs=0.01)
    assert poly['width'].tolist() == pytest.approx([39.641, 49.497, 55.981], abs=0.01)
    assert poly['height'].tolist() == pytest.approx([28.660, 49.497, 66.962], abs=0.01)
    assert_shapely(
        poly['segmentation'],
        np.array(
            [
                shapely.polygons([[1, -4.660], [1, 24], [40.641, 24], [40.641, -4.660]]),
                shapely.polygons([[2, -9.142], [2, 40.355], [51.497, 40.355], [51.497, -9.142]]),
                shapely.polygons([[3, -9], [3, 57.962], [58.981, 57.962], [58.981, -9]]),
            ]
        ),
        tolerance=0.001,
    )


def test_obb_poly_obb(df_anno_oriented, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_oriented).get_obb_poly()

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([1, 2, 3], abs=0.01)
    assert poly['y_top_left'].tolist() == pytest.approx([-4.660, -9.142, -9], abs=0.01)
    assert poly['width'].tolist() == pytest.approx([39.641, 49.497, 55.981], abs=0.01)
    assert poly['height'].tolist() == pytest.approx([28.660, 49.497, 66.962], abs=0.01)
    assert_shapely(
        poly['segmentation'],
        np.array(
            [
                shapely.polygons([[1, 4], [35.641, 24], [40.641, 15.340], [6, -4.660]]),
                shapely.polygons([[2, 5], [37.355, 40.355], [51.497, 26.213], [16.142, -9.142]]),
                shapely.polygons([[3, 6], [33, 57.962], [58.981, 42.962], [28.981, -9]]),
            ]
        ),
        tolerance=0.001,
    )


def test_poly_hbb(df_anno_poly):
    hbb = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb()

    assert hbb['x_top_left'].tolist() == [1, 2.5, 3, 4]
    assert hbb['y_top_left'].tolist() == [4, 5.5, 6.5, 7]
    assert hbb['width'].tolist() == [10, 0, 30, 40]
    assert hbb['height'].tolist() == [40, 0, 0, 70]


def test_poly_hbb_buffer(df_anno_poly, assert_dataframes):
    hbb = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb(buffer=1)

    assert_dataframes(
        hbb[['x_top_left', 'y_top_left', 'width', 'height']],
        df_anno_poly[['x_top_left', 'y_top_left', 'width', 'height']],
    )


def test_poly_obb(df_anno_poly):
    obb = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb()

    # Values computed / checked with vector drawing program
    assert obb['x_top_left'].tolist() == pytest.approx([0.908, 2.5, 3, 4], abs=1e-3)
    assert obb['y_top_left'].tolist() == pytest.approx([3.823, 5.5, 6.5, 7], abs=1e-3)
    assert obb['width'].tolist() == pytest.approx([8.462, 0, 30, 0], abs=1e-3)
    assert obb['height'].tolist() == pytest.approx([39.787, 0, 0, (40**2 + 70**2) ** (1 / 2)], abs=1e-3)
    assert obb['angle'].tolist() == pytest.approx([0.0526, 0, 0, 0.51915], abs=1e-3)


def test_poly_obb_buffer(df_anno_poly):
    obb = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb(buffer=1)

    # Values computed / checked with vector drawing program
    assert obb['x_top_left'].tolist() == pytest.approx([0.908, 2, 3, 3.566], abs=1e-3)
    assert obb['y_top_left'].tolist() == pytest.approx([3.823, 5, 6, 6.752], abs=1e-3)
    assert obb['width'].tolist() == pytest.approx([8.462, 1, 30, 1], abs=1e-3)
    assert obb['height'].tolist() == pytest.approx([39.787, 1, 1, (40**2 + 70**2) ** (1 / 2)], abs=1e-3)
    assert obb['angle'].tolist() == pytest.approx([0.0526, 0, 0, 0.51915], abs=1e-3)


def test_poly_poly(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_poly()

    assert poly['x_top_left'].tolist() == [1, 2.5, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5.5, 6.5, 7]
    assert poly['width'].tolist() == [10, 0, 30, 40]
    assert poly['height'].tolist() == [40, 0, 0, 70]
    assert_shapely(poly['segmentation'], df_anno_poly['segmentation'])


def test_poly_poly_force(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_poly(force_poly=True)

    assert poly['x_top_left'].tolist() == [1, 2.5, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5.5, 6.5, 7]
    assert poly['width'].tolist() == [10, 0, 30, 40]
    assert poly['height'].tolist() == [40, 0, 0, 70]

    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[1, 6], [6, 4], [11, 35], [3, 44]]),
            shapely.box(2.5, 5.5, 2.5, 5.5),
            # We need to encode this as a polygon to have the correct point ordering
            shapely.polygons([[3, 6.5], [33, 6.5], [33, 6.5], [3, 6.5], [3, 6.5]]),
            shapely.polygons([[4, 7], [44, 77], [44, 77], [4, 7], [4, 7]]),
        ],
    )


def test_poly_poly_buffer(df_anno_poly, assert_dataframes):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_poly(buffer=1)

    assert_dataframes(poly, df_anno_poly)


def test_poly_poly_buffer_poly(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_poly(buffer_poly=1)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([1, 2, 3, 3.566], abs=1e-3)
    assert poly['y_top_left'].tolist() == pytest.approx([4, 5, 6, 6.752], abs=1e-3)
    assert poly['width'].tolist() == pytest.approx([10, 1, 30, 40.868], abs=1e-3)
    assert poly['height'].tolist() == pytest.approx([40, 1, 1, 70.496], abs=1e-3)
    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[1, 6], [6, 4], [11, 35], [3, 44]]),
            shapely.polygons([[3, 6], [3, 5], [2, 5], [2, 6]]),
            shapely.polygons([[33, 7], [33, 6], [3, 6], [3, 7]]),
            shapely.polygons([[43.6, 77.2], [44.4, 76.8], [4.4, 6.8], [3.6, 7.2]]),
        ],
        tolerance=0.1,
    )


def test_poly_poly_hbb(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb_poly()

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == [1, 2.5, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5.5, 6.5, 7]
    assert poly['width'].tolist() == [10, 0, 30, 40]
    assert poly['height'].tolist() == [40, 0, 0, 70]
    assert_shapely(
        poly['segmentation'],
        [
            shapely.box(1, 4, 11, 44),
            shapely.points([2.5, 5.5]),
            shapely.linestrings([[3, 6.5], [33, 6.5]]),
            shapely.box(4, 7, 44, 77),
        ],
        tolerance=0.01,
    )


def test_poly_poly_hbb_force(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb_poly(force_poly=True)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == [1, 2.5, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5.5, 6.5, 7]
    assert poly['width'].tolist() == [10, 0, 30, 40]
    assert poly['height'].tolist() == [40, 0, 0, 70]
    assert_shapely(
        poly['segmentation'],
        [
            shapely.box(1, 4, 11, 44),
            shapely.box(2.5, 5.5, 2.5, 5.5),
            # We need to encode this as a polygon to have the correct point ordering
            shapely.polygons([[3, 6.5], [33, 6.5], [33, 6.5], [3, 6.5], [3, 6.5]]),
            shapely.box(4, 7, 44, 77),
        ],
        tolerance=0.01,
    )


def test_poly_poly_hbb_buffer(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb_poly(buffer=1)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == [1, 2, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5, 6, 7]
    assert poly['width'].tolist() == [10, 1, 30, 40]
    assert poly['height'].tolist() == [40, 1, 1, 70]
    assert_shapely(
        poly['segmentation'],
        [
            shapely.box(1, 4, 11, 44),
            shapely.points([2.5, 5.5]),
            shapely.linestrings([[3, 6.5], [33, 6.5]]),
            shapely.box(4, 7, 44, 77),
        ],
        tolerance=0.01,
    )


def test_poly_poly_hbb_buffer_poly(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_hbb_poly(buffer_poly=1)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == [1, 2, 3, 4]
    assert poly['y_top_left'].tolist() == [4, 5, 6, 7]
    assert poly['width'].tolist() == [10, 1, 30, 40]
    assert poly['height'].tolist() == [40, 1, 1, 70]
    assert_shapely(
        poly['segmentation'],
        [
            shapely.box(1, 4, 11, 44),
            shapely.box(2, 5, 3, 6),
            shapely.box(3, 6, 33, 7),
            shapely.box(4, 7, 44, 77),
        ],
        tolerance=0.01,
    )


def test_poly_poly_obb(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb_poly()

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([0.908, 2.5, 3, 4], abs=1e-3)
    assert poly['y_top_left'].tolist() == pytest.approx([3.823, 5.5, 6.5, 7], abs=1e-3)
    assert poly['width'].tolist() == pytest.approx([10.541, 0, 30, 40], abs=1e-3)
    assert poly['height'].tolist() == pytest.approx([40.177, 0, 0, 70], abs=1e-3)
    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[0.909, 4.268], [3, 44], [11.450, 43.555], [9.359, 3.823]]),
            shapely.points([2.5, 5.5]),
            shapely.linestrings([[3, 6.5], [33, 6.5]]),
            shapely.linestrings([[4, 7], [44, 77]]),
        ],
        tolerance=0.001,
    )


def test_poly_poly_obb_force(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb_poly(force_poly=True)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([0.908, 2.5, 3, 4], abs=1e-3)
    assert poly['y_top_left'].tolist() == pytest.approx([3.823, 5.5, 6.5, 7], abs=1e-3)
    assert poly['width'].tolist() == pytest.approx([10.541, 0, 30, 40], abs=1e-3)
    assert poly['height'].tolist() == pytest.approx([40.177, 0, 0, 70], abs=1e-3)
    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[0.909, 4.268], [3, 44], [11.450, 43.555], [9.359, 3.823]]),
            shapely.box(2.5, 5.5, 2.5, 5.5),
            shapely.polygons([[3, 6.5], [33, 6.5], [33, 6.5], [3, 6.5], [3, 6.5]]),
            shapely.polygons([[4, 7], [44, 77], [44, 77], [4, 7], [4, 7]]),
        ],
        tolerance=0.001,
    )


def test_poly_poly_obb_buffer(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb_poly(buffer=1)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([0.908, 2, 3, 4], abs=1e-3)
    assert poly['y_top_left'].tolist() == pytest.approx([3.823, 5, 6, 7], abs=1e-3)
    assert poly['width'].tolist() == pytest.approx([10.541, 1, 30, 40], abs=1e-3)
    assert poly['height'].tolist() == pytest.approx([40.177, 1, 1, 70], abs=1e-3)
    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[0.909, 4.268], [3, 44], [11.450, 43.555], [9.359, 3.823]]),
            shapely.points([2.5, 5.5]),
            shapely.linestrings([[3, 6.5], [33, 6.5]]),
            shapely.linestrings([[4, 7], [44, 77]]),
        ],
        tolerance=0.001,
    )


def test_poly_poly_obb_buffer_poly(df_anno_poly, assert_shapely):
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_obb_poly(buffer_poly=1)

    # Values computed / checked with vector drawing program
    assert poly['x_top_left'].tolist() == pytest.approx([0.908, 2, 3, 3.565], abs=1e-3)
    assert poly['y_top_left'].tolist() == pytest.approx([3.823, 5, 6, 6.751], abs=1e-3)
    assert poly['width'].tolist() == pytest.approx([10.541, 1, 30, 40.868], abs=1e-3)
    assert poly['height'].tolist() == pytest.approx([40.177, 1, 1, 70.497], abs=1e-3)
    assert_shapely(
        poly['segmentation'],
        [
            shapely.polygons([[0.909, 4.268], [3, 44], [11.450, 43.555], [9.359, 3.823]]),
            shapely.box(2, 5, 3, 6),
            shapely.box(3, 6, 33, 7),
            shapely.polygons([[3.565, 7.249], [43.568, 77.251], [44.432, 76.750], [4.434, 6.754]]),
        ],
        tolerance=0.01,
    )


def test_poly_poly_none(df_anno_poly, assert_shapely):
    df_anno_poly.loc[0, 'segmentation'] = None
    poly = bb.util.BoundingBoxTransformer(df_anno_poly).get_poly()

    assert poly.at[0, 'x_top_left'] == 1
    assert poly.at[0, 'y_top_left'] == 4
    assert poly.at[0, 'width'] == 10
    assert poly.at[0, 'height'] == 40
    assert_shapely(poly.at[0, 'segmentation'], shapely.box(1, 4, 11, 44))


def test_empty_hbb(df_anno_poly):
    df = df_anno_poly.loc[0:-1]
    assert len(df) == 0

    res = bb.util.BoundingBoxTransformer(df).get_hbb()
    assert len(res) == 0
    assert set(res.columns) == {
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'ignore',
    }


def test_empty_obb(df_anno_poly):
    df = df_anno_poly.loc[0:-1]
    assert len(df) == 0

    res = bb.util.BoundingBoxTransformer(df).get_obb()
    assert len(res) == 0
    assert set(res.columns) == {
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'angle',
        'ignore',
    }


def test_empty_poly(df_anno_poly):
    df = df_anno_poly.loc[0:-1]
    assert len(df) == 0

    res = bb.util.BoundingBoxTransformer(df).get_poly()
    assert len(res) == 0
    assert set(res.columns) == {
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'segmentation',
        'ignore',
    }
    assert str(res['segmentation'].dtype) == 'geos'


def test_empty_poly_hbb(df_anno_poly):
    df = df_anno_poly.loc[0:-1]
    assert len(df) == 0

    res = bb.util.BoundingBoxTransformer(df).get_hbb_poly()
    assert len(res) == 0
    assert set(res.columns) == {
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'segmentation',
        'ignore',
    }
    assert str(res['segmentation'].dtype) == 'geos'


def test_empty_poly_obb(df_anno_poly):
    df = df_anno_poly.loc[0:-1]
    assert len(df) == 0

    res = bb.util.BoundingBoxTransformer(df).get_obb_poly()
    assert len(res) == 0
    assert set(res.columns) == {
        'image',
        'class_label',
        'x_top_left',
        'y_top_left',
        'width',
        'height',
        'segmentation',
        'ignore',
    }
    assert str(res['segmentation'].dtype) == 'geos'
