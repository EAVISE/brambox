#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._pd.split_images
#

import pytest

import brambox as bb


def test_splitimages(df_anno_simple):
    df1, df2 = bb.util.split_images(df_anno_simple, ['img02'])

    assert len(df1) + len(df2) == len(df_anno_simple)
    assert list(df1.image.cat.categories) == ['img02']
    assert sorted(df2.image.cat.categories) == ['img01', 'img03']


def test_splitimages_noremainder(df_anno_simple):
    df1 = bb.util.split_images(df_anno_simple, ['img01', 'img03'], remainder=False)[0]

    assert list(df1.image.cat.categories) == ['img01', 'img03']
    assert len(df1) == 2


def test_splitimages_multiple(df_anno_simple):
    df1, df2 = bb.util.split_images(df_anno_simple, ['img01'], ['img02'], remainder=False)

    assert len(df1) + len(df2) == len(df_anno_simple)
    assert list(df1.image.cat.categories) == ['img01']
    assert sorted(df2.image.cat.categories) == ['img02']
