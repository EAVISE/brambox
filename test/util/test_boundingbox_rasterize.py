#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.util._boundingbox.rasterize_segmentation
#
from math import ceil, floor

import numpy as np
import pytest
import shapely

import brambox as bb


def test_rasterize(df_anno_poly):
    rasterized = bb.util.rasterize_segmentation(df_anno_poly, (100, 100))

    # Mask 0
    # TODO : Fix this manual mask
    # mask = np.zeros((100, 100), dtype=np.uint8)
    # for y in range(4, 45):
    #     if y < 6:
    #         x0 = floor((y - 4) / 2 * -5) + 6
    #     else:
    #         x0 = floor((y - 6) / 38 * 2) + 1

    #     if y < 35:
    #         x1 = ceil((y - 4) / 31 * 5) + 6
    #     else:
    #         x1 = ceil((y - 35) / 9 * -8) + 11

    #     mask[y, x0:x1+1] = 1
    # assert np.all(rasterized.loc[0, 'segmentation'] == mask)

    # Mask 1
    mask = np.zeros((100, 100), dtype=np.uint8)
    mask[5, 2] = 1
    assert np.all(rasterized.loc[1, 'segmentation'] == mask)

    # Mask 2
    mask = np.zeros((100, 100), dtype=np.uint8)
    mask[6, 3:34] = 1
    assert np.all(rasterized.loc[2, 'segmentation'] == mask)

    # Mask 3
    mask = np.zeros((100, 100), dtype=np.uint8)
    # 7 -> 77 ; 4 -> 44
    for y in range(7, 78):
        x = round((y - 7) / 70 * 40) + 4
        mask[y, x] = 1
    assert np.all(rasterized.loc[3, 'segmentation'] == mask)


def test_rasterize_out_shapes(df_anno_poly):
    rasterized = bb.util.rasterize_segmentation(
        df_anno_poly,
        [(100, 100), (10, 10), (50, 50), (100, 100)],
    )

    # Mask 1
    mask = np.zeros((10, 10), dtype=np.uint8)
    mask[5, 2] = 1
    assert np.all(rasterized.loc[1, 'segmentation'] == mask)

    # Mask 2
    mask = np.zeros((50, 50), dtype=np.uint8)
    mask[6, 3:34] = 1
    assert np.all(rasterized.loc[2, 'segmentation'] == mask)

    # Mask 3
    mask = np.zeros((100, 100), dtype=np.uint8)
    # 7 -> 77 ; 4 -> 44
    for y in range(7, 78):
        x = round((y - 7) / 70 * 40) + 4
        mask[y, x] = 1
    assert np.all(rasterized.loc[3, 'segmentation'] == mask)


def test_rasterize_rescale(df_anno_poly):
    rasterized = bb.util.rasterize_segmentation(df_anno_poly, (100, 100), rescale=2)

    # Mask 1
    mask = np.zeros((100, 100), dtype=np.uint8)
    mask[11, 5] = 1
    assert np.all(rasterized.loc[1, 'segmentation'] == mask)

    # Mask 2
    mask = np.zeros((100, 100), dtype=np.uint8)
    mask[13, 6:67] = 1
    assert np.all(rasterized.loc[2, 'segmentation'] == mask)


def test_rasterize_dtype(df_anno_poly):
    rasterized = bb.util.rasterize_segmentation(df_anno_poly, (100, 100), dtype=float)

    # Mask 1
    mask = np.zeros((100, 100), dtype=float)
    mask[5, 2] = 1
    assert rasterized.loc[1, 'segmentation'].dtype == float
    assert np.all(rasterized.loc[1, 'segmentation'] == mask)

    # Mask 2
    mask = np.zeros((100, 100), dtype=float)
    mask[6, 3:34] = 1
    assert rasterized.loc[2, 'segmentation'].dtype == float
    assert np.all(rasterized.loc[2, 'segmentation'] == mask)


def test_rasterize_inplace(df_anno_poly, assert_dataframes):
    rasterized = bb.util.rasterize_segmentation(df_anno_poly, (100, 100))
    bb.util.rasterize_segmentation(df_anno_poly, (100, 100), inplace=True)

    assert_dataframes(rasterized, df_anno_poly)
