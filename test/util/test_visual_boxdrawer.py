#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test BoxDrawer class
#

import pytest

import brambox as bb

try:
    import cv2
    import numpy as np
except ModuleNotFoundError:
    cv2 = None

try:
    from PIL import Image, ImageDraw
except ModuleNotFoundError:
    Image = None


@pytest.mark.skipif(Image is None and cv2 is None, reason='No image visualization libraries installed')
def test_boxdrawer_basic(df_anno_simple):
    bd = bb.util.BoxDrawer(lambda a: None, df_anno_simple)
    assert len(bd) == 3

    bd = bb.util.BoxDrawer(lambda a: None, df_anno_simple, show_empty=False)
    assert len(bd) == 2

    with pytest.raises(TypeError) as errinfo:
        bd[0]
    assert 'Unkown image type' in str(errinfo.value)


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_boxdrawer_pil_basic(df_anno_simple):
    bd = bb.util.BoxDrawer(lambda a: Image.new('RGB', (75, 75)), df_anno_simple, color='red')

    # test img01
    res = bd[0]
    img = Image.new('RGB', (75, 75))
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 4), (11, 4), (11, 44), (1, 44), (1, 4)], 'red', 3)
    imgd.line([(2, 5), (22, 5), (22, 55), (2, 55), (2, 5)], 'red', 3)
    assert list(img.getdata()) == list(res.getdata())

    # test img02
    res = bd[1]
    img = Image.new('RGB', (75, 75))
    imgd = ImageDraw.Draw(img)
    imgd.line([(3, 6), (33, 6), (33, 66), (3, 66), (3, 6)], 'red', 3)
    assert list(img.getdata()) == list(res.getdata())

    # test img03
    res = bd[2]
    img = Image.new('RGB', (75, 75))
    assert list(img.getdata()) == list(res.getdata())

    # test int vs str index
    res = bd[0]
    res2 = bd['img01']
    assert list(res.getdata()) == list(res2.getdata())


@pytest.mark.skipif(Image is None, reason='Pillow not installed')
def test_boxdrawer_pil_l(df_anno_simple):
    bd = bb.util.BoxDrawer(lambda a: Image.new('L', (75, 75)), df_anno_simple, color='red')

    # test img01
    res = bd[0]
    img = Image.new('RGB', (75, 75))
    imgd = ImageDraw.Draw(img)
    imgd.line([(1, 4), (11, 4), (11, 44), (1, 44), (1, 4)], 'red', 3)
    imgd.line([(2, 5), (22, 5), (22, 55), (2, 55), (2, 5)], 'red', 3)
    assert list(img.getdata()) == list(res.getdata())


@pytest.mark.skipif(cv2 is None, reason='Pillow not installed')
def test_boxdrawer_cv_basic(df_anno_simple):
    bd = bb.util.BoxDrawer(
        {'img01': np.zeros((75, 75, 3), np.uint8), 'img02': np.zeros((75, 75, 3), np.uint8), 'img03': np.zeros((75, 75, 3), np.uint8)},
        df_anno_simple,
        color=(255, 0, 0),
    )

    # test img01
    res = bd[0]
    img = np.zeros((75, 75, 3), np.uint8)
    cv2.rectangle(img, (1, 4), (11, 44), (0, 0, 255), 3)
    cv2.rectangle(img, (2, 5), (22, 55), (0, 0, 255), 3)
    assert np.array_equal(img, res)

    # test img02
    res = bd[1]
    img = np.zeros((75, 75, 3), np.uint8)
    cv2.rectangle(img, (3, 6), (33, 66), (0, 0, 255), 3)
    assert np.array_equal(img, res)

    # test img03
    res = bd[2]
    img = np.zeros((75, 75, 3), np.uint8)
    assert np.array_equal(img, res)

    # test int vs str index
    res = bd[0]
    res2 = bd['img01']
    assert np.array_equal(res, res2)
