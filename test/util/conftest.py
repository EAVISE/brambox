#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Brambox util fixtures
#
from collections import namedtuple

import pytest


@pytest.fixture(scope='module')
def Box():
    """
    This fixture returns a Box namedtuple, mocking the return value from itertuples.
    """
    return namedtuple(
        'Box',
        ('points', 'label', 'size', 'color', 'alpha', 'segmentation'),
        defaults=(1, None),
    )
