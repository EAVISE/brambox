#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test tide evaluation
#
import os

import pandas as pd
import pytest

import brambox as bb


@pytest.mark.long
def test_tide():
    # Note that we test our code on the FasterRCNNX-101 code and not the published masked RCNN version
    anno = bb.io.load('anno_coco', os.path.join(os.path.dirname(__file__), './data/instances_val2017.json'))
    det = bb.io.load(
        'det_coco',
        os.path.join(os.path.dirname(__file__), './data/FasterRCNNX-101-32x8d-FPN.json'),
        class_label_map=[line.strip() for line in open(os.path.join(os.path.dirname(__file__), './data/coco.names'), 'r')],  # noqa: SIM115
    )
    det.image = det.image.cat.rename_categories(lambda img: f'{img:012}')

    tide = bb.eval.TIDE(det, anno, tqdm=False)
    mdAP = tide.mdAP

    # The following mdAP values match the official TIDE code really well!
    assert mdAP.mAP == pytest.approx(0.6369, abs=0.001)
    assert mdAP.mdAP_both == pytest.approx(0.0116, abs=0.001)
    assert mdAP.mdAP_duplicate == pytest.approx(0.0021, abs=0.001)
    assert mdAP.mdAP_background == pytest.approx(0.0405, abs=0.001)
    assert mdAP.mdAP_fp == pytest.approx(0.1668, abs=0.001)

    # The following mdAP values are not as close as the original code, but still pretty decent...
    assert mdAP.mdAP_localisation == pytest.approx(0.0700, abs=0.002)
    assert mdAP.mdAP_missed == pytest.approx(0.0706, abs=0.002)
    assert mdAP.mdAP_classification == pytest.approx(0.0251, abs=0.003)
    assert mdAP.mdAP_fn == pytest.approx(0.1400, abs=0.003)

    # Check that error dataframes are correctly returned
    det_err, anno_err = tide.errors
    assert 'error' in det_err.columns
    assert set(det_err['error'].unique()) == {'classification', 'localisation', 'both', 'duplicate', 'background', pd.NA}
    assert 'annotation' in det_err.columns
    assert set(det_err.loc[det_err['annotation'].isna(), 'error'].unique()) == {'background'}, 'Only background errors have no matching annotation'
    assert (det_err['annotation'] >= 0).all(), 'Annotation index is always greater than zero'
    assert 'error' in anno_err.columns
    assert set(anno_err['error'].unique()) == {'missed', pd.NA}

    # Check reset works
    tide.reset()
    assert len(tide.cache) == 0
