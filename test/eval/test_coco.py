#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test coco evaluation
#
import os

import pytest

import brambox as bb


@pytest.mark.long
def test_coco():
    anno = bb.io.load('anno_coco', os.path.join(os.path.dirname(__file__), './data/instances_val2017.json'))
    det = bb.io.load(
        'det_coco',
        os.path.join(os.path.dirname(__file__), './data/FasterRCNNX-101-32x8d-FPN.json'),
        class_label_map=[line.strip() for line in open(os.path.join(os.path.dirname(__file__), './data/coco.names'), 'r')],  # noqa: SIM115
    )
    det.image = det.image.cat.rename_categories(lambda img: f'{img:012}')

    coco = bb.eval.COCO(det, anno, tqdm=False)
    mAP = coco.mAP

    assert mAP.mAP_50 == pytest.approx(0.637, abs=0.001)
    assert mAP.mAP_75 == pytest.approx(0.447, abs=0.001)
    assert mAP.mAP_coco == pytest.approx(0.413, abs=0.001)

    # Small, Medium, Large dont have the same area computation, as pycocotools uses segmentation masks for those
    assert mAP.mAP_small == pytest.approx(0.255, abs=0.060)
    assert mAP.mAP_medium == pytest.approx(0.453, abs=0.060)
    assert mAP.mAP_large == pytest.approx(0.529, abs=0.010)
