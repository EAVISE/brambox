#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test bb.io.register_parser
#

import pytest

import brambox as bb


@pytest.mark.dependency()
def test_registerparser_anno():
    bb.io.register_parser('dummy', bb.io.AnnotationParser)
    assert 'anno_dummy' in bb.io.formats
    assert bb.io.formats['anno_dummy'] == bb.io.AnnotationParser


@pytest.mark.dependency()
def test_registerparser_det():
    bb.io.register_parser('dummy', bb.io.DetectionParser)
    assert 'det_dummy' in bb.io.formats
    assert bb.io.formats['det_dummy'] == bb.io.DetectionParser


@pytest.mark.dependency()
def test_registerparser_box():
    bb.io.register_parser('dummy', bb.io.Parser)
    assert 'dummy' in bb.io.formats
    assert bb.io.formats['dummy'] == bb.io.Parser


def test_registerparser_invalidtype():
    with pytest.raises(TypeError) as errinfo:
        bb.io.register_parser('invalid', bb.io.ParserType)
    assert 'is not of type' in str(errinfo.value)


@pytest.mark.dependency(depends=['test_registerparser_anno', 'test_registerparser_det', 'test_registerparser_box'])
def test_registerparser_nameclash():
    with pytest.raises(KeyError) as errinfo:
        bb.io.register_parser('dummy', bb.io.parser.AnnotationParser)
    assert 'already registered for parser' in str(errinfo.value)

    with pytest.raises(KeyError) as errinfo:
        bb.io.register_parser('dummy', bb.io.parser.DetectionParser)
    assert 'already registered for parser' in str(errinfo.value)

    with pytest.raises(KeyError) as errinfo:
        bb.io.register_parser('dummy', bb.io.parser.Parser)
    assert 'already registered for parser' in str(errinfo.value)


def test_registerparser_decorator():
    decorator = bb.io.register_parser('dummy2')
    decorator(bb.io.AnnotationParser)
    assert 'anno_dummy2' in bb.io.formats
    assert bb.io.formats['anno_dummy2'] == bb.io.AnnotationParser


def teardown_module(module):
    if 'anno_dummy' in bb.io.formats:
        del bb.io.formats['anno_dummy']
    if 'anno_dummy2' in bb.io.formats:
        del bb.io.formats['anno_dummy2']
    if 'det_dummy' in bb.io.formats:
        del bb.io.formats['det_dummy']
    if 'dummy' in bb.io.formats:
        del bb.io.formats['dummy']
