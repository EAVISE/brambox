#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation HRSC parser
#
import pandas as pd
import pytest
import shapely

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.HRSCParser


raw_simple = {
    'img01': """<HRSC_Image><HRSC_Objects>
<HRSC_Object>
    <Class_ID>person</Class_ID>
    <truncated>0</truncated>
    <difficult>0</difficult>
    <box_xmin>1</box_xmin>
    <box_ymin>4</box_ymin>
    <box_xmax>11</box_xmax>
    <box_ymax>44</box_ymax>
    <mbox_cx>6.0</mbox_cx>
    <mbox_cy>24.0</mbox_cy>
    <mbox_w>10.0</mbox_w>
    <mbox_h>40.0</mbox_h>
    <mbox_ang>0.0</mbox_ang>
</HRSC_Object>
<HRSC_Object>
    <Class_ID>person</Class_ID>
    <truncated>0</truncated>
    <difficult>0</difficult>
    <box_xmin>2</box_xmin>
    <box_ymin>5</box_ymin>
    <box_xmax>22</box_xmax>
    <box_ymax>55</box_ymax>
    <mbox_cx>12.0</mbox_cx>
    <mbox_cy>30.0</mbox_cy>
    <mbox_w>20.0</mbox_w>
    <mbox_h>50.0</mbox_h>
    <mbox_ang>0.0</mbox_ang>
</HRSC_Object>
</HRSC_Objects></HRSC_Image>""",
    'img02': """<HRSC_Image><HRSC_Objects>
<HRSC_Object>
    <Class_ID>dog</Class_ID>
    <truncated>0</truncated>
    <difficult>0</difficult>
    <box_xmin>3</box_xmin>
    <box_ymin>6</box_ymin>
    <box_xmax>33</box_xmax>
    <box_ymax>66</box_ymax>
    <mbox_cx>18.0</mbox_cx>
    <mbox_cy>36.0</mbox_cy>
    <mbox_w>30.0</mbox_w>
    <mbox_h>60.0</mbox_h>
    <mbox_ang>0.0</mbox_ang>
</HRSC_Object>
</HRSC_Objects></HRSC_Image>""",
    'img03': """<HRSC_Image><HRSC_Objects></HRSC_Objects></HRSC_Image>""",
}


def test_basic(parser, df_anno_simple, assert_deserialize):
    parser = parser(extract_oriented=False)

    df_anno_simple['difficult'] = False
    df_anno_simple['truncated'] = 0.0

    assert_deserialize(parser, raw_simple, df_anno_simple)


def test_obb(parser, df_anno_simple, assert_deserialize):
    parser = parser()

    df_anno_simple['difficult'] = False
    df_anno_simple['truncated'] = 0.0
    df_anno_simple['segmentation'] = pd.Series(shapely.box([1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [11.0, 22.0, 33.0], [44.0, 55.0, 66.0])).astype('geos')

    assert_deserialize(parser, raw_simple, df_anno_simple)
