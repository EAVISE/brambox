#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test detection pascal voc parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.PascalVocParser


raw_simple = {
    'person': """img01 0.0 1 4 11 44
img01 1.0 2 5 22 55
""",
    'dog': """img02 0.0 3 6 33 66
""",
}


def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(images=['img01', 'img02', 'img03'])

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


def test_no_images(parser, df_det_simple, assert_deserialize, assert_serialize):
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()
    parser = parser()

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


raw_precision = {
    'person': """img01 0.0 1.0 4.04 11.11 44.04
img01 1.0 2.0 5.0 22.0 55.0
""",
    'dog': """img02 0.0 3.0 6.0 33.34 66.0
""",
}


def test_precision(parser, df_det_simple, assert_deserialize, assert_serialize):
    df_det_simple.at[0, 'y_top_left'] = 4.04
    df_det_simple.at[0, 'width'] = 10.1111
    df_det_simple.at[2, 'width'] = 30.335

    parser = parser(images=['img01', 'img02', 'img03'], precision=2)
    assert_serialize(parser, df_det_simple, raw_precision)

    parser.precision = None
    assert_serialize(parser, df_det_simple, raw_simple)
