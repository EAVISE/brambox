#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test detection coco parser
#

import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.CocoParser


raw_simple = """[
  {"image_id": 0, "category_id": 1, "bbox": [1, 4, 10, 40], "score": 0.0},
  {"image_id": 0, "category_id": 1, "bbox": [2, 5, 20, 50], "score": 1.0},
  {"image_id": 1, "category_id": 2, "bbox": [3, 6, 30, 60], "score": 0.0}
]"""


def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label_map=['person', 'dog'])
    df_det_simple.image = df_det_simple.image.cat.rename_categories([0, 1, 2])
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


def test_catcodes(parser, df_det_simple, assert_serialize):
    parser = parser(class_label_map=['person', 'dog'])
    assert_serialize(parser, df_det_simple, raw_simple)


raw_no_labelmap = """[
  {"image_id": 0, "category_id": 1, "bbox": [1, 4, 10, 40], "score": 0.0},
  {"image_id": 0, "category_id": 1, "bbox": [2, 5, 20, 50], "score": 1.0},
  {"image_id": 1, "category_id": 2, "bbox": [3, 6, 30, 60], "score": 0.0}
]"""


raw_no_labelmap_default = """[
  {"image_id": 0, "category_id": 1, "bbox": [1, 4, 10, 40], "score": 0.0},
  {"image_id": 0, "category_id": 1, "bbox": [2, 5, 20, 50], "score": 1.0},
  {"image_id": 1, "category_id": 1, "bbox": [3, 6, 30, 60], "score": 0.0}
]"""


def test_no_labelmap(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_simple.image = df_det_simple.image.cat.rename_categories([0, 1, 2])
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()

    df_det_simple.class_label = df_det_simple.class_label.map({'person': '1', 'dog': '2'})
    assert_deserialize(parser, raw_no_labelmap, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_no_labelmap)

    df_det_simple.class_label = df_det_simple.class_label.map({'1': 'person', '2': 'dog'})
    assert_serialize(parser, df_det_simple, raw_no_labelmap_default)


raw_precision = """[
  {"image_id": 0, "category_id": 1, "bbox": [1.0, 4.01, 10.01, 40.0], "score": 0.0},
  {"image_id": 0, "category_id": 1, "bbox": [2.0, 5.01, 20.01, 50.0], "score": 1.0},
  {"image_id": 1, "category_id": 2, "bbox": [3.0, 6.01, 30.01, 60.0], "score": 0.0}
]"""


def test_precision(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label_map=['person', 'dog'], precision=2)
    df_det_simple.image = df_det_simple.image.cat.rename_categories([0, 1, 2])
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()
    df_det_simple.y_top_left += 0.01
    df_det_simple.width += 0.006

    assert_serialize(parser, df_det_simple, raw_precision)
