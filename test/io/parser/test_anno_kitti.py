#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation kitti parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.KittiParser


raw_simple = {
    'img01': """person 0.10 0 -10 1.00 4.00 11.00 44.00 -1 -1 -1 -1000 -1000 -1000 -10
person 0.00 0 -10 2.00 5.00 22.00 55.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img02': """dog 0.00 0 -10 3.00 6.00 33.00 66.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img03': """""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['truncated'] = [0.1, 0.0, 0.0]
    df_anno_simple['occluded'] = [0, 0, 0]  # KITTI has occluded state instead of percentage

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_occ = {
    'img01': """person 0.00 0 -10 1.00 4.00 11.00 44.00 -1 -1 -1 -1000 -1000 -1000 -10
person 0.00 1 -10 2.00 5.00 22.00 55.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img02': """dog 0.00 2 -10 3.00 6.00 33.00 66.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img03': """""",
}


def test_occluded_state(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['truncated'] = [0.0, 0.0, 0.0]
    df_anno_simple['occluded'] = [0, 1, 2]

    assert_deserialize(parser, raw_occ, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_occ)


raw_occ_float = {
    'img01': """person 0.00 0.10 -10 1.00 4.00 11.00 44.00 -1 -1 -1 -1000 -1000 -1000 -10
person 0.00 0.00 -10 2.00 5.00 22.00 55.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img02': """dog 0.00 0.50 -10 3.00 6.00 33.00 66.00 -1 -1 -1 -1000 -1000 -1000 -10
""",
    'img03': """""",
}


def test_occluded_float(parser, df_anno_simple, assert_deserialize, assert_serialize):
    """Our own extension to KITTI to allow working with float occluded percentages"""
    parser = parser()

    df_anno_simple['truncated'] = [0.0, 0.0, 0.0]
    df_anno_simple['occluded'] = [0.1, 0.0, 0.5]

    assert_deserialize(parser, raw_occ_float, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_occ_float)
