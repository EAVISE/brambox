#
#   Copyright EAVISE
#   Author: Maarten Vandersteegen
#
#   Test annotation MOT parser
#
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.MotParser


raw_simple = {
    'vid01': """1,1,1.0,4.0,10.0,40.0,0,1,1.0
1,1,2.0,5.0,20.0,50.0,1,1,0.7
2,5,3.0,6.0,30.0,60.0,1,2,1.0
""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(['person', 'dog'], precision=1)

    df_anno_simple['image'] = df_anno_simple.image.cat.rename_categories(['vid01--1', 'vid01--2', 'vid01--3'])
    df_anno_simple['image'] = df_anno_simple.image.cat.remove_unused_categories()
    df_anno_simple['ignore'] = [True, False, False]
    df_anno_simple['id'] = pd.Series([1, 1, 5], dtype=pd.Int64Dtype())
    df_anno_simple['occluded'] = [1.0, 0.7, 1.0]
    df_anno_simple['video'] = ['vid01', 'vid01', 'vid01']
    df_anno_simple['frame'] = [1, 1, 2]

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)
