#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test anno cvat image parser
#

import pandas as pd
import pgpd
import pytest
import shapely

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.CvatVideoAnnoParser


raw_basic = """<?xml version="1.0" encoding="utf-8"?>
<annotations>
  <version>1.1</version>
  <meta><project>
    <tasks>
        <task>
          <id>42</id>
          <name>unittest</name>
          <start_frame>0</start_frame>
          <stop_frame>3</stop_frame>
        </task>
    </tasks>
    <labels>
        <label>
            <name>person</name>
            <type>any</type>
            <attributes>
                <attribute><name>truncated</name><input_type>number</input_type></attribute>
                <attribute><name>difficult</name><input_type>checkbox</input_type><default_value>false</default_value></attribute>
                <attribute><name>age</name><input_type>text</input_type></attribute>
            </attributes>
        </label>
    </labels>
  </project></meta>
  <track id="0" label="person" task_id="42">
    <box frame="0" keyframe="1" outside="0" xtl="1.0" ytl="5.0" xbr="11.0" ybr="55.0" occluded="0" z_order="0">
      <attribute name="truncated">0.0</attribute>
      <attribute name="age">adult</attribute>
    </box>
    <polygon frame="1" keyframe="0" outside="1" occluded="0" points="2.0,6.0;22.0,6.0;22.0,66.0;2.0,66.0" z_order="1">
      <attribute name="truncated">0.2</attribute>
      <attribute name="difficult">true</attribute>
      <attribute name="age">adult</attribute>
    </polygon>
    <polyline frame="2" keyframe="1" outside="0" occluded="1" points="3.0,7.0;33.0,7.0;33.0,77.0" z_order="0">
      <attribute name="truncated">0.0</attribute>
      <attribute name="difficult">true</attribute>
      <attribute name="age">multi</attribute>
    </polyline>
  </track>
  <track id="2" label="person" task_id="42">
    <points frame="2" keyframe="1" outside="0" occluded="0" points="4.0,8.0;44.0,88.0" z_order="0">
      <attribute name="truncated">1.0</attribute>
      <attribute name="difficult">false</attribute>
      <attribute name="age">children</attribute>
    </points>
  </track>
</annotations>"""

df_basic = pd.DataFrame(
    {
        'image': pd.Categorical(
            ['unittest/00000', 'unittest/00001', 'unittest/00002', 'unittest/00002'],
            categories=['unittest/00000', 'unittest/00001', 'unittest/00002', 'unittest/00003'],
        ),
        'class_label': ['person', 'person', 'person', 'person'],
        'id': pd.Series([0, 0, 0, 2], dtype=pd.Int64Dtype()),
        'x_top_left': [1.0, 2.0, 3.0, 4.0],
        'y_top_left': [5.0, 6.0, 7.0, 8.0],
        'width': [10.0, 20.0, 30.0, 40.0],
        'height': [50.0, 60.0, 70.0, 80.0],
        'ignore': [False, False, False, False],
        'occluded': [False, False, True, False],
        'outside': [False, True, False, False],
        'segmentation': pgpd.GeosArray(
            [
                shapely.box(1, 5, 11, 55),
                shapely.Polygon([[2, 6], [22, 6], [22, 66], [2, 66]]),
                shapely.LineString([[3, 7], [33, 7], [33, 77]]),
                shapely.MultiPoint([[4, 8], [44, 88]]),
            ]
        ),
    }
)


def test_basic(parser, assert_deserialize, assert_serialize):
    parser = parser(extra_attributes=False)
    df = df_basic.copy()

    assert_deserialize(parser, raw_basic, df)


def test_extra_attributes(parser, assert_deserialize, assert_serialize):
    parser = parser(extra_attributes=True)
    df = df_basic.copy()
    df['truncated'] = [0.0, 0.2, 0.0, 1.0]
    df['difficult'] = [False, True, True, False]
    df['age'] = ['adult', 'adult', 'multi', 'children']

    assert_deserialize(parser, raw_basic, df)


def test_keyframe(parser, assert_deserialize, assert_serialize):
    parser = parser(extra_attributes=False, keyframe=True)
    df = df_basic.copy()
    df['keyframe'] = [True, False, True, True]

    assert_deserialize(parser, raw_basic, df)


def test_z_order(parser, assert_deserialize, assert_serialize):
    parser = parser(extra_attributes=False, z_order=True)
    df = df_basic.copy()
    df['z_order'] = [0, 1, 0, 0]

    assert_deserialize(parser, raw_basic, df)


raw_box_only = """<?xml version="1.0" encoding="utf-8"?>
<annotations>
  <version>1.1</version>
  <meta><project>
    <tasks>
        <task>
          <id>42</id>
          <name>unittest</name>
          <start_frame>0</start_frame>
          <stop_frame>3</stop_frame>
        </task>
    </tasks>
    <labels>
        <label>
            <name>person</name>
            <type>any</type>
            <attributes>
                <attribute><name>truncated</name><input_type>number</input_type></attribute>
                <attribute><name>difficult</name><input_type>checkbox</input_type><default_value>false</default_value></attribute>
                <attribute><name>age</name><input_type>text</input_type></attribute>
            </attributes>
        </label>
    </labels>
  </project></meta>
  <track id="0" label="person" task_id="42">
    <box frame="0" keyframe="1" outside="0" xtl="1.0" ytl="5.0" xbr="11.0" ybr="55.0" occluded="0" z_order="0">
      <attribute name="truncated">0.0</attribute>
      <attribute name="age">adult</attribute>
    </box>
  </track>
</annotations>"""


def test_box_only(parser, assert_deserialize, assert_serialize):
    parser = parser(extra_attributes=False)
    df = df_basic.drop(index=[1, 2, 3], columns=['segmentation'])

    assert_deserialize(parser, raw_box_only, df)
