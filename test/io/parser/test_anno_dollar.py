#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation dollar parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.DollarParser


raw_simple = {
    'img01': """% bbGt version=3
person 1 4 10 40 0 0 0 0 0 0 0
person 2 5 20 50 0 0 0 0 0 0 0
""",
    'img02': """% bbGt version=3
dog 3 6 30 60 0 0 0 0 0 0 0
""",
    'img03': """% bbGt version=3
""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_extra = {
    'img01': """% bbGt version=3
person 1 4 10 40 1 0 0 0 0 0 0
person 2 5 20 50 0 0 0 0 0 1 0
""",
    'img02': """% bbGt version=3
dog 3 6 30 60 1 0 0 0 0 0 0
""",
    'img03': """% bbGt version=3
""",
}


def test_extra(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0
    df_anno_simple.loc[0, 'occluded'] = 1.0
    df_anno_simple.loc[1, 'lost'] = True

    df_anno_simple.loc[2, 'occluded'] = 1.0
    assert_deserialize(parser, raw_extra, df_anno_simple)

    df_anno_simple.loc[2, 'occluded'] = 0.5
    assert_serialize(parser, df_anno_simple, raw_extra)


raw_comment = {
    'img01': """% bbGt version=3
person 1 4 10 40 0 0 0 0 0 0 0
person 2 5 20 50 0 0 0 0 0 0 0
""",
    'img02': """% bbGt version=3
dog 3 6 30 60 0 0 0 0 0 0 0
% random comment woot
""",
    'img03': """% bbGt version=3
""",
}


def test_comment(parser, df_anno_simple, assert_deserialize):
    parser = parser()

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0

    assert_deserialize(parser, raw_comment, df_anno_simple)


raw_precision = {
    'img01': """% bbGt version=3
person 1.0 4.0 10.01 40.0 0 0 0 0 0 0 0
person 2.2 5.0 20.0 50.0 0 0 0 0 0 0 0
""",
    'img02': """% bbGt version=3
dog 3.0 6.0 30.0 60.05 0 0 0 0 0 0 0
""",
    'img03': """% bbGt version=3
""",
}


def test_precision(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(precision=2)

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0
    df_anno_simple.loc[0, 'width'] = 10.01
    df_anno_simple.loc[1, 'x_top_left'] = 2.2

    df_anno_simple.loc[2, 'height'] = 60.047
    assert_serialize(parser, df_anno_simple, raw_precision)

    df_anno_simple.loc[2, 'height'] = 60.05
    assert_deserialize(parser, raw_precision, df_anno_simple)


raw_occludedvisible = {
    'img01': """% bbGt version=3
person 1 4 10 40 1 1 4 9 40 0 0
person 2 5 20 50 0 0 0 0 0 0 0
""",
    'img02': """% bbGt version=3
dog 3 6 30 60 1 0 0 0 0 0 0
""",
    'img03': """% bbGt version=3
""",
}


def test_occludedvisible(parser, df_anno_simple, assert_deserialize):
    parser = parser(occluded_from_visible=True)

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0
    df_anno_simple.loc[0, 'occluded'] = 0.9

    assert_deserialize(parser, raw_occludedvisible, df_anno_simple)


raw_occludedtagmap = {
    'img01': """person 1 4 10 40 1 0 0 0 0 0 0
person 2 5 20 50 3 0 0 0 0 0 0
""",
    'img02': """dog 3 6 30 60 0 0 0 0 0 0 0
""",
    'img03': """""",
}


def test_occludedtagmap(parser, df_anno_simple, assert_deserialize):
    parser = parser(occluded_tag_map=[0.0, 0.25, 0.50, 0.75, 1])

    df_anno_simple['lost'] = False
    df_anno_simple['occluded'] = 0.0
    df_anno_simple.loc[0, 'occluded'] = 0.25
    df_anno_simple.loc[1, 'occluded'] = 0.75

    assert_deserialize(parser, raw_occludedtagmap, df_anno_simple)


def test_occlded_multiple_error(parser):
    with pytest.raises(ValueError) as errinfo:
        parser = parser(occluded_from_visible=True, occluded_tag_map=[0.0, 0.5, 1.0])
    assert 'Cannot work with both' in str(errinfo.value)
