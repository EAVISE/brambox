#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation cvc parser
#
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.VaticParser


raw_simple = """-1 1 4 11 44 1 0 0 0 \"person\"
-1 2 5 22 55 1 0 0 0 \"person\"
-1 3 6 33 66 2 0 0 0 \"dog\""""


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['image'] = df_anno_simple['image'].cat.rename_categories(['1', '2', '3'])
    df_anno_simple['image'] = df_anno_simple['image'].cat.remove_unused_categories()
    df_anno_simple['id'] = pd.Series([pd.NA, pd.NA, pd.NA], dtype=pd.Int64Dtype())
    df_anno_simple['occluded'] = [0.0, 0.0, 0.0]
    df_anno_simple['lost'] = [False, False, False]

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_flags = """0 1 4 11 44 1 0 1 0 \"person\"
1 2 5 22 55 1 0 0 0 \"person\"
-1 3 6 33 66 2 1 0 0 \"dog\""""


def test_flags(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(occluded_cutoff=0.7)

    df_anno_simple['image'] = df_anno_simple['image'].cat.rename_categories(['1', '2', '3'])
    df_anno_simple['image'] = df_anno_simple['image'].cat.remove_unused_categories()
    df_anno_simple['id'] = pd.Series([0, 1, pd.NA], dtype=pd.Int64Dtype())
    df_anno_simple['lost'] = [False, False, True]

    df_anno_simple['occluded'] = [1.0, 0.0, 0.0]
    assert_deserialize(parser, raw_flags, df_anno_simple)

    df_anno_simple.at[0, 'occluded'] = 0.8
    df_anno_simple.at[1, 'occluded'] = 0.5
    assert_serialize(parser, df_anno_simple, raw_flags)


raw_catcodes = """-1 1 4 11 44 0 0 0 0 \"person\"
-1 2 5 22 55 0 0 0 0 \"person\"
-1 3 6 33 66 1 0 0 0 \"dog\""""


def test_catcodes(parser, df_anno_simple, assert_serialize):
    parser = parser()

    df_anno_simple['id'] = pd.Series([pd.NA, pd.NA, pd.NA], dtype=pd.Int64Dtype())
    df_anno_simple['occluded'] = [0.0, 0.0, 0.0]
    df_anno_simple['lost'] = [False, False, False]

    assert_serialize(parser, df_anno_simple, raw_catcodes)
