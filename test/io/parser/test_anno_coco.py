#
#   Copyright EAVISE
#   Author: Maarten Vandersteegen
#
#   Test anno coco parser
#

import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.CocoParser


raw_simple = """{
    "annotations":[{"iscrowd": 0, "image_id": 1, "bbox": [1.0, 4.0, 10.0, 40.0], "category_id": 1, "id": 1},
                   {"iscrowd": 0, "image_id": 1, "bbox": [2.0, 5.0, 20.0, 50.0], "category_id": 1, "id": 1},
                   {"iscrowd": 1, "image_id": 2, "bbox": [3.0, 6.0, 30.0, 60.0], "category_id": 5, "id": 2}],
    "categories":[{"id":1, "name":"person"}, {"id":5, "name":"dog"}],
    "images":[{"file_name":"img01", "id":1},
              {"file_name":"img02", "id":2},
              {"file_name":"img03", "id":3}]
}"""


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['id'] = pd.Series([1, 1, 2], dtype=pd.Int64Dtype())
    df_anno_simple.loc[2, 'ignore'] = True

    assert_deserialize(parser, raw_simple, df_anno_simple)


def test_image_id(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(parse_image_names=False)

    df_anno_simple['id'] = pd.Series([1, 1, 2], dtype=pd.Int64Dtype())
    df_anno_simple.loc[2, 'ignore'] = True
    df_anno_simple.image = df_anno_simple.image.cat.rename_categories(lambda imgname: int(imgname[-2:]))

    assert_deserialize(parser, raw_simple, df_anno_simple)


raw_simple_width_height = """{
    "annotations":[{"iscrowd": 0, "image_id": 1, "bbox": [1.0, 4.0, 10.0, 40.0], "category_id": 1, "id": 1},
                   {"iscrowd": 0, "image_id": 1, "bbox": [2.0, 5.0, 20.0, 50.0], "category_id": 1, "id": 1},
                   {"iscrowd": 0, "image_id": 2, "bbox": [3.0, 6.0, 30.0, 60.0], "category_id": 5, "id": 1}],
    "categories":[{"id":1, "name":"person"}, {"id":5, "name":"dog"}],
    "images":[{"file_name":"img01", "id":1, "width":640, "height":512},
              {"file_name":"img02", "id":2, "width":320, "height":240},
              {"file_name":"img03", "id":3, "width":500, "height":600}]
}"""


def test_width_height_columns(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(add_image_dims=True)

    df_anno_simple['id'] = pd.Series([1, 1, 1], dtype=pd.Int64Dtype())
    image_dims = pd.DataFrame(
        {
            'image_width': [640, 640, 320],
            'image_height': [512, 512, 240],
        }
    )
    df_anno_simple = pd.concat([df_anno_simple, image_dims], axis=1)

    assert_deserialize(parser, raw_simple_width_height, df_anno_simple)
