#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation pascalvoc parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.PascalVocParser


raw_simple = {
    'img01': """<annotation>
<object>
\t<name>person</name>
\t<pose>Unspecified</pose>
\t<truncated>0</truncated>
\t<occluded>0</occluded>
\t<difficult>0</difficult>
\t<bndbox>
\t\t<xmin>1</xmin>
\t\t<ymin>4</ymin>
\t\t<xmax>11</xmax>
\t\t<ymax>44</ymax>
\t</bndbox>
</object>
<object>
\t<name>person</name>
\t<pose>Unspecified</pose>
\t<truncated>0</truncated>
\t<occluded>0</occluded>
\t<difficult>0</difficult>
\t<bndbox>
\t\t<xmin>2</xmin>
\t\t<ymin>5</ymin>
\t\t<xmax>22</xmax>
\t\t<ymax>55</ymax>
\t</bndbox>
</object>
</annotation>
""",
    'img02': """<annotation>
<object>
\t<name>dog</name>
\t<pose>Unspecified</pose>
\t<truncated>0</truncated>
\t<occluded>0</occluded>
\t<difficult>0</difficult>
\t<bndbox>
\t\t<xmin>3</xmin>
\t\t<ymin>6</ymin>
\t\t<xmax>33</xmax>
\t\t<ymax>66</ymax>
\t</bndbox>
</object>
</annotation>
""",
    'img03': """<annotation>\n</annotation>\n""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['difficult'] = [False, False, False]
    df_anno_simple['occluded'] = [0.0, 0.0, 0.0]
    df_anno_simple['truncated'] = [0.0, 0.0, 0.0]

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_extra = {
    'img01': """<annotation>
<object>
\t<name>person</name>
\t<pose>Unspecified</pose>
\t<truncated>1</truncated>
\t<occluded>0</occluded>
\t<difficult>0</difficult>
\t<bndbox>
\t\t<xmin>1</xmin>
\t\t<ymin>4</ymin>
\t\t<xmax>11</xmax>
\t\t<ymax>44</ymax>
\t</bndbox>
</object>
<object>
\t<name>person</name>
\t<pose>Unspecified</pose>
\t<truncated>0</truncated>
\t<occluded>1</occluded>
\t<difficult>0</difficult>
\t<bndbox>
\t\t<xmin>2</xmin>
\t\t<ymin>5</ymin>
\t\t<xmax>22</xmax>
\t\t<ymax>55</ymax>
\t</bndbox>
</object>
</annotation>
""",
    'img02': """<annotation>
<object>
\t<name>dog</name>
\t<pose>Unspecified</pose>
\t<truncated>0</truncated>
\t<occluded>0</occluded>
\t<difficult>1</difficult>
\t<bndbox>
\t\t<xmin>3</xmin>
\t\t<ymin>6</ymin>
\t\t<xmax>33</xmax>
\t\t<ymax>66</ymax>
\t</bndbox>
</object>
</annotation>
""",
    'img03': """<annotation>\n</annotation>\n""",
}


def test_extracols(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['difficult'] = [False, False, True]
    df_anno_simple['occluded'] = [0.0, 1.0, 0.0]
    df_anno_simple['truncated'] = [1.0, 0.0, 0.0]

    assert_deserialize(parser, raw_extra, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_extra)


def test_cutoffargs(parser, df_anno_simple, assert_serialize):
    parser = parser(truncated_cutoff=0.5, occluded_cutoff=0.25)

    df_anno_simple['difficult'] = [False, False, False]
    df_anno_simple['occluded'] = [0.0, 0.0, 0.0]
    df_anno_simple['truncated'] = [0.0, 0.0, 0.0]

    df_anno_simple.loc[2, 'difficult'] = True
    df_anno_simple.loc[0, 'truncated'] = 0.6
    df_anno_simple.loc[1, 'occluded'] = 0.3
    assert_serialize(parser, df_anno_simple, raw_extra)

    df_anno_simple.loc[2, 'difficult'] = False
    df_anno_simple.loc[0, 'truncated'] = 0.4
    df_anno_simple.loc[1, 'occluded'] = 0.2
    assert_serialize(parser, df_anno_simple, raw_simple)
