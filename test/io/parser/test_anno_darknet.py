#
#   Copyright EAVISE
#   Author: Maarten Vandersteegen
#
#   Test annotation darknet parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.DarknetParser


class_label_map = ['person', 'dog']

image_dimensions = {'img01': (200, 100), 'img02': (50, 100), 'img03': (10, 10)}

raw_simple = {
    'img01': """0 0.03 0.24 0.05 0.4
0 0.06 0.3 0.1 0.5
""",
    'img02': """1 0.36 0.36 0.6 0.6
""",
    'img03': """""",
}


def test_basic_callable(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(lambda x: image_dimensions[x], class_label_map)

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


def test_basic_dict(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(image_dimensions, class_label_map)

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


def test_missing_class_label_map(parser, df_anno_simple, assert_deserialize, assert_serialize):
    with pytest.raises(ValueError):
        parser(image_dims=lambda x: (200, 100))


def test_missing_image_dims(parser, df_anno_simple, assert_deserialize, assert_serialize):
    with pytest.raises(ValueError):
        parser(class_label_map=class_label_map)
