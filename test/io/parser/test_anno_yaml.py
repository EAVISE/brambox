#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation yaml parser
#
import pandas as pd
import pytest

import brambox as bb

try:
    import yaml

    skip = False
except ImportError:
    skip = True


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.YamlParser


raw_simple = """img01:
  person:
  - coords: [1, 4, 10, 40]
    ignore: false
  - coords: [2, 5, 20, 50]
    ignore: false
img02:
  dog:
  - coords: [3, 6, 30, 60]
    ignore: false
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    # df_anno_simple['id'] = pd.Series([pd.NA, pd.NA, pd.NA], pd.Int64Dtype())

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_kwargs = """img01:
  person:
  - coords: [1.01, 4.0, 10.0, 40.0]
    ignore: true
  - coords: [2.0, 5.0, 20.0, 50.0]
    ignore: false
img02:
  dog:
  - coords: [3.0, 6.0, 30.0, 60.0]
    ignore: false
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_kwargs(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(keep_ignore=True, precision=2)
    df_anno_simple.at[0, 'x_top_left'] = 1.01
    df_anno_simple.at[0, 'ignore'] = True
    assert_deserialize(parser, raw_kwargs, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_kwargs)


raw_with_ignore = """img01:
  person:
  - coords: [1, 4, 10, 40]
    ignore: true
  - coords: [2, 5, 20, 50]
    ignore: true
img02:
  dog:
  - coords: [3, 6, 30, 60]
    ignore: false
img03: {}
"""

raw_without_ignore = """img01:
  person:
  - coords: [1, 4, 10, 40]
  - coords: [2, 5, 20, 50]
img02:
  dog:
  - coords: [3, 6, 30, 60]
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_ignore(parser, df_anno_simple, assert_deserialize, assert_serialize):
    p = parser(keep_ignore=False)
    assert_deserialize(p, raw_with_ignore, df_anno_simple)

    p = parser(keep_ignore=False)
    df_anno_simple.at[0, 'ignore'] = True
    assert_serialize(p, df_anno_simple, raw_without_ignore)
    df_anno_simple.at[0, 'ignore'] = False
    assert_deserialize(p, raw_without_ignore, df_anno_simple)


raw_id = """img01:
  person:
  - coords: [1, 4, 10, 40]
    id: 1
    ignore: false
  - coords: [2, 5, 20, 50]
    id: 2
    ignore: false
img02:
  dog:
  - coords: [3, 6, 30, 60]
    ignore: false
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_id(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_anno_simple['id'] = pd.Series([1, 2, pd.NA], dtype=pd.Int64Dtype())

    assert_deserialize(parser, raw_id, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_id)


raw_label = """img01:
  '?':
  - coords: [1, 4, 10, 40]
    ignore: false
  person:
  - coords: [2, 5, 20, 50]
    ignore: false
img02:
  dog:
  - coords: [3, 6, 30, 60]
    ignore: false
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_no_label(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_anno_simple.at[0, 'class_label'] = ''

    assert_deserialize(parser, raw_label, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_label)


raw_segmentation = """img01:
  person:
  - coords: [1, 4, 10, 40]
    ignore: false
    segmentation: POLYGON ((1 6, 6 4, 11 35, 3 44, 1 6))
  - coords: [2, 5, 1, 1]
    ignore: false
    segmentation: POINT (2.5 5.5)
img02:
  dog:
  - coords: [3, 6, 30, 1]
    ignore: false
    segmentation: LINESTRING (3 6.5, 33 6.5)
  rail:
  - coords: [4, 7, 40, 70]
    ignore: false
    segmentation: LINESTRING (4 7, 44 77)
img03:
  person:
  - coords: [1, 2, 3, 4]
    ignore: false
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_segmentation(parser, df_anno_poly, assert_deserialize, assert_serialize):
    parser = parser()
    df_anno_poly.loc[4] = pd.Series(
        {
            'image': 'img03',
            'class_label': 'person',
            'x_top_left': 1,
            'y_top_left': 2,
            'width': 3,
            'height': 4,
            'difficult': False,
            'ignore': False,
            'segmentation': None,
        }
    )
    df_anno_poly['image'] = df_anno_poly['image'].astype('category')
    df_anno_poly['segmentation'] = df_anno_poly['segmentation'].astype('geos')

    assert_serialize(parser, df_anno_poly, raw_segmentation)
    assert_deserialize(parser, raw_segmentation, df_anno_poly)
