#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation cvc parser
#
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.CvcParser


raw_simple = {
    'img01': """1 4 10 40 1 0 0 0 0 -1 0
2 5 20 50 1 0 0 0 0 -1 0
""",
    'img02': """3 6 30 60 1 0 0 0 0 -1 0
""",
    'img03': """""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label='person')

    df_anno_simple['id'] = pd.Series([pd.NA, pd.NA, pd.NA], dtype=pd.Int64Dtype())
    df_anno_simple.at[2, 'class_label'] = 'person'

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)


raw_ignore = {
    'img01': """1 4 10 40 1 0 0 0 0 -1 0
2 5 20 50 0 0 0 0 0 -1 0
""",
    'img02': """3 6 30 60 1 0 0 0 0 -1 0
""",
    'img03': """""",
}


def test_ignore(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label='person')

    df_anno_simple['id'] = pd.Series([pd.NA, pd.NA, pd.NA], dtype=pd.Int64Dtype())
    df_anno_simple.at[2, 'class_label'] = 'person'
    df_anno_simple.at[1, 'ignore'] = True

    assert_deserialize(parser, raw_ignore, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_ignore)


raw_id = {
    'img01': """1 4 10 40 1 0 0 0 0 1 0
2 5 20 50 1 0 0 0 0 2 0
""",
    'img02': """3 6 30 60 1 0 0 0 0 1 0
""",
    'img03': """""",
}


def test_id(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label='person')

    df_anno_simple['id'] = pd.Series([1, 2, 1], dtype=pd.Int64Dtype())
    df_anno_simple.at[2, 'class_label'] = 'person'

    assert_deserialize(parser, raw_id, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_id)
