#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation dota parser
#
import pandas as pd
import pytest
import shapely

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.annotation.DotaParser


raw_simple = {
    'img01': """1.0 4.0 11.0 4.0 11.0 44.0 1.0 44.0 person 0
2.0 5.0 22.0 5.0 22.0 55.0 2.0 55.0 person 0""",
    'img02': """3.0 6.0 33.0 6.0 33.0 66.0 3.0 66.0 dog 0""",
    'img03': """""",
}


def test_basic(parser, df_anno_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_anno_simple['difficult'] = False
    df_anno_simple['segmentation'] = pd.Series(shapely.box([1.0, 2.0, 3.0], [4.0, 5.0, 6.0], [11.0, 22.0, 33.0], [44.0, 55.0, 66.0])).astype('geos')

    assert_deserialize(parser, raw_simple, df_anno_simple)
    assert_serialize(parser, df_anno_simple, raw_simple)
