#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test pandas parser
#
import pytest

import brambox as bb

try:
    import bs4
    import lxml
    import openpyxl
    import pyarrow
    import tables

    skipio = False
except ImportError:
    skipio = True


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.generic.PandasParser


@pytest.mark.io
@pytest.mark.skipif(skipio, reason='Necessary packages not installed to fully test the pandas parser')
@pytest.mark.parametrize('fmt', ['.csv', '.json', '.html', '.xlsx', '.h5', '.parquet', '.pck'])
def test_basic(fmt, parser, tmp_path, df_anno_simple, df_det_simple, assert_dataframes):
    parser = parser()

    parser.serialize(df_anno_simple, str(tmp_path / f'anno{fmt}'))
    parser.deserialize(str(tmp_path / f'anno{fmt}'))
    if parser.format in parser.unsafe_formats:
        df_anno_simple.image = df_anno_simple.image.cat.remove_unused_categories()
    assert_dataframes(df_anno_simple, parser.get_df())

    parser.serialize(df_det_simple, str(tmp_path / f'det{fmt}'))
    parser.deserialize(str(tmp_path / f'det{fmt}'))
    if parser.format in parser.unsafe_formats:
        df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()
    assert_dataframes(df_det_simple, parser.get_df())


@pytest.mark.io
def test_hardcoded_fmt(parser, tmp_path, df_anno_simple, df_det_simple):
    parser = parser(pd_format='pickle')

    parser.serialize(df_anno_simple, str(tmp_path / 'anno'))
    parser.deserialize(str(tmp_path / 'anno'))
    assert df_anno_simple.equals(parser.get_df())

    parser.serialize(df_det_simple, str(tmp_path / 'det'))
    parser.deserialize(str(tmp_path / 'det'))
    assert df_det_simple.equals(parser.get_df())


@pytest.mark.io
def test_index_order(parser, tmp_path, df_anno_simple):
    parser = parser(pd_format='json')

    df_anno_simple = df_anno_simple.sort_values('x_top_left', ascending=False)
    parser.serialize(df_anno_simple, str(tmp_path / 'anno'))
    parser.deserialize(str(tmp_path / 'anno'))
    df = parser.get_df()
    assert list(df.index) == [0, 1, 2]


def test_invalid_fmt(parser, df_anno_simple):
    with pytest.raises(NotImplementedError) as errinfo:
        p = parser(pd_format='unkown')
        p.serialize(df_anno_simple, 'path.unk')
        assert 'is not a valid function' in str(errinfo.value)

    with pytest.raises(TypeError) as errinfo:
        p = parser()
        p.serialize(df_anno_simple, 'path.unk')
        assert 'is not a known file extension' in str(errinfo.value)


@pytest.mark.io
@pytest.mark.skipif(skipio, reason='Necessary packages not installed to fully test the pandas parser')
@pytest.mark.parametrize('fmt', ['.csv', '.json', '.html', '.xlsx', '.h5', '.parquet', '.pck'])
def test_segmentation(fmt, parser, tmp_path, df_anno_poly, df_det_poly, assert_dataframes):
    parser = parser()

    parser.serialize(df_anno_poly, str(tmp_path / f'anno{fmt}'))
    parser.deserialize(str(tmp_path / f'anno{fmt}'))
    if parser.format in parser.unsafe_formats:
        df_anno_poly.image = df_anno_poly.image.cat.remove_unused_categories()
    assert_dataframes(df_anno_poly, parser.get_df())

    parser.serialize(df_det_poly, str(tmp_path / f'det{fmt}'))
    parser.deserialize(str(tmp_path / f'det{fmt}'))
    if parser.format in parser.unsafe_formats:
        df_det_poly.image = df_det_poly.image.cat.remove_unused_categories()
    assert_dataframes(df_det_poly, parser.get_df())
