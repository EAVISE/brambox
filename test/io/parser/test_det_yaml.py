#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test detection yaml parser
#

import pandas as pd
import pytest

import brambox as bb

try:
    import yaml

    skip = False
except ImportError:
    skip = True


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.YamlParser


raw_simple = """img01:
  person:
  - coords: [1, 4, 10, 40]
    score: 0.0
  - coords: [2, 5, 20, 50]
    score: 100.0
img02:
  dog:
  - coords: [3, 6, 30, 60]
    score: 0.0
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


raw_kwargs = """img01:
  person:
  - coords: [1.01, 4.0, 10.0, 40.0]
    score: 0.0
  - coords: [2.0, 5.0, 20.0, 50.0]
    score: 100.0
img02:
  dog:
  - coords: [3.0, 6.0, 30.0, 60.0]
    score: 0.0
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_kwargs(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(precision=2)
    df_det_simple.at[0, 'x_top_left'] = 1.01

    assert_deserialize(parser, raw_kwargs, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_kwargs)


raw_id = """img01:
  person:
  - coords: [1, 4, 10, 40]
    id: 1
    score: 0.0
  - coords: [2, 5, 20, 50]
    id: 2
    score: 100.0
img02:
  dog:
  - coords: [3, 6, 30, 60]
    score: 0.0
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_id(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_simple['id'] = pd.Series([1, 2, pd.NA], dtype=pd.Int64Dtype())

    assert_deserialize(parser, raw_id, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_id)


raw_label = """img01:
  '?':
  - coords: [1, 4, 10, 40]
    score: 0.0
  person:
  - coords: [2, 5, 20, 50]
    score: 100.0
img02:
  dog:
  - coords: [3, 6, 30, 60]
    score: 0.0
img03: {}
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_no_label(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_simple.at[0, 'class_label'] = ''

    assert_deserialize(parser, raw_label, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_label)


raw_segmentation = """img01:
  person:
  - coords: [1, 4, 10, 40]
    score: 0.0
    segmentation: POLYGON ((1 6, 6 4, 11 35, 3 44, 1 6))
  - coords: [2, 5, 1, 1]
    score: 100.0
    segmentation: POINT (2.5 5.5)
img02:
  dog:
  - coords: [3, 6, 30, 1]
    score: 0.0
    segmentation: LINESTRING (3 6.5, 33 6.5)
  rail:
  - coords: [4, 7, 40, 70]
    score: 100.0
    segmentation: LINESTRING (4 7, 44 77)
img03:
  person:
  - coords: [1, 2, 3, 4]
    score: 50.0
"""


@pytest.mark.skipif(skip, reason='Pyyaml not installed')
def test_segmentation(parser, df_det_poly, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_poly['segmentation'] = df_det_poly['segmentation'].astype(object)
    df_det_poly.loc[4] = pd.Series(
        {
            'image': 'img03',
            'class_label': 'person',
            'x_top_left': 1,
            'y_top_left': 2,
            'width': 3,
            'height': 4,
            'confidence': 0.5,
            'segmentation': None,
        }
    )
    df_det_poly['image'] = df_det_poly['image'].astype('category')
    df_det_poly['segmentation'] = df_det_poly['segmentation'].astype('geos')

    assert_serialize(parser, df_det_poly, raw_segmentation)
    assert_deserialize(parser, raw_segmentation, df_det_poly)
