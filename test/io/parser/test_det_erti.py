#
#   Copyright EAVISE
#   Author: Maarten Vandersteegen
#
#   Test detection ERTI parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.ErtiParser


raw_simple = {
    'img01': """1,4,10,40,0.0
2,5,20,50,1.0
""",
    'img02': """3,6,30,60,0.0
""",
    'img03': """""",
}


def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()

    df_det_simple['class_label'] = ''
    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


def test_basic_custom_class(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label='person')

    df_det_simple['class_label'] = 'person'
    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)
