#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test base parser
#
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.Parser


def test_add_column(parser):
    parser = parser()

    parser.add_column('testcol1')
    assert 'testcol1' in parser.data
    assert 'testcol1' not in parser._column_defaults
    assert 'testcol1' not in parser._column_types

    parser.add_column('testcol2', 'default')
    assert 'testcol2' in parser.data
    assert parser._column_defaults['testcol2'] == 'default'
    assert 'testcol2' not in parser._column_types

    parser.add_column('testcol3', 'value', int)
    assert 'testcol3' in parser.data
    assert parser._column_defaults['testcol3'] == 'value'
    assert parser._column_types['testcol3'] == int


def test_replace_column(parser):
    parser = parser()

    assert 'x_top_left' in parser.data
    assert 'x_top_left' not in parser._column_defaults
    parser.add_column('x_top_left', 0)
    assert 'x_top_left' in parser.data
    assert parser._column_defaults['x_top_left'] == 0

    parser.add_column('id', pd.NA, pd.Int64Dtype())
    assert 'id' in parser.data
    assert 'id' in parser._column_defaults
    assert 'id' in parser._column_types
    parser.add_column('id')
    assert 'id' in parser.data
    assert 'id' not in parser._column_defaults
    assert 'id' not in parser._column_types


def test_add_column_already_data(parser):
    parser = parser()
    parser.append(
        'img01',
        class_label='test',
        id=666,
        x_top_left=1,
        y_top_left=2,
        width=3,
        height=4,
    )

    parser.add_column('testcol1', -1)
    assert parser.data['testcol1'][-1] == -1

    with pytest.raises(ValueError) as errinfo:
        parser.add_column('testcol2')
    assert 'dictionary is not empty' in str(errinfo.value)


def test_append_image(parser):
    parser = parser()
    parser.append_image('img01')

    assert 'img01' in parser.images


def test_append(parser):
    parser = parser()
    parser.append(
        'img01',
        class_label='test',
        x_top_left=1,
        y_top_left=2,
        width=3,
        height=4,
    )

    assert 'img01' in parser.images
    assert parser.data['image'][-1] == 'img01'
    assert parser.data['class_label'][-1] == 'test'
    assert parser.data['x_top_left'][-1] == 1
    assert parser.data['y_top_left'][-1] == 2
    assert parser.data['width'][-1] == 3
    assert parser.data['height'][-1] == 4


def test_append_missing_col(parser):
    parser = parser()
    parser.add_column('id', pd.NA)

    parser.append(
        'img01',
        class_label='test',
        x_top_left=1,
        y_top_left=2,
        width=3,
        height=4,
    )
    assert pd.isna(parser.data['id'][-1])

    with pytest.raises(KeyError) as errinfo:
        parser.append(
            'img01',
            class_label='test',
        )
    assert 'not found in keyword arguments' in str(errinfo.value)
