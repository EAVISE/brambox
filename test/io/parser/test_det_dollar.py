#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test detection dollar parser
#
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.DollarParser


raw_simple = """1,1,4,10,40,0.0
1,2,5,20,50,1.0
2,3,6,30,60,0.0"""


def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_simple.class_label = '?'
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()
    df_det_simple.image = df_det_simple.image.cat.rename_categories(['1', '2'])

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


def test_class_label(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser(class_label='person')
    df_det_simple['class_label'] = 'person'
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()
    df_det_simple.image = df_det_simple.image.cat.rename_categories(['1', '2'])

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)


raw_catcodes = """0,1,4,10,40,0.0
0,2,5,20,50,1.0
1,3,6,30,60,0.0"""


def test_catcodes(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser()
    df_det_simple.class_label = '?'
    df_det_simple.image = df_det_simple.image.cat.remove_unused_categories()

    assert_serialize(parser, df_det_simple, raw_catcodes)
