#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test annotation MOT parser
#
import pandas as pd
import pytest

import brambox as bb


@pytest.fixture(scope='module')
def parser():
    return bb.io.parser.detection.MotParser


raw_simple = {
    'vid01': """1,1,1.0,4.0,10.0,40.0,0.0,-1,-1,-1
1,1,2.0,5.0,20.0,50.0,1.0,-1,-1,-1
2,5,3.0,6.0,30.0,60.0,0.7,-1,-1,-1
""",
}


def test_basic(parser, df_det_simple, assert_deserialize, assert_serialize):
    parser = parser('person', precision=1)

    df_det_simple['image'] = df_det_simple['image'].cat.rename_categories(['vid01--1', 'vid01--2', 'vid01--3'])
    df_det_simple['image'] = df_det_simple['image'].cat.remove_unused_categories()
    df_det_simple['id'] = pd.Series([1, 1, 5], dtype=pd.Int64Dtype())
    df_det_simple.loc[2, 'class_label'] = 'person'
    df_det_simple.loc[2, 'confidence'] = 0.7
    df_det_simple['video'] = ['vid01', 'vid01', 'vid01']
    df_det_simple['frame'] = [1, 1, 2]

    assert_deserialize(parser, raw_simple, df_det_simple)
    assert_serialize(parser, df_det_simple, raw_simple)
