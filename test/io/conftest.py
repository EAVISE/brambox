#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Brambox io fixtures
#
import pytest

import brambox as bb


@pytest.fixture(scope='package')
def assert_deserialize(assert_dataframes):
    """This fixture returns a function that will assert whether deserialization with a certain parser works.
    The function is called with the following parameters: `assert_deserialize(parser, rawdata, df)`.
    """

    def _assert_deserialize(parser, rawdata, df):
        if parser.parser_type == bb.io.parser.ParserType.SINGLE_FILE:
            parser.deserialize(rawdata)
        elif parser.parser_type == bb.io.parser.ParserType.MULTI_FILE:
            for img, data in rawdata.items():
                parser.deserialize(data, file_id=img)
        else:
            raise NotImplementedError(f'Cannot check this type of parser [{parser.parser_type}]')
        df_result = parser.get_df()

        # Fixture from ./test/conftest.py
        assert_dataframes(df, df_result)

    return _assert_deserialize


@pytest.fixture(scope='package')
def assert_serialize():
    """This fixture returns a function that will assert whether serialization with a certain parser works.
    The function is called with the following parameters: `assert_deserialize(parser, df, rawdata)`.
    """

    def _assert_serialize(parser, df, rawdata):
        df = parser.set_df(df)

        data = df.apply(parser.serialize, axis=1) if parser.serialize_group is None else df.groupby(parser.serialize_group).apply(parser.serialize)

        if parser.parser_type == bb.io.parser.ParserType.SINGLE_FILE:
            rawdata_result = parser.header + data.str.cat(sep=parser.serialize_group_separator) + parser.footer
            assert rawdata == rawdata_result
        elif parser.parser_type == bb.io.parser.ParserType.MULTI_FILE:
            for img, rawdata_result in data.items():
                assert rawdata[img] == parser.header + rawdata_result + parser.footer
        else:
            raise NotImplementedError(f'Cannot check this type of parser [{parser.parser_type}]')

    return _assert_serialize
