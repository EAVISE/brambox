#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Test brambox.io._util module
#

import pytest

import brambox.io._util as uut


def test_strider():
    elements = [0, 1, 2, 3, 4, 5]

    assert list(uut.strider(elements, 1, 0)) == elements
    assert list(uut.strider(elements, 2, 0)) == elements[::2]
    assert list(uut.strider(elements, 1, 3)) == elements[3:]
    assert list(uut.strider(elements, 2, 1)) == elements[1::2]


@pytest.mark.io
def test_modulo_expand(tmp_path):
    filenames = []
    for i in range(10):
        f = tmp_path / f'file_{i:02d}.txt'
        filenames.append(str(f))
        f.open('a').close()

    assert list(uut.modulo_expand(f'{tmp_path}/file_%02d.txt', 1, 0)) == filenames
    assert list(uut.modulo_expand(f'{tmp_path}/file_%02d.txt', 2, 0)) == filenames[::2]
    assert list(uut.modulo_expand(f'{tmp_path}/file_%02d.txt', 1, 3)) == filenames[3:]
    assert list(uut.modulo_expand(f'{tmp_path}/file_%02d.txt', 2, 1)) == filenames[1::2]


@pytest.mark.io
def test_files(tmp_path):
    filenames = []
    for i in range(10):
        f = tmp_path / f'file_{i:02d}.txt'
        filenames.append(str(f))
        f.open('a').close()

    (tmp_path / 'subdir').mkdir()
    for i in range(2):
        (tmp_path / 'subdir' / f'file_{i}.txt').open('a').close()

    assert sorted(uut.files(str(tmp_path))) == filenames


@pytest.mark.io
def test_expand(tmp_path):
    filenames = []
    for i in range(10):
        f = tmp_path / f'file_{i:02d}.txt'
        filenames.append(str(f))
        f.open('a').close()

    assert list(uut.expand(f'{tmp_path}')) == filenames
    assert uut.expand(f'{tmp_path}/file_00.txt') == filenames[:1]
    assert list(uut.expand(f'{tmp_path}/file_%02d.txt')) == filenames
    assert list(uut.expand(f'{tmp_path}/file_*.txt')) == filenames
    with pytest.raises(TypeError) as errinfo:
        uut.expand(f'{tmp_path}/doesnotexist.txt')
    assert 'expression invalid' in str(errinfo.value)
