#
#   Copyright EAVISE
#   Author: Tanguy Ophoff
#
#   Brambox parser fixtures
#
import sys

import numpy as np
import pandas as pd
import pgpd
import pytest
import shapely


@pytest.fixture
def df_anno_simple():
    """Base annotation dataframe every parser should be able to create.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | occluded | truncated | lost  | difficult | ignore
    ------------------------------------------------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | 0.0      | 0.0       | False | False     | False
    img01 | person      | NaN | 2.0        | 5.0        | 20.0  | 50.0   | 0.0      | 0.0       | False | False     | False
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  | 60.0   | 0.0      | 0.0       | False | False     | False
    """
    return pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog'],
            'x_top_left': [1.0, 2.0, 3.0],
            'y_top_left': [4.0, 5.0, 6.0],
            'width': [10.0, 20.0, 30.0],
            'height': [40.0, 50.0, 60.0],
            'ignore': [False, False, False],
        }
    )


@pytest.fixture
def df_det_simple():
    """Base detection dataframe every parser should be able to create.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | confidence
    ---------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | 0.0
    img01 | person      | NaN | 2.0        | 5.0        | 20.0  | 50.0   | 1.0
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  | 60.0   | 0.0
    """
    return pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog'],
            'x_top_left': [1.0, 2.0, 3.0],
            'y_top_left': [4.0, 5.0, 6.0],
            'width': [10.0, 20.0, 30.0],
            'height': [40.0, 50.0, 60.0],
            'confidence': [0.0, 1.0, 0.0],
        }
    )


@pytest.fixture
def df_anno_multi_img():
    """
    Basic annotation dataframe that has 11 images ['img00', 'img01', 'img02', 'img03', 'img04', 'img05', 'img06', 'img07', 'img08', 'img09', 'img10'].
    img00 has 2 bounding boxes and img10 has none. All other have exactly 1 bounding box and all bounding boxes have the same data:

    image | class_label | id  | x_top_left | y_top_left | width | height | occluded | truncated | lost  | difficult | ignore
    ------------------------------------------------------------------------------------------------------------------------
    img.. | person      | NaN | 1.0        | 2.0        | 3.0   | 4.0    | 0.0      | 0.0       | False | False     | False
    """
    df = pd.DataFrame(
        {
            'image': pd.Categorical(['img00'] + [f'img0{i}' for i in range(10)], categories=[f'img{i:02d}' for i in range(11)]),
            'class_label': ['person'] * 11,
            'x_top_left': [1.0] * 11,
            'y_top_left': [2.0] * 11,
            'width': [3.0] * 11,
            'height': [4.0] * 11,
            'ignore': [False] * 11,
        }
    )

    return df.sort_values('image').reset_index(drop=True)


@pytest.fixture
def df_det_multi_img():
    """
    Basic annotation dataframe that has 11 images ['img00', 'img01', 'img02', 'img03', 'img04', 'img05', 'img06', 'img07', 'img08', 'img09', 'img10'].
    img00 has 2 bounding boxes and img10 has none. All others have exactly 1 bounding box and all bounding boxes have the same data:

    image | class_label | id  | x_top_left | y_top_left | width | height | confidence
    ---------------------------------------------------------------------------------
    img.. | person      | NaN | 1.0        | 2.0        | 3.0   | 4.0    | 1.0
    """
    df = pd.DataFrame(
        {
            'image': pd.Categorical(['img00'] + [f'img0{i}' for i in range(10)], categories=[f'img{i:02d}' for i in range(11)]),
            'class_label': ['person'] * 11,
            'x_top_left': [1.0] * 11,
            'y_top_left': [2.0] * 11,
            'width': [3.0] * 11,
            'height': [4.0] * 11,
            'confidence': [1.0] * 11,
        }
    )

    return df.sort_values('image').reset_index(drop=True)


@pytest.fixture
def df_anno_oriented():
    """Annotation dataframe with angle column.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | angle | occluded | truncated | lost  | difficult | ignore
    -------------------------------------------------------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | pi/3  | 0.0      | 0.0       | False | False     | False
    img01 | person      | NaN | 2.0        | 5.0        | 20.0  | 50.0   | pi/4  | 0.0      | 0.0       | False | False     | False
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  | 60.0   | pi/6  | 0.0      | 0.0       | False | False     | False
    """
    return pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog'],
            'x_top_left': [1.0, 2.0, 3.0],
            'y_top_left': [4.0, 5.0, 6.0],
            'width': [10.0, 20.0, 30.0],
            'height': [40.0, 50.0, 60.0],
            'angle': [np.pi / 3, np.pi / 4, np.pi / 6],
            'ignore': [False, False, False],
        }
    )


@pytest.fixture
def df_det_oriented():
    """Detection dataframe with angle column.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | angle | confidence
    -----------------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | pi/3  | 0.0
    img01 | person      | NaN | 2.0        | 5.0        | 20.0  | 50.0   | pi/4  | 1.0
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  | 60.0   | pi/6  | 0.0
    """
    return pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog'],
            'x_top_left': [1.0, 2.0, 3.0],
            'y_top_left': [4.0, 5.0, 6.0],
            'width': [10.0, 20.0, 30.0],
            'height': [40.0, 50.0, 60.0],
            'angle': [np.pi / 3, np.pi / 4, np.pi / 6],
            'confidence': [0.0, 1.0, 0.0],
        }
    )


@pytest.fixture
def df_anno_poly():
    """Annotation dataframe with segmentation polygon column.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | occluded | truncated | lost  | difficult | ignore | segmentation
    ---------------------------------------------------------------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | 0.0      | 0.0       | False | False     | False  | Polygon ()
    img01 | person      | NaN | 2.0        | 5.0        |  1.0  |  1.0   | 0.0      | 0.0       | False | False     | False  | Point ()
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  |  1.0   | 0.0      | 0.0       | False | False     | False  | Line ()
    img02 | rail        | NaN | 4.0        | 7.0        | 40.0  | 70.0   | 0.0      | 0.0       | False | False     | False  | Line ()
    """
    df = pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog', 'rail'],
            'x_top_left': [1.0, 2.0, 3.0, 4.0],
            'y_top_left': [4.0, 5.0, 6.0, 7.0],
            'width': [10.0, 1.0, 30.0, 40.0],
            'height': [40.0, 1.0, 1.0, 70.0],
            'ignore': [False, False, False, False],
            'segmentation': pgpd.GeosArray(
                [
                    shapely.Polygon([[1, 6], [6, 4], [11, 35], [3, 44]]),
                    shapely.Point(2.5, 5.5),
                    shapely.LineString([[3, 6.5], [33, 6.5]]),
                    shapely.LineString([[4, 7], [44, 77]]),
                ]
            ),
        }
    )
    df['segmentation'] = df['segmentation'].geos.to_geos()

    return df


@pytest.fixture
def df_det_poly():
    """Detection dataframe with segmentation polygon column.
    The df consists of 3 images ['img01', 'img02', 'img03'] and has the following boxes:

    image | class_label | id  | x_top_left | y_top_left | width | height | confidence | segmentation
    ------------------------------------------------------------------------------------------------
    img01 | person      | NaN | 1.0        | 4.0        | 10.0  | 40.0   | 0.0        | Polygon()
    img01 | person      | NaN | 2.0        | 5.0        |  1.0  |  1.0   | 1.0        | Point()
    img02 | dog         | NaN | 3.0        | 6.0        | 30.0  |  1.0   | 0.0        | Line()
    img02 | rail        | NaN | 4.0        | 7.0        | 40.0  | 70.0   | 1.0        | Line()
    """
    df = pd.DataFrame(
        {
            'image': pd.Categorical(['img01', 'img01', 'img02', 'img02'], categories=['img01', 'img02', 'img03']),
            'class_label': ['person', 'person', 'dog', 'rail'],
            'x_top_left': [1.0, 2.0, 3.0, 4.0],
            'y_top_left': [4.0, 5.0, 6.0, 7.0],
            'width': [10.0, 1.0, 30.0, 40.0],
            'height': [40.0, 1.0, 1.0, 70.0],
            'confidence': [0.0, 1.0, 0.0, 1.0],
            'segmentation': pgpd.GeosArray(
                [
                    shapely.Polygon([[1, 6], [6, 4], [11, 35], [3, 44]]),
                    shapely.Point(2.5, 5.5),
                    shapely.LineString([[3, 6.5], [33, 6.5]]),
                    shapely.LineString([[4, 7], [44, 77]]),
                ]
            ),
        }
    )
    df['segmentation'] = df['segmentation'].geos.to_geos()

    return df


@pytest.fixture(scope='session')
def assert_dataframes():
    """This fixture provides a wrapper to assert that 2 dataframes are equal and pretty prints both dataframes otherwise.
    The function is called with the following parameters: `assert_dataframes(df1, df2)`.
    """

    def _assert_dataframes(df1, df2):
        df1 = df1.sort_index(axis=1)
        df2 = df2.sort_index(axis=1)

        eq = df1.equals(df2)
        if not eq:
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                print('DF1:')
                print(df1)
                print(df1.dtypes)
                if 'image' in df1.columns:
                    print(df1.image.cat.categories)
                print('\nDF2:')
                print(df2)
                print(df2.dtypes)
                if 'image' in df1.columns:
                    print(df2.image.cat.categories)

        assert eq

    return _assert_dataframes


@pytest.fixture(scope='session')
def assert_shapely():
    """This fixture provides a wrapper to assert that 2 shapely arrays are equal and pretty prints both arrays otherwise.
    The function is called with the following parameters: `assert_shapely(series1, series2)`.
    """

    def _assert_shapely(geom1, geom2, tolerance=None):
        geom1 = shapely.normalize(np.asarray(geom1))
        geom2 = shapely.normalize(np.asarray(geom2))

        if tolerance is not None:
            geom1 = shapely.set_precision(geom1, tolerance)
            geom2 = shapely.set_precision(geom2, tolerance)

        eq = shapely.equals(geom1, geom2) | shapely.equals_exact(geom1, geom2)
        if not np.all(eq):
            with np.printoptions(threshold=sys.maxsize):
                print('GEOM1:')
                print(geom1)
                print('\nGEOM2:')
                print(geom2)
                print('\nEQUALITY:')
                print(np.asarray(eq))

        assert np.all(eq)

    return _assert_shapely
